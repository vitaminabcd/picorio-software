#!/bin/bash

dir=$1
shift
pushd $dir && $* && popd
