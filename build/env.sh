#!/bin/bash

TOP_DIR=$(\cd $(\dirname ${BASH_SOURCE:-$0})/..;pwd)

export PROJ_ROOT=${TOP_DIR}
export RISCV=${PROJ_ROOT}/output/internal/software/host
export PLATFORM_NAME=es1y
echo "Now PROJ_ROOT = $PROJ_ROOT"
echo "Now RISCV     = $RISCV"
export PATH=${RISCV}/bin:${PROJ_ROOT}/output/gcc/bin:${PATH}
export LD_LIBRARY_PATH=${RISCV}/lib