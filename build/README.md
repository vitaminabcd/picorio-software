Till 2021/1/24, can boot linux single-core on 100M CPU/50M DDR, and the ISA is rv64imafdc while riscv gcc version is 10.2

***********************

0, pre-requisite

0.1 vivado_lab

make sure vivado_lab is on the PATH env. That means, "which vivado_lab" command can find it.

0.2 uart console

make sure uart console is open. (baudrate: 1500000 8n1)

tips: get the console device name by this command:
dmesg | grep cp210x

and then bootup minicom or other serial tools on the other terminal: (provided it is ttyUSB0)

sudo minicom -D /dev/ttyUSB0

0.3 set env

on the root path, please run:

source build/env.sh

***********************

1, how to make (usual commands)

1.0 make info

help. display info which target it supports.

1.1 make

it will eventually run linux on picorio board.

1.2 make all-clean

it will delete anythings in output directory and clean all targets

1.3 make rvxlx

if you modify linux kernel and re-run linux on picorio, please use it.

1.4 make rvxbr

if you only add target files on rootfs buildroot generates and re-run linux on picorio, please use it.

1.5 make rv

run vivado or vivado_lab with tcl script

1.6 make rvd

means run debug-socket.o in driver

***********************

2, output

2.1 gcc
it is in output/gcc/

2.2 bbl & its dump
it is in output/internal/software/target/bin/bootloader/bbl/

2.3 vmlinux & its dump
it is in output/internal/software/target/bin/linux/generic/

***********************

3, TODO:

3.1 beak
beak has not been verified.

3.2 linux multi-core cases / ISA without mc / 200M CPU
these cases just on going...
