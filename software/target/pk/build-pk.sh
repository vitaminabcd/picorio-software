#!/bin/bash
#
# Script to build RISC-V ISA simulator, proxy kernel, and GNU toolchain.
# Tools will be installed to $RISCV.

#export PATH=/work/users/tian/spike/riscv/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

#export RISCV=/work/tools/ours-gnu-toolchain/riscv/
export MAKEFLAGS="$MAKEFLAGS -j16"
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:/usr/lib/pkgconfig
#export PATH=$RISCV/bin:$PATH
export LD_LIBRARY_PATH=$RISCV/lib:/usr/local/lib64:/usr/local/lib
#export CC=gcc-7.2
#export CXX=g++-7.2
#export PATH=/work/tools/compilers/rv64gc_ours_gen2/bin/:$PATH



. build.common

echo "Starting RISC-V Toolchain build process"

# CFLAGS='-std=c99' build_project riscv-openocd --prefix=$RISCV --enable-remote-bitbang --enable-jtag_vpi --disable-werror
# build_project riscv-openocd --prefix=$RISCV --enable-remote-bitbang --enable-jtag_vpi --disable-werror
# build_project riscv-fesvr --prefix=$RISCV
# build_project riscv-isa-sim --prefix=$RISCV --with-fesvr=$RISCV
# build_project riscv-gnu-toolchain --prefix=$RISCV
# cd riscv-gnu-toolchain/build; make -j16 linux
 CC= CXX= build_project riscv-pk --prefix=$RISCV --host=riscv64-unknown-linux-gnu --with-payload=$1
# build_project riscv-tests --prefix=$RISCV/riscv64-unknown-elf
# cd ../riscv-linux; CROSS_COMPILE=riscv64-unknown-linux-gnu- make -j32 ARCH=riscv vmlinux

#cd riscv-pk/build; ../configure --with-payload=/work/users/jwang/tmp/riscv-linux/vmlinux --host=riscv64-unknown-linux-gnu; make bbl
#cd ../../
#build_project riscv-fesvr --prefix=$RISCV
#build_project riscv-isa-sim --prefix=$RISCV --with-fesvr=$RISCV
#build_project riscv-gnu-toolchain --prefix=$RISCV

echo -e "\\nRISC-V Toolchain installation completed!"
