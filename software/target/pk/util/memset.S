/*
 * changes from linux kernel.
*/

#include "bits.h"

/* void *memset(void *, int, size_t) */
  .align 4
  .globl memset
memset:
	move t0, a0  /* Preserve return value */

	/* Defer to byte-oriented fill for small sizes */
	sltiu a3, a2, 16
	bnez a3, 4f

	/*
	 * Round to nearest XLEN-aligned address
	 * greater than or equal to start address
	 */
	addi a3, t0, REGBYTES-1
	andi a3, a3, ~(REGBYTES-1)
	beq a3, t0, 2f  /* Skip if already aligned */
	/* Handle initial misalignment */
	sub a4, a3, t0
1:
	sb a1, 0(t0)
	addi t0, t0, 1
	bltu t0, a3, 1b
	sub a2, a2, a4  /* Update count */

2: /* Duff's device with 32 XLEN stores per iteration */
	/* Broadcast value into all bytes */
	andi a1, a1, 0xff
	slli a3, a1, 8
	or a1, a3, a1
	slli a3, a1, 16
	or a1, a3, a1
#if __riscv_xlen == 64
	slli a3, a1, 32
	or a1, a3, a1
#endif

	/* Calculate end address */
	andi a4, a2, ~(REGBYTES-1)
	add a3, t0, a4

	andi a4, a4, 31*REGBYTES  /* Calculate remainder */
	beqz a4, 3f            /* Shortcut if no remainder */
	neg a4, a4
	addi a4, a4, 32*REGBYTES  /* Calculate initial offset */

	/* Adjust start address with offset */
	sub t0, t0, a4

	/* Jump into loop body */
	/* Assumes 32-bit instruction lengths */
	la a5, 3f
#if __riscv_xlen == 64
	srli a4, a4, 1
#endif
	add a5, a5, a4
	jr a5
3:
	STORE a1,        0(t0)
	STORE a1,    REGBYTES(t0)
	STORE a1,  2*REGBYTES(t0)
	STORE a1,  3*REGBYTES(t0)
	STORE a1,  4*REGBYTES(t0)
	STORE a1,  5*REGBYTES(t0)
	STORE a1,  6*REGBYTES(t0)
	STORE a1,  7*REGBYTES(t0)
	STORE a1,  8*REGBYTES(t0)
	STORE a1,  9*REGBYTES(t0)
	STORE a1, 10*REGBYTES(t0)
	STORE a1, 11*REGBYTES(t0)
	STORE a1, 12*REGBYTES(t0)
	STORE a1, 13*REGBYTES(t0)
	STORE a1, 14*REGBYTES(t0)
	STORE a1, 15*REGBYTES(t0)
	STORE a1, 16*REGBYTES(t0)
	STORE a1, 17*REGBYTES(t0)
	STORE a1, 18*REGBYTES(t0)
	STORE a1, 19*REGBYTES(t0)
	STORE a1, 20*REGBYTES(t0)
	STORE a1, 21*REGBYTES(t0)
	STORE a1, 22*REGBYTES(t0)
	STORE a1, 23*REGBYTES(t0)
	STORE a1, 24*REGBYTES(t0)
	STORE a1, 25*REGBYTES(t0)
	STORE a1, 26*REGBYTES(t0)
	STORE a1, 27*REGBYTES(t0)
	STORE a1, 28*REGBYTES(t0)
	STORE a1, 29*REGBYTES(t0)
	STORE a1, 30*REGBYTES(t0)
	STORE a1, 31*REGBYTES(t0)
	addi t0, t0, 32*REGBYTES
	bltu t0, a3, 3b
	andi a2, a2, REGBYTES-1  /* Update count */

4:
	/* Handle trailing misalignment */
	beqz a2, 6f
	add a3, t0, a2
5:
	sb a1, 0(t0)
	addi t0, t0, 1
	bltu t0, a3, 5b
6:
	ret
