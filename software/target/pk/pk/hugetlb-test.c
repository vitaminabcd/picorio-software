
 #include <ctype.h>
 #include <stdio.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>

//#define LINUX_ENV
#ifdef LINUX_ENV
#include <sys/mman.h>
#else
#define PROT_READ 1
#define PROT_WRITE 2
#define MAP_FAILED -1
#endif
#define VCORE_USE_HUGETLB /* SV39: 2MB */
#define SIZE (4*1024*1024)

static int huge_fd = -1;
/* 
 * hack here!! 
 * riscv newlib doesn't support mmap syscall usage and its malloc() calls sbrk() that is not good choice for hugetlb.
 * use "open" syscall temporay to avoid modifying toolchain/newlib
 * only for pk but not linux kernel
*/
static void *hugetlb_mmap(size_t length)
{
#ifdef LINUX_ENV /*MAP_POPULATE*/
//	return mmap(NULL, length, 0x11/*PROT_READ | PROT_WRITE*/, 0x40022/*MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB*/, -1, 0)
	return mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB | MAP_POPULATE, huge_fd, 0);
#else
	return (void *)open("/mmap", PROT_READ | PROT_WRITE, (int)length);
#endif
}

static int hugetlb_munmap(void *addr, size_t length)
{
#ifdef LINUX_ENV
	return munmap(addr, length);
#else
	return open("/munmap", (int)addr, (int)length);
#endif
}
#if 1
static inline void vfload(unsigned long addr)
{
	asm volatile (
	  "vfld v0, %0 \n\t"
	 ::"r"(addr)
	);
}

static inline void vfstore(unsigned long addr)
{
	asm volatile (
      "vfst v0, %0 \n\t"
	 ::"r"(addr)
	);
}

static inline void vfinit(unsigned long vlen)
{
	asm volatile (
	"csrw  vlen, %0 \n\t"
	::"r"(vlen)
	);
	asm volatile (
	"csrw  vgrpdepth, %0 \n\t"
	::"r"(vlen)
	);
}

static inline void mb(void)
{
	asm volatile ("fence" ::: "memory");
}
#endif
#ifdef VCORE_USE_HUGETLB 
static void *alloc_mm()
{
	return hugetlb_mmap(SIZE);
}

static int alloc_error(void *addr)
{
	return ((void *)(-1) == addr);
}

static void free_mm(void *addr)
{
	hugetlb_munmap(addr, SIZE);
}

#else
static void *alloc_mm()
{
	return malloc(SIZE);
}

static int alloc_error(void *addr)
{
	return (NULL == addr);
}

static void free_mm(void *addr)
{
	free(addr);
}
#endif

int main(int argc, char **argv)
{
#if 1
	const unsigned long offset = 512*1024UL;

///work/home/rchen/data/test
//	huge_fd = open("/dev/hugepages/htest", O_RDWR);
//	huge_fd = open("work/home/rchen/data/test/htest", O_RDWR);

	unsigned short *mp = alloc_mm();
	if(alloc_error(mp)) {
		printf("error\n");		
		return -1;
	}
#if 1
	printf("mp: 0x%lx\n", mp);
	*mp = 0x1234;
	mp[offset] = *mp;
	printf("test: 0x%x\n", mp[offset]);
#else
//	printf("mp=%p %p\n", mp, (unsigned long)mp+2*1024);
	vfinit(1UL);

    *mp = 0x1234;
	vfload(mp);
//	mb();
	vfstore((unsigned long)mp +offset);
//    mb();
	printf("test: 0x%x\n", *(volatile unsigned short *)((unsigned long)mp +offset));
#endif
#if 0
	vfinit(1UL);
	vfload((unsigned long)mp+1024*1024);
	vfload((unsigned long)mp+3*1024*1024);
	vfload((unsigned long)mp+5*1024*1024);
#if 0
	vfload((unsigned long)mp+7*1024*1024);
	vfload((unsigned long)mp+9*1024*1024);
	vfload((unsigned long)mp+11*1024*1024);
	vfload((unsigned long)mp+13*1024*1024);
	vfload((unsigned long)mp+15*1024*1024);
	vfload((unsigned long)mp+17*1024*1024);
	vfload((unsigned long)mp+19*1024*1024);
#endif

	vfload((unsigned long)mp+1024*1024+8*1024);
	vfload((unsigned long)mp+3*1024*1024+8*1024);
	vfload((unsigned long)mp+5*1024*1024+8*1024);
#if 0
	vfload((unsigned long)mp+7*1024*1024+8*1024);
	vfload((unsigned long)mp+9*1024*1024+8*1024);
	vfload((unsigned long)mp+11*1024*1024+8*1024);
	vfload((unsigned long)mp+13*1024*1024+8*1024);
	vfload((unsigned long)mp+15*1024*1024+8*1024);
	vfload((unsigned long)mp+17*1024*1024+8*1024);
	vfload((unsigned long)mp+19*1024*1024+8*1024);
#endif

	vfload((unsigned long)mp+1024*1024+16*1024);
	vfload((unsigned long)mp+3*1024*1024+16*1024);
	vfload((unsigned long)mp+5*1024*1024+16*1024);
#endif
#if 0
	vfload((unsigned long)mp+7*1024*1024+16*1024);
	vfload((unsigned long)mp+9*1024*1024+16*1024);
	vfload((unsigned long)mp+11*1024*1024+16*1024);
	vfload((unsigned long)mp+13*1024*1024+16*1024);
	vfload((unsigned long)mp+15*1024*1024+16*1024);
	vfload((unsigned long)mp+17*1024*1024+16*1024);
	vfload((unsigned long)mp+19*1024*1024+16*1024);
#endif
#if 0
//	mp[1024] = 0x1234; // 2k
    unsigned short val = 0x123;
#if 0
	asm volatile (
      "xor x0, x0, x0\n\t"
	);
	asm volatile (
      "add x0, x0, %0\n\t"
	 ::"r"(val)
	);
#endif
	asm volatile (
      "vxor v0, v0, v0 \n\t"
	);
	asm volatile (
      "vadds v0, v0, %0 \n\t"
	 ::"r"(val)
	);

	vfinit(1UL);
	mb();
	vfstore((unsigned long)mp +2*1024);

//	printf("line: %d\n", __LINE__);

//	printf("line: %d\n", __LINE__);

	vfload(mp);
	//printf("ori: %d\n", mp[1024]);
//	mb();
	vfstore((unsigned long)mp +offset);
#endif
	//printf("copy: 0x%x\n", *(unsigned short *)((unsigned long)mp +offset));
//	while(1);
	free_mm(mp);
//	printf("hello world\n");
#if 0
	int i, size = SIZE;

	unsigned char *mp = hugetlb_mmap(size);

	if (mp == (void *)MAP_FAILED) {
	    perror("map mem");
	    return -1;
	}

	*mp = 0x12;
	*(mp+size-1) = 0x34;

	hugetlb_munmap(mp, size);	
//#else
	int i, size = SIZE;

	unsigned char *mp = mmap(NULL, size, 0x11/*PROT_READ | PROT_WRITE*/, 0x40022/*MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB*/, -1, 0);

	if (mp == (void *)(-1)/*MAP_FAILED*/) {
	    perror("map mem");
	    return -1;
	}
	printf("hello world\n");

	munmap(mp, size);

//#else

printf("line: %d\n", __LINE__);
	unsigned char *mp1 = malloc(1024*1024);
	*(unsigned char *)mp1 = 1;
printf("line: %d mp1:0x%x\n", __LINE__, mp1);
	unsigned char *mp = malloc(8*1024*1024);
	*(unsigned char *)mp = 1;
printf("line: %d mp:0x%x\n", __LINE__, mp);

	if (mp1)
		free(mp1);
printf("line: %d\n", __LINE__);

	if (mp)
		free(mp);
printf("line: %d\n", __LINE__);

	printf("hello world\n");

#if 0
	mp = malloc(1024*1024*1024);

	if (mp)
		free(mp);
//#else
#endif
//	printf("hello world\n");
//#else

#endif
//while(1);
#endif
	return 0;
}
