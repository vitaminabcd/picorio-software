/*
 * FreeRTOS Kernel V10.2.1
 * Copyright (C) 2019 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://www.FreeRTOS.org
 * http://aws.amazon.com/freertos
 *
 * 1 tab == 4 spaces!
 */

/*-----------------------------------------------------------
 * Implementation of functions defined in portable.h for the RISC-V RV32 port.
 *----------------------------------------------------------*/

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "portmacro.h"

/* Standard includes. */
#include "string.h"

#ifndef configCLINT_BASE_ADDRESS
	#warning configCLINT_BASE_ADDRESS must be defined in FreeRTOSConfig.h.  If the target chip includes a Core Local Interrupter (CLINT) then set configCLINT_BASE_ADDRESS to the CLINT base address.  Otherwise set configCLINT_BASE_ADDRESS to 0.
#endif

/* Let the user override the pre-loading of the initial LR with the address of
prvTaskExitError() in case it messes up unwinding of the stack in the
debugger. */
#ifdef configTASK_RETURN_ADDRESS
	#define portTASK_RETURN_ADDRESS	configTASK_RETURN_ADDRESS
#else
	#define portTASK_RETURN_ADDRESS	prvTaskExitError
#endif

/* The stack used by interrupt service routines.  Set configISR_STACK_SIZE_WORDS
to use a statically allocated array as the interrupt stack.  Alternative leave
configISR_STACK_SIZE_WORDS undefined and update the linker script so that a
linker variable names __freertos_irq_stack_top has the same value as the top
of the stack used by main.  Using the linker script method will repurpose the
stack that was used by main before the scheduler was started for use as the
interrupt stack after the scheduler has started. */
#ifdef configISR_STACK_SIZE_WORDS
	static __attribute__ ((aligned(16))) StackType_t xISRStack[ configISR_STACK_SIZE_WORDS ] = { 0 };
	const StackType_t xISRStackTop = ( StackType_t ) &( xISRStack[ configISR_STACK_SIZE_WORDS & ~portBYTE_ALIGNMENT_MASK ] );

	/* Don't use 0xa5 as the stack fill bytes as that is used by the kernerl for
	the task stacks, and so will legitimately appear in many positions within
	the ISR stack. */
	#define portISR_STACK_FILL_BYTE	0xee	
#else
	extern const uint32_t __freertos_irq_stack_top[];
	const StackType_t xISRStackTop = ( StackType_t ) __freertos_irq_stack_top;
#endif

/*-----------------------------------------------------------*/

/* Set configCHECK_FOR_STACK_OVERFLOW to 3 to add ISR stack checking to task
stack checking.  A problem in the ISR stack will trigger an assert, not call the
stack overflow hook function (because the stack overflow hook is specific to a
task stack, not the ISR stack). */
#if defined( configISR_STACK_SIZE_WORDS ) && ( configCHECK_FOR_STACK_OVERFLOW > 2 )
	#warning This path not tested, or even compiled yet.

	static const uint8_t ucExpectedStackBytes[] = {
									portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE,		\
									portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE,		\
									portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE,		\
									portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE,		\
									portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE };	\

	#define portCHECK_ISR_STACK() configASSERT( ( memcmp( ( void * ) xISRStack, ( void * ) ucExpectedStackBytes, sizeof( ucExpectedStackBytes ) ) == 0 ) )
#else
	/* Define the function away. */
	#define portCHECK_ISR_STACK()
#endif /* configCHECK_FOR_STACK_OVERFLOW > 2 */

/*
 * Setup the timer to generate the tick interrupts.  The implementation in this
 * file is weak to allow application writers to change the timer used to
 * generate the tick interrupt.
 */
void vPortSetupTimerInterrupt( void ) __attribute__(( weak ));

static volatile uint64_t* mtime;
static volatile uint64_t* mtimecmp;
static uint32_t time_delta = configTICK_CLOCK_HZ / configTICK_RATE_HZ;

#if 1 // from guoqing
static volatile uint64_t mtime_base;
static volatile uint64_t mtimecmp_base;
static volatile uint32_t mie;
static volatile uint32_t mie_mte;

void vWakupTimerInterruptDisable(void)
{
	/* Disable timer interupt */
	__asm volatile("csrc mie,%0"::"r"(0x80));
}

void vWakupTimerInterruptEnable(void)
{
	/* Enable timer interupt */
	__asm volatile("csrs mie,%0"::"r"(0x80));
}

void vBackupCompenstateTime(void)
{
	mtimecmp_base = *mtimecmp;
	mtime_base = *mtime;
}

void vSetCompenstateTime(uint32_t ms)
{
	*mtimecmp = mtimecmp_base + (configTICK_CLOCK_HZ / configTICK_RATE_HZ * ms);
	*mtime = mtime_base + (configTICK_CLOCK_HZ / configTICK_RATE_HZ * ms);	
}

void vSetWakupTimerInterrupt(uint32_t ms)
{
	/* Disable timer interupt */
	__asm volatile("csrc mie,%0"::"r"(0x80));
	//for beak: beak is too slow, forbid ms = 1;
	*mtimecmp = *mtime+(configTICK_CLOCK_HZ / configTICK_RATE_HZ * ms);
	/* Enable timer interupt */
	__asm volatile("csrs mie,%0"::"r"(0x80));

}
#define TIMER1_CTRL_REG (STATION_SLOW_IO_TIMERS_BLOCK_REG_ADDR + 0x8)
#define TIMER1_LDCNT_REG (STATION_SLOW_IO_TIMERS_BLOCK_REG_ADDR)
#define TIMER1_CLK_FREQ (25000)
void vTimer1Init(void)
{
	// Program timer 1                          
	(*(uint64_t *) STATION_SLOW_IO_S2B_TIMERS_1_RESETN_ADDR) = 0x1;
	return;
}
void vTimer1Disable(void)
{
	(*(uint64_t *) TIMER1_CTRL_REG) = 0x0;
	return;
}
void vTimer1Enable(void)
{
	(*(uint64_t *) TIMER1_CTRL_REG) = 0x3;
	return;
}
void vSetWkupTimer1(uint32_t ms)
{
	(*(uint64_t *) TIMER1_LDCNT_REG) = TIMER1_CLK_FREQ * ms;                                                                                                                                                   
	(*(uint64_t *) TIMER1_CTRL_REG) = 0x3;
	return;
}
#endif

// TODO: overflow case
/* Sets the next timer interrupt
 * Reads previous timer compare register, and adds tickrate */
void vSetNextTimerInterrupt(void);
void vSetNextTimerInterrupt(void)
{
    *mtimecmp += time_delta;
}
/*-----------------------------------------------------------*/
/* Sets and enable the timer interrupt */
void vPortSetupTimerInterrupt(void)
{
	mtime =      (volatile uint64_t *)(configCLINT_BASE_ADDRESS + 0xbff8);
	mtimecmp =    (volatile uint64_t *)(configCLINT_BASE_ADDRESS + 0x4000);

    *mtimecmp = *mtime+time_delta;
	/* Enable timer interupt */
	__asm volatile("csrs mie,%0"::"r"(0x80));
}

BaseType_t xPortStartScheduler( void )
{
extern void xPortStartFirstTask( void );

	#if( configASSERT_DEFINED == 1 )
	{
		volatile uint32_t mtvec = 0;

		/* Check the least significant two bits of mtvec are 00 - indicating
		single vector mode. */
		__asm volatile( "csrr %0, mtvec" : "=r"( mtvec ) );
		configASSERT( ( mtvec & 0x03UL ) == 0 );

		/* Check alignment of the interrupt stack - which is the same as the
		stack that was being used by main() prior to the scheduler being
		started. */
		configASSERT( ( xISRStackTop & portBYTE_ALIGNMENT_MASK ) == 0 );

		#ifdef configISR_STACK_SIZE_WORDS
		{
			memset( ( void * ) xISRStack, portISR_STACK_FILL_BYTE, sizeof( xISRStack ) );
		}
		#endif	 /* configISR_STACK_SIZE_WORDS */
	}
	#endif /* configASSERT_DEFINED */

	/* If there is a CLINT then it is ok to use the default implementation
	in this file, otherwise vPortSetupTimerInterrupt() must be implemented to
	configure whichever clock is to be used to generate the tick interrupt. */
	vPortSetupTimerInterrupt();

	#if( configCLINT_BASE_ADDRESS != 0 )
	{
		/* Enable mtime and external interrupts.  1<<7 for timer interrupt, 1<<11
		for external interrupt.  _RB_ What happens here when mtime is not present as
		with pulpino? */
		__asm volatile( "csrs mie, %0" :: "r"(0x880) );
	}
	#else
	{
		/* Enable external interrupts. */
		__asm volatile( "csrs mie, %0" :: "r"(0x800) );
	}
	#endif /* configCLINT_BASE_ADDRESS */

	xPortStartFirstTask();

	/* Should not get here as after calling xPortStartFirstTask() only tasks
	should be executing. */
	return pdFAIL;
}
/*-----------------------------------------------------------*/

void vPortEndScheduler( void )
{
	/* exit here for demo use */
	extern void exit(int code);
	exit(0);
#if 0
	/* Not implemented. */
	for( ;; );
#endif
}


typedef struct exc_ctx
{
	uint64_t mepc;
	uint64_t x1;
	uint64_t x5;
	uint64_t x6;
	uint64_t x7;
	uint64_t x8;
	uint64_t x9;
	uint64_t x10;
	uint64_t x11;
	uint64_t x12;
	uint64_t x13;
	uint64_t x14;
	uint64_t x15;
	uint64_t x16;
	uint64_t x17;
	uint64_t x18;
	uint64_t x19;
	uint64_t x20;
	uint64_t x21;
	uint64_t x22;
	uint64_t x23;
	uint64_t x24;
	uint64_t x25;
	uint64_t x26;
	uint64_t x27;
	uint64_t x28;
	uint64_t x29;
	uint64_t x30;
	uint64_t x31;
} exc_ctx_t;

#include "util.h"
void print_exception_ctx(uint64_t mcause, uint64_t mepc, uint64_t mstatus, exc_ctx_t* ctx)
{
	RVHAL_OS_LOG_INFO("\nmstatus: %lld, mepc: %lld, mcause: %lld\n", mstatus, mepc, mcause);
	RVHAL_OS_LOG_INFO("\tra: %lld\n", ctx->x1);
	RVHAL_OS_LOG_INFO("\tt0: %lld\n", ctx->x5);
	RVHAL_OS_LOG_INFO("\tt1: %lld\n", ctx->x6);
	RVHAL_OS_LOG_INFO("\tt2: %lld\n", ctx->x7);
	RVHAL_OS_LOG_INFO("\ts0: %lld\n", ctx->x8);
	RVHAL_OS_LOG_INFO("\ts1: %lld\n", ctx->x9);
	RVHAL_OS_LOG_INFO("\na0: %lld\n", ctx->x10);
	RVHAL_OS_LOG_INFO("\na1: %lld\n", ctx->x11);
	RVHAL_OS_LOG_INFO("\na2: %lld\n", ctx->x12);
	RVHAL_OS_LOG_INFO("\na3: %lld\n", ctx->x13);
	RVHAL_OS_LOG_INFO("\na4: %lld\n", ctx->x14);
	RVHAL_OS_LOG_INFO("\na5: %lld\n", ctx->x15);
	RVHAL_OS_LOG_INFO("\na6: %lld\n", ctx->x16);
	RVHAL_OS_LOG_INFO("\na7: %lld\n", ctx->x17);
	RVHAL_OS_LOG_INFO("\ts2: %lld\n", ctx->x18);
	RVHAL_OS_LOG_INFO("\ts3: %lld\n", ctx->x19);
	RVHAL_OS_LOG_INFO("\ts4: %lld\n", ctx->x20);
	RVHAL_OS_LOG_INFO("\ts5: %lld\n", ctx->x21);
	RVHAL_OS_LOG_INFO("\ts6: %lld\n", ctx->x22);
	RVHAL_OS_LOG_INFO("\ts7: %lld\n", ctx->x23);
	RVHAL_OS_LOG_INFO("\ts8: %lld\n", ctx->x24);
	RVHAL_OS_LOG_INFO("\ts9: %lld\n", ctx->x25);
	RVHAL_OS_LOG_INFO("\ts10: %lld\n", ctx->x26);
	RVHAL_OS_LOG_INFO("\ts11: %lld\n", ctx->x27);
	RVHAL_OS_LOG_INFO("\tt3: %lld\n", ctx->x28);
	RVHAL_OS_LOG_INFO("\tt4: %lld\n", ctx->x29);
	RVHAL_OS_LOG_INFO("\tt5: %lld\n", ctx->x30);
	RVHAL_OS_LOG_INFO("\tt6: %lld\n", ctx->x31);

	while(1);
}

