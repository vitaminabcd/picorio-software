#include <stdint.h>
#include <string.h>
#include "syscalls.h"
//#include "encoding.h"
#include "clib.h"

#ifdef TOHOST_IN_HTIF_SECTION
volatile uint64_t tohost __attribute__((section(".htif")));
volatile uint64_t fromhost __attribute__((section(".htif")));
#else
volatile uint64_t tohost __attribute__((aligned(64)));
volatile uint64_t fromhost __attribute__((aligned(64)));
#endif

static uint64_t zeroExtend(long val)
{
	uint64_t ret = val;
	#if __riscv_xlen == 32
		ret = (0x00000000ffffffff & val);
	#endif
	return ret;
}

/* Relay syscall to host */
uint64_t vSyscallToHost(long which, long arg0, long arg1, long arg2)
{
	volatile uint64_t magic_mem[8] __attribute__((aligned(64)));
    volatile uint64_t oldfromhost;
	magic_mem[0] = zeroExtend(which);
	magic_mem[1] = zeroExtend(arg0);
	magic_mem[2] = zeroExtend(arg1);
	magic_mem[3] = zeroExtend(arg2);
	__sync_synchronize(); 
    tohost = zeroExtend((long)magic_mem);
    do
    {
        oldfromhost = fromhost;
		if (oldfromhost) {
			fromhost = 0;
			break;
		}
    } while (oldfromhost == 0);
	return magic_mem[0];
}
/*-----------------------------------------------------------*/

/* Exit systemcall */
static void prvSyscallExit(long code __attribute__((unused)))
{
#if 0
	uint64_t zcode = zeroExtend(code);
	tohost = ((zcode << 1) | 1);
	for(;;) { }
#else
  while (1) {
    fromhost = 0;
    tohost = 1;
  }
#endif
}

void exit(int code)
{
	prvSyscallExit(code);
}

/*-----------------------------------------------------------*/

/*-----------------------------------------------------------*/

/* Programs need to override this function. */
int __attribute__((weak)) main(__attribute__ ((unused)) int argc, __attribute__ ((unused)) char** argv)
{
	printf("Implement a main function!\n");
	return -1;
}
/*-----------------------------------------------------------*/

/* Starts main function. */
void vSyscallInit(void)
{
	int ret = main(0, 0);
	exit(ret);
}
/*-----------------------------------------------------------*/

/*-----------------------------------------------------------*/
