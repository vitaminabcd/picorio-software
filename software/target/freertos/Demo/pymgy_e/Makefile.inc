# File to be included in all makefiles

TARGET=riscv64-unknown-linux-gnu
#-----------------------------------------------------------
GCC		= $(TARGET)-gcc
OBJCOPY	= $(TARGET)-objcopy
OBJDUMP	= $(TARGET)-objdump
STRIP	= $(TARGET)-strip
ELF2HEX  = elf2hex
AR		= $(TARGET)-ar
RANLIB	= $(TARGET)-ranlib

PROG	= freertos
CRT0	= arch/boot.S

FREERTOS_SOURCE_DIR	= ../../Source
CUNIT	= ../../../cunit

#-----------------------------------------------------------
WARNINGS= -Wall -Wextra -Wshadow -Wpointer-arith -Wbad-function-cast -Wcast-align -Wsign-compare \
		-Waggregate-return -Wstrict-prototypes -Wmissing-prototypes -Wmissing-declarations -Wunused

