#include "FreeRTOS.h"
#include "common.h"
#include "util.h"

int rvhal_log_mask = RVHAL_LOG_MASK_DEFAULT;
int rvhal_log_level = RVHAL_LOG_LEVEL_DEFAULT;
int rvhal_dump_hex_on = RVHAL_DUMP_HEX_ON_DEFAULT;

void rvHal_set_log_mask(int mask)
{
	rvhal_log_mask = mask;
}

void rvHal_set_log_level(int level)
{
	rvhal_log_level = level;
}

void rvHal_assert_failed(unsigned char *file, unsigned int line)
{
    RVHAL_HAL_LOG_ERR( "Wrong parameters value: file %s on line %lu\n", ( const char* )file, line );

    while( 1 ) {
    }
}

void rvHal_dump_hex ( unsigned char* buf, unsigned int size, char *description, ... )
{
    const unsigned char *p = buf;
    unsigned i, j;
	unsigned char dbuf[BUF_SIZE_DUMP];
	struct putchar_data data;
	data.buf = dbuf;
	data.bufsize = sizeof(dbuf);
	data.buflen = 0;

    printf(description);
    
    while ( 1 ) {
		data.end = 0;
        if ( 0 == size ) {
            break;
        }

        i = ( size >= 16 ) ? 16 : size;
        size -= i;

        for ( j = 0; j < i; j++ ) {
            ddprintf (&data, "%02x%s", p[j], ( j == 7 ) ? ( "  " ) : ( " " ) );
        }

        for ( j = i; j < 16 ; j++ ) {
            ddprintf (&data, "%s%s", "  ", ( j == 7 ) ? ( "  " ) : ( " " ) );
        }

        ddprintf (&data, " | " );

        for ( j = 0; j < i; j++ ) {
            ddprintf (&data, "%c%s", isprint ( p[j] ) ? p[j] : '.', ( j == 7 ) ? ( " " ) : ( "" ) );
        }

		data.end = 1;
        ddprintf (&data, "\n" );

		p += i;
    }
	//data.end = 1;

//    RVHAL_HAL_LOG_DEBUG ( "\n" );
}
