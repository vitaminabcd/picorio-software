#include <string.h>
#include "hal.h"
#include "clib.h"

/*
 * The external irq controller is PLIC for RISC-V
*/

#define DBG_EN_BASE					STATION_DMA_S2B_PLIC_DBG_EN_ADDR__DEPTH_0
#define INTR_EN_BASE				STATION_DMA_S2B_PLIC_INTR_EN_ADDR__DEPTH_0
#define INTR_CORE_ID_BASE			STATION_DMA_S2B_PLIC_INTR_CORE_ID_ADDR__DEPTH_0
#define INTR_SRC_BASE				STATION_DMA_B2S_PLIC_INTR_SRC_ADDR__DEPTH_0

#define INTR_CORE_ID_ORV32     4 // TODO: for pygmy_e: 0

#define PLIC_REG_BYTES	8

#define PLIC_REG_USE_32BIT 
#ifdef PLIC_REG_USE_32BIT
#define readPlicReg(addr)			read32(addr)
#define writePlicReg(addr, value)	write32(addr, value)
#else
#define readPlicReg(addr)			read64(addr)
#define writePlicReg(addr, value)	write64(addr, value)
#endif

#define RegIntrEn(irq)		(INTR_EN_BASE + ((irq) * PLIC_REG_BYTES))
#define RegIntrCoreId(irq)	(INTR_CORE_ID_BASE + ((irq) * PLIC_REG_BYTES))
#define RegDbgEn(irq)		(DBG_EN_BASE + ((irq) * PLIC_REG_BYTES))
#define RegSrc(hartid)		(INTR_SRC_BASE + ((hartid) * PLIC_REG_BYTES))

static struct irq_ext_handler_t *irq_ext_handlers[NUM_IRQ_EXT_MAX];

void __rvHal_irq_ext_restore(void)
{
	   int i;
	   for (i=0; i<NUM_IRQ_EXT_MAX; i++)
	   {
			   writePlicReg(RegIntrEn(i), 0x1);
		   //writePlicReg(RegIntrEn(i), plic_reg_en_backup[i]);
	   }
	   __asm volatile( "csrs mie, %0" :: "r"(0x800) );
}

void __rvHal_irq_ext_init(void)
{
	int i;

	memset(irq_ext_handlers, 0, sizeof(ARRAY_SIZE(irq_ext_handlers)));

	for (i=0; i<NUM_IRQ_EXT_MAX; i++)
	{
		writePlicReg(RegDbgEn(i), 0);
		writePlicReg(RegIntrEn(i), 0);
		writePlicReg(RegIntrCoreId(i), INTR_CORE_ID_ORV32);
	}

	__asm volatile( "csrs mie, %0" :: "r"(0x800) );
}

void __rvHal_request_irq_ext(unsigned int irq, struct irq_ext_handler_t *handler)
{
	if (irq < NUM_IRQ_EXT_MAX)
	{
		irq_ext_handlers[irq] = handler;
		writePlicReg(RegIntrEn(irq), 1);
	}
	else
	{
		RVHAL_HAL_LOG_ERR("Error: [%s] irq number is out of range\n", __func__);
		/* exit */
	}
}

void __rvHal_free_irq_ext(unsigned int irq)
{
	if (irq < NUM_IRQ_EXT_MAX)
	{
		writePlicReg(RegIntrEn(irq), 0);
		irq_ext_handlers[irq] = NULL;
	}
	else
	{
		RVHAL_HAL_LOG_ERR("Error: [%s] irq number is out of range\n", __func__);
		/* exit */
	}
}

unsigned int rvHal_irq_check_pending(void)
{
        unsigned int irq;
        irq = readPlicReg(RegSrc(0));

        RVHAL_HAL_LOG_DEBUG("%s: irq=%lu\n", __func__, irq);

        return irq;
}



void rvHal_irq_ext_interrupt(void)
{
	unsigned int irq;
	irq = readPlicReg(RegSrc(INTR_CORE_ID_ORV32));

	//RVHAL_HAL_LOG_DEBUG("%s: irq=%lu\n", __func__, irq);

	if (irq < NUM_IRQ_EXT_MAX)
	{
		if (irq_ext_handlers[irq])
		{
			(irq_ext_handlers[irq])->hook();
		}
		else
		{
			RVHAL_HAL_LOG_ERR("Error: [%s] irq[%d] has no handler\n", __func__, irq);
		}
	}
	else
	{
		RVHAL_HAL_LOG_ERR("Error: [%s] irq[%d] number is out of range\n", __func__, irq);
		/* exit */
	}
}

