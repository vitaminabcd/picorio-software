#include "hal.h"
#include "hal_rtc.h"
#include "clib.h"

uint32_t rtc_cnt=0;

static void rtc_cfg(struct rtc_cfg_struct *cfg)
{
	unsigned int regval=0;

	//get current cnt
	regval = read32(RTC_CCVR);
	regval = cfg->setalarm + regval; 
	write32(RTC_CMR, regval);
#if 1	
	regval = read32(RTC_CCR);
	regval &= ~RTC_CCR_MASK;
	regval |= BIT(4);
	regval |= RTC_CCR_IE;
	write32(RTC_CCR,regval);
#endif

#if 1
	regval = read32(RTC_CCR);
	RVHAL_OS_LOG_DEBUG("%s: RTC_CCR (%lu)\r\n", __func__, regval);
	regval = read32(RTC_CLR);
	RVHAL_OS_LOG_DEBUG("%s: RTC_CLR (%lu)\r\n", __func__, regval);
	regval = read32(RTC_CMR);
	RVHAL_OS_LOG_DEBUG("%s: RTC_CMR (%lu)\r\n", __func__, regval);
	regval = read32(RTC_CCVR);
	RVHAL_OS_LOG_DEBUG("%s: RTC_CCVR (%lu)\r\n", __func__, regval);
	regval = read32(RTC_EOI);
	RVHAL_OS_LOG_DEBUG("%s: RTC_EOI (%lu)\r\n", __func__, regval);
#endif
}

static void rtc_irq_handler(void)
{
	int regval;

	if (!(read32(RTC_STAT) & RTC_STAT_BIT))
		return;

	/* Clear interrupt */
	regval = read32(RTC_EOI);

	regval = read32(RTC_CCVR);	

	RVHAL_OS_LOG_DEBUG("%s: ccvr (%lu)\r\n", __func__, regval);

#if 1
        regval = read32(RTC_CCR);
        regval |= RTC_CCR_MASK;
        regval &= ~RTC_CCR_IE;
        write32(RTC_CCR,regval);
#endif	
	//write32(RTC_CMR, regval+0x3000000);
	//rtc_cnt++;
}

static struct irq_ext_handler_t ext_irq_rtc_handler = {
	.hook		= rtc_irq_handler,
};

void __rvHal_rtc_init(void)
{
	struct rtc_cfg_struct cfg;
	int regval;

	RVHAL_OS_LOG_DEBUG("%s: %d\n", __func__, __LINE__);
#if 0
	write64(RTC_CLOCK_EN, 1UL);
	write64(RTC_EN, 1UL);
#endif

	write32(RTC_PRESCALAR, PRESCALAR_CFG);
	write32(RTC_CCR, RTC_PSCLR_EN | RTC_CCR_EN);
	
	regval = read32(RTC_PRESCALAR);
	RVHAL_OS_LOG_DEBUG("%s: RTC_PRESCALAR (%lu)\r\n", __func__, regval);
	regval = read32(RTC_CPCVR);
	RVHAL_OS_LOG_DEBUG("%s: RTC_CPCVR (%lu)\r\n", __func__, regval);
	regval = read32(RTC_CCR);
	RVHAL_OS_LOG_DEBUG("%s: RTC_CCR (%lu)\r\n", __func__, regval);
#if 0	
	cfg.setalarm=0x800;//0x300000
//	cfg.mask=0x0;
//	cfg.start=0x1;

	rtc_cfg(&cfg);

	__rvHal_request_irq_ext(NUM_IRQ_EXT_RTC, &ext_irq_rtc_handler);
#endif	
}

void rvHal_SetWakupRtcInterrupt(uint32_t ms)
{
	struct rtc_cfg_struct cfg;
	RVHAL_OS_LOG_DEBUG("%s: %d: ms = %d \n", __func__, __LINE__, ms);
	
	/* Set wakeup rtc */
	cfg.setalarm = (ms);

	rtc_cfg(&cfg);
	/* Enable timer interupt */
}

void rvHal_RtcGetCnt(uint32_t *cnt)
{
	*cnt = read32(RTC_CCVR);	
	//RVHAL_OS_LOG_DEBUG("%s: ccvr (%lu)\r\n", __func__, *cnt);
}

void rvHal_RtcDisable(void)
{
	int regval;
	
	regval = read32(RTC_CCR);
        regval |= RTC_CCR_MASK;
        regval &= ~RTC_CCR_IE;
        write32(RTC_CCR,regval);
	
	/* Clear interrupt */
	regval = read32(RTC_EOI);
	RVHAL_OS_LOG_DEBUG("%s:rtc pending = %x \n", __func__, regval);

}

void rtc_wakeup(void)
{
	struct rtc_cfg_struct cfg;

	RVHAL_OS_LOG_DEBUG("%s: %d\n", __func__, __LINE__);
	
	cfg.setalarm=0x10000;//0x300000;
//	cfg.mask=0x0;
//	cfg.start=0x1;
	//RVHAL_OS_LOG_DEBUG("rtc_init: %d\n", __LINE__);

	rtc_cfg(&cfg);
	
	//RVHAL_OS_LOG_DEBUG("%s: enter low power mode: before disable mstatus.mie  %d\n", __func__, __LINE__);
	asm volatile(
			"csrw mstatus, %0 \n\t" ::"rK"(0x1800)
			);
	//RVHAL_OS_LOG_DEBUG("%s: enter low power mode: after enable device int  %d\n", __func__, __LINE__);
	(*(uint64_t *) STATION_SLOW_IO_SCU_LP_MODE_ADDR) = 0x3;
	asm volatile("wfi \n\t");


	asm volatile(
			"csrw mstatus, %0 \n\t" ::"rK"(0x1808)
			);
}

#if 0
void rtc_loop(void)
{
	unsigned int i, regval=0, regval_new=0;
	while(1)
	{
		regval_new=read32(RTC_CCVR);
		
		//for (i=0; i<100000; i++)
		//{;}
		//RVHAL_OS_LOG_DEBUG("get CCVR1, 0x%lx\n", regval_new);
		//regval_new=read32(RTC_CCVR);
		if (regval != regval_new) {
			RVHAL_OS_LOG_DEBUG("get CCVR, 0x%lx - 0x%lx\n", regval, regval_new);
			regval=regval_new;
		}
	}
}
#endif
