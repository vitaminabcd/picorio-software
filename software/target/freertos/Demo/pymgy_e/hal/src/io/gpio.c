#include <string.h>
#include "FreeRTOS.h"
#include "common.h"
#include "rtlgen.h"
#include "hal.h"
#include "clib.h"

static unsigned int reg_gpio = STATION_SLOW_IO_GPIO_BLOCK_REG_ADDR;
static unsigned int reg_mux = STATION_SLOW_IO_SSP_SHARED_SEL_0_ADDR;

#define GPIO_SWPORTA_DR     (0x00)
#define GPIO_SWPORTA_DDR    (0x04)
#define GPIO_SWPORTB_DR     (0x0c)
#define GPIO_SWPORTB_DDR    (0x10)
#define GPIO_SWPORTC_DR     (0x18)
#define GPIO_SWPORTC_DDR    (0x1c)
#define GPIO_SWPORTD_DR     (0x24)
#define GPIO_SWPORTD_DDR    (0x28)
#define GPIO_INTEN          (0x30)
#define GPIO_INTMASK        (0x34)
#define GPIO_INTTYPE_LEVEL  (0x38)
#define GPIO_INT_POLARITY   (0x3c)
#define GPIO_INTSTATUS      (0x40)
#define GPIO_RAW_INTSTATUS  (0x44)
#define GPIO_DEBOUNCE       (0x48)
#define GPIO_PORTA_EOI      (0x4c)
#define GPIO_EXT_PORTA      (0x50)
#define GPIO_EXT_PORTB      (0x54)
#define GPIO_EXT_PORTC      (0x58)
#define GPIO_EXT_PORTD      (0x5c)
#define GPIO_LS_SYNC        (0x60)
#define GPIO_ID_CODE        (0x64)
#define GPIO_INT_BOTHEDGE   (0x68)
#define GPIO_VER_ID_CODE    (0x6c)
#define GPIO_CONFIG_REG2    (0x70)
#define GPIO_CONFIG_REG1    (0x74)

#define NUM_PIN_PER_GPIO_GROUP_MAX		(32) // TODO:
#if 0
static void vGpioTestTask( void *pvParameters )
{
//	printf("ready to test gpio for dead loop, every 5s toggle\n");
	while(1) {
//	printf("GPIO 1 ---------\n");
	      write32(reg_gpio+GPIO_SWPORTA_DR, 0xf0000000);
		  rvHal_sleep(5000);

//	printf("GPIO 0 ---------\n");
	      write32(reg_gpio+GPIO_SWPORTA_DR, 0x00000000);
		  rvHal_sleep(5000);
	}
}
#endif

static struct irq_gpio_handler_t *irq_gpio_handlers[NUM_PIN_PER_GPIO_GROUP_MAX];// TODO:

void __rvHal_request_irq_gpio(unsigned int pin, struct irq_gpio_handler_t *handler)
{
	if (pin < NUM_PIN_PER_GPIO_GROUP_MAX)
	{
		irq_gpio_handlers[pin] = handler;
	}
	else
	{
		RVHAL_HAL_LOG_ERR("Error: [%s] pin number is out of range\n", __func__);
		/* exit */
	}
}

void __rvHal_free_irq_gpio(unsigned int pin)
{
	if (pin < NUM_PIN_PER_GPIO_GROUP_MAX)
	{
		irq_gpio_handlers[pin] = NULL;
	}
	else
	{
		RVHAL_HAL_LOG_ERR("Error: [%s] pin number is out of range\n", __func__);
		/* exit */
	}
}

static void gpio_irq_handler(void)
{
	struct irq_gpio_handler_t * handler = NULL;
	uint32_t status = 0;
	status = read32(reg_gpio+GPIO_INTSTATUS);

//	RVHAL_HAL_LOG_DEBUG("gpio interrupt status: 0x%x\n", status);

	for(int i=0; i<32; i++) // TODO:
	{
		if ((status & BIT(i)) && (handler = irq_gpio_handlers[i]))
		{
//			RVHAL_HAL_LOG_DEBUG("gpio interrupt : %d - 0x%x\n", i, (irq_gpio_handlers[i])->hook);
			handler->hook(handler->context);
			break;
		}
	}

	write32(reg_gpio+GPIO_PORTA_EOI, status);
}

static struct irq_ext_handler_t ext_irq_gpio_handler = {
	.hook		= gpio_irq_handler,
};

static void gpio_request_irq_lora(unsigned int pin, unsigned int lvl, unsigned int pol, struct irq_gpio_handler_t *handler)
{
	unsigned int pin_bit = BIT(pin), 
	reg_lvl = read32(reg_gpio+GPIO_INTTYPE_LEVEL),
	reg_pol = read32(reg_gpio+GPIO_INT_POLARITY),
	reg_inten = read32(reg_gpio+GPIO_INTEN);

	if (NO_IRQ != lvl)
		reg_lvl |= pin_bit;
	else
		reg_lvl &=~ pin_bit;

	if ((IRQ_HIGH_PRIORITY == pol) || (IRQ_VERY_HIGH_PRIORITY == pol))
		reg_pol |= pin_bit;
	else
		reg_pol &=~ pin_bit;

	__rvHal_request_irq_gpio(pin, handler);

	write32(reg_gpio+GPIO_INTTYPE_LEVEL, reg_lvl);
	write32(reg_gpio+GPIO_INT_POLARITY, reg_pol);
    write32(reg_gpio+GPIO_INTEN, reg_inten | pin_bit);
}

static void gpio_request_irq(unsigned int pin, unsigned int level, unsigned int polarity, struct irq_gpio_handler_t *handler)
{
	unsigned int pin_bit = BIT(pin), 
	reg_lvl = read32(reg_gpio+GPIO_INTTYPE_LEVEL),
	reg_pol = read32(reg_gpio+GPIO_INT_POLARITY),
	reg_inten = read32(reg_gpio+GPIO_INTEN);

	if (GPIO_INT_TYPE_EDGE == level)
		reg_lvl |= pin_bit;
	else
		reg_lvl &=~ pin_bit;

	if (GPIO_INT_POLARITY_HIGH == polarity)
		reg_pol |= pin_bit;
	else
		reg_pol &=~ pin_bit;

	__rvHal_request_irq_gpio(pin, handler);

	write32(reg_gpio+GPIO_INTTYPE_LEVEL, reg_lvl);
	write32(reg_gpio+GPIO_INT_POLARITY, reg_pol);
    write32(reg_gpio+GPIO_INTEN, reg_inten | pin_bit);
}

static void gpio_free_irq(unsigned int pin)
{
	__rvHal_free_irq_gpio(pin);

    write32(reg_gpio+GPIO_INTEN, read32(reg_gpio+GPIO_INTEN) & ~(BIT(pin)));
}

void __rvHal_gpio_init(void)
{
	int i, tmp;
#if 0
	for (i=28; i<32; i++) {
		write64(reg_mux+i*8, 1UL);
	}
#endif
	tmp=read32(reg_gpio+GPIO_INTEN);//
	tmp=read32(reg_gpio+GPIO_PORTA_EOI);//closeandclrformerint

    write32(reg_gpio+GPIO_INTEN,0x0);//
    write32(reg_gpio+GPIO_PORTA_EOI,0xffffffff);//closeandclrformerint
    write32(reg_gpio+GPIO_SWPORTA_DR,0);
    write32(reg_gpio+GPIO_SWPORTA_DDR,0);

    write32(reg_gpio+GPIO_INTMASK,0x0);
    write32(reg_gpio+GPIO_LS_SYNC,0x0);

	memset(irq_gpio_handlers, 0, sizeof(ARRAY_SIZE(irq_gpio_handlers)));
	__rvHal_request_irq_ext(NUM_IRQ_EXT_GPIO, &ext_irq_gpio_handler);
}

#if 1
void rvHal_gpio_init( struct gpio_desc *dgpio, unsigned int pin, unsigned int type, unsigned int value )
{
	RVHAL_ASSERT_PARAM(dgpio && pin && (pin<32)); // TODO:

	write64(reg_mux+pin*8, 1UL); // TODO: mux

	unsigned int pin_bit = BIT(pin), 
	ddr = read32(reg_gpio+GPIO_SWPORTA_DDR);
	
	dgpio->pin = pin;

	if (GPIO_PIN_INPUT == type) { // TODO:
	    write32(reg_gpio+GPIO_SWPORTA_DDR, ddr & (~pin_bit));
	} else if (GPIO_PIN_OUTPUT == type) {
		unsigned int dr = read32(reg_gpio+GPIO_SWPORTA_DR);
	    write32(reg_gpio+GPIO_SWPORTA_DDR, ddr | pin_bit);
	    write32(reg_gpio+GPIO_SWPORTA_DR, value ? (dr|pin_bit) : (dr & ~pin_bit));
	} else {
		RVHAL_ASSERT_PARAM(0);
	}
}

void rvHal_gpio_set_interrupt( struct gpio_desc *dgpio, unsigned int level, unsigned int polarity, void (*irqHandler)(void*), void *context)
{
	RVHAL_ASSERT_PARAM(dgpio); // TODO:

	dgpio->handler.hook = irqHandler;
	dgpio->handler.context = context;

	gpio_request_irq(dgpio->pin, level, polarity, &dgpio->handler);
}

void rvHal_gpio_remove_interrupt( struct gpio_desc *dgpio )
{
	gpio_free_irq(dgpio->pin);
}

void rvHal_gpio_write( struct gpio_desc *dgpio, unsigned int value )
{
	unsigned int pin_bit = BIT(dgpio->pin),
	dr = read32(reg_gpio+GPIO_SWPORTA_DR);

    write32(reg_gpio+GPIO_SWPORTA_DR, value ? (dr|pin_bit) : (dr & ~pin_bit));
}


unsigned int rvHal_gpio_read( struct gpio_desc *dgpio )
{
    return !!(read32(reg_gpio+GPIO_EXT_PORTA) & (BIT(dgpio->pin)));
}

void rvHal_gpio_toggle( struct gpio_desc *dgpio )
{
    rvHal_gpio_write( dgpio, rvHal_gpio_read( dgpio ) ^ 1 );
}

void GpioInit( Gpio_t *obj, PinNames pin, PinModes mode,  PinConfigs config, PinTypes type, uint32_t value )
{
	RVHAL_ASSERT_PARAM(pin && (pin<32)); // TODO:

	write64(reg_mux+pin*8, 1UL); // TODO: mux

	unsigned int pin_bit = BIT(pin), 
	ddr = read32(reg_gpio+GPIO_SWPORTA_DDR);
	
	obj->pin = pin;
	obj->port = (void *)reg_gpio; // TODO:

	if (PIN_INPUT == mode || PIN_NO_PULL == type) { // TODO:
	    write32(reg_gpio+GPIO_SWPORTA_DDR, ddr & (~pin_bit));
	} else if (PIN_OUTPUT == mode) {
		unsigned int dr = read32(reg_gpio+GPIO_SWPORTA_DR);
	    write32(reg_gpio+GPIO_SWPORTA_DDR, ddr | pin_bit);
	    write32(reg_gpio+GPIO_SWPORTA_DR, value ? (dr|pin_bit) : (dr & ~pin_bit));
	} else {
		RVHAL_ASSERT_PARAM(0);
	}
}

void GpioSetContext( Gpio_t *obj, void* context )
{
    obj->Context = context;
}

void GpioSetInterrupt( Gpio_t *obj, IrqModes irqMode, IrqPriorities irqPriority, void (*irqHandler)(void*))
{
	obj->handler.hook = irqHandler;

	gpio_request_irq_lora(obj->pin, IRQ_RISING_EDGE, IRQ_HIGH_PRIORITY, &obj->handler);
}

void GpioRemoveInterrupt( Gpio_t *obj )
{
	gpio_free_irq(obj->pin);
}

void GpioWrite( Gpio_t *obj, uint32_t value )
{
	unsigned int pin_bit = BIT(obj->pin),
	dr = read32(reg_gpio+GPIO_SWPORTA_DR);

    write32(reg_gpio+GPIO_SWPORTA_DR, value ? (dr|pin_bit) : (dr & ~pin_bit));
}


uint32_t GpioRead( Gpio_t *obj )
{
    return !!(read32(reg_gpio+GPIO_EXT_PORTA) & (BIT(obj->pin)));
}

void GpioToggle( Gpio_t *obj )
{
    GpioWrite( obj, GpioRead( obj ) ^ 1 );
}

#endif
