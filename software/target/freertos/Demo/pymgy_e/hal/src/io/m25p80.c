// RIVAI: GPL-2.0-only
#if defined _M25P80_
#include "spi.h"
#include "m25p80.h"

/* Maximum value an `unsigned int' can hold.  (Minimum is 0.)  */
#  define UINT_MAX      4294967295U
struct m25p {
	struct spi_mem		*spimem;
	struct spi_nor		spi_nor;
};

static int m25p80_read_reg(struct spi_nor *nor, u8 code, u8 *val, int len)
{
	struct spi_mem *spi_mem_priv =  nor->priv;
	struct spi_mem_op op = SPI_MEM_OP(SPI_MEM_OP_CMD(code, 1),
					  SPI_MEM_OP_NO_ADDR,
					  SPI_MEM_OP_NO_DUMMY,
					  SPI_MEM_OP_DATA_IN(len, NULL, 1));
	void *scratchbuf;
	int ret;

	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
	scratchbuf = (void *)val;

	op.data.buf.in = scratchbuf;
	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. len = %d \n", __func__, __LINE__, len);
	ret = spi_mem_exec_op(spi_mem_priv, &op);
	if (ret < 0)
		//RVHAL_OS_LOG_DEBUG("error %d reading %x\n", ret, code);

	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. len = %d \n", __func__, __LINE__, len);

	return ret;
}

static int m25p80_write_reg(struct spi_nor *nor, u8 opcode, u8 *buf, int len)
{
	struct spi_mem *spi_mem_priv =  nor->priv;
	struct spi_mem_op op = SPI_MEM_OP(SPI_MEM_OP_CMD(opcode, 1),
					  SPI_MEM_OP_NO_ADDR,
					  SPI_MEM_OP_NO_DUMMY,
					  SPI_MEM_OP_DATA_OUT(len, NULL, 1));
	void *scratchbuf;
	int ret;

	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
	scratchbuf = (void *)buf;

	op.data.buf.out = scratchbuf;
	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. len = %d \n", __func__, __LINE__, len);
       // print_hex_dump_bytes("before write data: ",
         //                    DUMP_PREFIX_OFFSET, scratchbuf, len);	
	ret = spi_mem_exec_op(spi_mem_priv, &op);

	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. len = %d \n", __func__, __LINE__, len);
	return ret;
}

static ssize_t m25p80_write(struct spi_nor *nor, loff_t to, size_t len,
			    const u_char *buf)
{
	struct spi_mem *spi_mem_priv =  nor->priv;
	struct spi_mem_op op =
			SPI_MEM_OP(SPI_MEM_OP_CMD(nor->program_opcode, 1),
				   SPI_MEM_OP_ADDR(nor->addr_width, to, 1),
				   SPI_MEM_OP_NO_DUMMY,
				   SPI_MEM_OP_DATA_OUT(len, buf, 1));
	int ret;

	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
	/* get transfer protocols. */
	op.cmd.buswidth = spi_nor_get_protocol_inst_nbits(nor->write_proto);
	op.addr.buswidth = spi_nor_get_protocol_addr_nbits(nor->write_proto);
	op.data.buswidth = spi_nor_get_protocol_data_nbits(nor->write_proto);

	if (nor->program_opcode == SPINOR_OP_AAI_WP && nor->sst_write_second)
		op.addr.nbytes = 0;

	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
	ret = spi_mem_adjust_op_size(spi_mem_priv, &op);
	if (ret)
		return ret;
	op.data.nbytes = len < op.data.nbytes ? len : op.data.nbytes;

	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
	ret = spi_mem_exec_op(spi_mem_priv, &op);
	if (ret)
		return ret;

	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
	return op.data.nbytes;
}

static ssize_t m25p80_eeprom_write(struct spi_nor *nor, loff_t to, size_t len,
			    const u_char *buf)
{
	struct spi_mem *spi_mem_priv =  nor->priv;
	struct spi_mem_op op =
			SPI_MEM_OP(SPI_MEM_OP_CMD(nor->eeprom_program_opcode, 1),
				   SPI_MEM_OP_ADDR(nor->addr_width, to, 1),
				   SPI_MEM_OP_NO_DUMMY,
				   SPI_MEM_OP_DATA_OUT(len, buf, 1));
	int ret;

	RVHAL_OS_LOG_DEBUG("func: %s, line: %d. nor->eeprom_program_opcode = %x \n", __func__, __LINE__, nor->eeprom_program_opcode);
	/* get transfer protocols. */
	op.cmd.buswidth = spi_nor_get_protocol_inst_nbits(nor->eeprom_write_proto);
	op.addr.buswidth = spi_nor_get_protocol_addr_nbits(nor->eeprom_write_proto);
	op.data.buswidth = spi_nor_get_protocol_data_nbits(nor->eeprom_write_proto);

	if (nor->eeprom_program_opcode == SPINOR_OP_AAI_WP && nor->sst_write_second)
		op.addr.nbytes = 0;

	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
	ret = spi_mem_adjust_op_size(spi_mem_priv, &op);
	if (ret)
		return ret;
	op.data.nbytes = len < op.data.nbytes ? len : op.data.nbytes;

	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
	ret = spi_mem_exec_op(spi_mem_priv, &op);
	if (ret)
		return ret;

	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
	return op.data.nbytes;
}
/*
 * Read an address range from the nor chip.  The address range
 * may be any size provided it is within the physical boundaries.
 */
static ssize_t m25p80_read(struct spi_nor *nor, loff_t from, size_t len,
			   u_char *buf)
{
	struct spi_mem *spi_mem_priv =  nor->priv;
	struct spi_mem_op op =
			SPI_MEM_OP(SPI_MEM_OP_CMD(nor->read_opcode, 1),
				   SPI_MEM_OP_ADDR(nor->addr_width, from, 1),
				   SPI_MEM_OP_DUMMY(nor->read_dummy, 1),
				   SPI_MEM_OP_DATA_IN(len, buf, 1));
	size_t remaining = len;
	int ret;

	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
	/* get transfer protocols. */
	op.cmd.buswidth = spi_nor_get_protocol_inst_nbits(nor->read_proto);
	op.addr.buswidth = spi_nor_get_protocol_addr_nbits(nor->read_proto);
	op.dummy.buswidth = op.addr.buswidth;
	op.data.buswidth = spi_nor_get_protocol_data_nbits(nor->read_proto);

	/* convert the dummy cycles to the number of bytes */
	op.dummy.nbytes = (nor->read_dummy * op.dummy.buswidth) / 8;

	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
	while (remaining) {
		op.data.nbytes = remaining < UINT_MAX ? remaining : UINT_MAX;
		//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
		ret = spi_mem_adjust_op_size(spi_mem_priv, &op);
		if (ret)
			return ret;

		//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
		ret = spi_mem_exec_op(spi_mem_priv, &op);
		if (ret)
			return ret;

		op.addr.val += op.data.nbytes;
		//RVHAL_OS_LOG_DEBUG("func: %s, line: %d op.data.nbytes = 0x%x \n", __func__, __LINE__, op.data.nbytes);
		remaining -= op.data.nbytes;
		op.data.buf.in += op.data.nbytes;
	}
	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);

	return len;
}

/*
 * board specific setup should have ensured the SPI clock used here
 * matches what the READ command supports, at least until this driver
 * understands FAST_READ (for clocks over 25 MHz).
 */
static int m25p_init(struct spi_mem *spimem)
{
	struct spi_device *spi = spimem->spi;
	struct spi_nor *nor = &(((struct m25p *)spimem->drvpriv)->spi_nor);
	struct spi_nor_hwcaps hwcaps = {
		.mask = SNOR_HWCAPS_READ |
			SNOR_HWCAPS_READ_FAST |
			SNOR_HWCAPS_PP | 
			SNOR_HWCAPS_EEPR_PP,
	};
	int ret;


	/* install the hooks */
	nor->read = m25p80_read;
	nor->write = m25p80_write;
	nor->eeprom_write = m25p80_eeprom_write;
	nor->write_reg = m25p80_write_reg;
	nor->read_reg = m25p80_read_reg;
	nor->priv = spimem;

	if (spi->mode & SPI_RX_OCTAL) {
		hwcaps.mask |= SNOR_HWCAPS_READ_1_1_8;

		if (spi->mode & SPI_TX_OCTAL)
			hwcaps.mask |= (SNOR_HWCAPS_READ_1_8_8 |
					SNOR_HWCAPS_PP_1_1_8 |
					SNOR_HWCAPS_PP_1_8_8);
	} else if (spi->mode & SPI_RX_QUAD) {
		//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
		hwcaps.mask |= SNOR_HWCAPS_READ_1_1_4;

		if (spi->mode & SPI_TX_QUAD)
			hwcaps.mask |= (SNOR_HWCAPS_READ_1_4_4 |
					SNOR_HWCAPS_PP_1_1_4 |
					SNOR_HWCAPS_PP_1_4_4);
	} else if (spi->mode & SPI_RX_DUAL) {
		//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
		hwcaps.mask |= SNOR_HWCAPS_READ_1_1_2;

		if (spi->mode & SPI_TX_DUAL)
			hwcaps.mask |= SNOR_HWCAPS_READ_1_2_2;
	}

	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
	ret = spi_nor_scan(nor, "w25q16", &hwcaps);
	if (ret)
		return ret;

}

static void m25p_shutdown(struct spi_mem *spimem)
{
	struct m25p *flash = spi_mem_get_drvdata(spimem);
	//RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);

}

static struct spi_device spi_device = {
	.mode = SPI_CS_HIGH | SPI_MODE_0,
	.bits_per_word = 8,
	.chip_select = 0, 
};

struct spi_mem spi_mem_obj = {
	.spi = &spi_device,
	.name = "spi_mem",
};
static struct m25p m25p_flash = {
	.spimem = &spi_mem_obj,
};

void hal_m25p_init(void)
{
	RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
	spi_mem_obj.drvpriv = &m25p_flash;
	m25p_init(&(spi_mem_obj));
}

u32 spi_nor_check_data(u32 *start, u32 len)
{
	int i = 0;
	u32 crc = 0;
	for (int i = 0; i < len/4; i++ ){
		crc += *(start + i);
		//RVHAL_OS_LOG_DEBUG("addr 0x%x value: 0x%08x \n", start + i, *(start + i));
	}

	return crc;
}

extern void at25xe161d_set_lpm(void);
extern  at25xe161d_clr_lpm(void);
extern void at25xe161d_enter_lpm(void);

/**
 * Erase an address range on the nor chip.  The address range may extend
 * one or more erase sectors.  Return an error is there is a problem erasing.
 */
int m25p80_spi_nor_erase(u32 addr, u32 len)
{
	struct spi_nor *nor = &(m25p_flash.spi_nor);
	struct mtd_info *mtd = &nor->mtd;

	return spi_nor_erase(mtd, addr, len);
}
/*
 * Write an address range to the nor chip.  Data must be written in
 * FLASH_PAGESIZE chunks.  The address range may be any size provided
 * it is within the physical boundaries.
 */
int m25p80_spi_nor_write(loff_t to, size_t len,
	size_t *retlen, const u_char *buf)
{
	struct spi_nor *nor = &(m25p_flash.spi_nor);
 
	return spi_nor_write(nor, to, len, retlen, buf);
}

int m25p80_spi_nor_read(loff_t from, size_t len,
			size_t *retlen, u_char *buf)
{
	struct spi_nor *nor = &(m25p_flash.spi_nor);
 
	return spi_nor_read(nor, from, len, retlen, buf);
}

void m25p80_spi_nor_init(void)
{
	spi_init();
	hal_m25p_init();
}
#endif
