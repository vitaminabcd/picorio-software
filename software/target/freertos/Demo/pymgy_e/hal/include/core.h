#ifndef HAL_INCLUDE_CORE_H
#define HAL_INCLUDE_CORE_H

#include "rtlgen.h"

// TODO: SMP, save flags...
#define RVHAL_ENTER_CS()	__asm volatile( "csrc mstatus, 8" )
#define RVHAL_EXIT_CS()		__asm volatile( "csrs mstatus, 8" )

struct irq_ext_handler_t
{
	void (*hook)(void);
};

void __rvHal_request_irq_ext(unsigned int irq, struct irq_ext_handler_t *handler);
void __rvHal_free_irq_ext(unsigned int irq);
void __rvHal_irq_ext_init(void);

void rvHal_irq_ext_interrupt(void);

#endif /* HAL_INCLUDE_IRQ_EXT_H */

