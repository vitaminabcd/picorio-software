#ifndef __HAL_DEFINE_H__
#define __HAL_DEFINE_H__


#include <stdint.h>
#include <stdbool.h>

#if defined ( __GNUC__ )
  #ifndef __weak
    #define __weak   __attribute__((weak))
  #endif /* __weak */
  #ifndef __packed
    #define __packed __attribute__((__packed__))
  #endif /* __packed */
  #ifndef __unused
   #define __unused __attribute__((unused))
  #endif
  #define __NOINLINE __attribute__ ( (noinline) ) 

#endif /* __GNUC__ */

#ifndef NULL
#ifdef __cplusplus
#define NULL				(0) /*!< NULL */
#else
#define NULL				((void *)(0)) /*!< NULL */
#endif
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

typedef unsigned char u8;
typedef unsigned short u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef u32 loff_t;
typedef unsigned char u_char;

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif

#endif /* __HAL_DEFINE_H__ */

