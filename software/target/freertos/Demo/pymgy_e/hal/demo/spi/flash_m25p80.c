// RIVAI: GPL-2.0-only
#include "hal.h"
#include "m25p80.h"
#include "demo.h"

#ifdef TEST_DEMO_SPI_FLASH

void spi_nor_test(void)
{
	size_t retlen = 0;
	const u_char *wbuf = 0x80000000;
	u_char *rbuf = 0x80000400; //1024B
	u32 write_buf_crc = 0;
	u32 read_buf_crc1 = 0;
	u32 read_buf_crc2 = 0;
	u32 index = 0;
	//RVHAL_OS_LOG_DEBUG("func:%s, erase chip 16MBytes \n", __func__);
	//spi_nor_erase(mtd, 0, 0x1000000);
	//RVHAL_OS_LOG_DEBUG("func:%s, erase block 64KB \n", __func__);
#if 1 
	RVHAL_OS_LOG_INFO("------spi flash test------- \n \
		main flow: 1. erase 1 block(64KB). 2 write 1 page(256B). 3. read 1 page(256B); 4. check the crc result. 5.sleep 50000 ms \n", __func__);
	m25p80_spi_nor_erase(0x20000, 0x10000);
	RVHAL_OS_LOG_DEBUG("func:%s, write page 256B \n", __func__);
	//fixme: ? 
	m25p80_spi_nor_write(0x20000, 256, &retlen, wbuf);
	RVHAL_OS_LOG_DEBUG("func:%s, write page 256B retlen = %x \n", __func__, retlen);
	write_buf_crc = spi_nor_check_data((u32 *)wbuf, 256);
	
#endif 
#if 1
	while(index++ < 1) {
		RVHAL_OS_LOG_DEBUG("func:%s, read page 256B \n", __func__);
		m25p80_spi_nor_read(0x20000, 256, &retlen, rbuf);
		RVHAL_OS_LOG_DEBUG("func:%s, read page 256B retlen = 0x%x \n", __func__, retlen);
		read_buf_crc1 = spi_nor_check_data((u32 *)rbuf, 256);

		if(read_buf_crc1 != write_buf_crc) {
			RVHAL_OS_LOG_ERR("[ERR]: mean write or read err happened! \n");
		} else {
			RVHAL_OS_LOG_INFO("[PASS][SPI_NOR_FLASH][erase&write&read&compare][crc check pass] ...\n");
		}

	}
	
#endif
	rvHal_sleep(50000);
#if 0
	
	at25xe161d_set_lpm();
	//at25xe161d_clr_lpm();
	at25xe161d_enter_lpm();
	while(1);
#endif
}

void vSpiNorTestTask(void *pvParameters __unused)
{
	static u32 test_flag = 1;
	m25p80_spi_nor_init();
	RVHAL_OS_LOG_DEBUG("func: %s, line: %d. \n", __func__, __LINE__);
	while(1)
	{
		if(1 == test_flag) {
			test_flag = 0;
			spi_nor_test();
		} else {
			vTaskDelay(10000);
		}
	}
	return ;
}
#else
void vSpiNorTestTask(void *pvParameters __unused)
{
	return ;
}
#endif
