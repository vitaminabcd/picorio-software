/*!
 * \file      pingpong.c
 *
 * \brief     Ping-Pong implementation
 *
 * \copyright Revised BSD License, see section \ref LICENSE.
 *
 * \code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 * \endcode
 *
 * \author    Miguel Luis ( Semtech )
 *
 * \author    Gregory Cristian ( Semtech )
 */
#include <string.h>
#include "hal.h"
#include "gpio.h"
#include "timer.h"
#include "radio.h" 
#include "clib.h"

#define GC_SX1262_BOARD
#ifdef GC_SX1262_BOARD
#define RF_FREQUENCY                                868000000 // gc sx1262 board

#define TX_OUTPUT_POWER                             1//14        // dBm

#define LORA_BANDWIDTH                              2         // [0: 125 kHz,
                                                              //  1: 250 kHz,
                                                              //  2: 500 kHz,
                                                              //  3: Reserved]
#define LORA_SPREADING_FACTOR                       7//12         // [SF7..SF12]
#else
#define RF_FREQUENCY                                470000000 // semtech sx1276 board

#define TX_OUTPUT_POWER                             14        // dBm

#define LORA_BANDWIDTH                              0         // [0: 125 kHz,
                                                              //  1: 250 kHz,
                                                              //  2: 500 kHz,
                                                              //  3: Reserved]
#define LORA_SPREADING_FACTOR                       12         // [SF7..SF12]
#endif
#define LORA_CODINGRATE                             1        // [1: 4/5,
                                                              //  2: 4/6,
                                                              //  3: 4/7,
                                                              //  4: 4/8]
#define LORA_PREAMBLE_LENGTH                        8         // Same for Tx and Rx
#define LORA_SYMBOL_TIMEOUT                         0         // Symbols
#define LORA_FIX_LENGTH_PAYLOAD_ON                  false
#define LORA_IQ_INVERSION_ON                        false

typedef enum
{
    LOWPOWER,
    RX,
    RX_TIMEOUT,
    RX_ERROR,
    TX,
    TX_TIMEOUT,
}States_t;

//#define STM_PER
//#define RX_ONLY
#ifdef RX_ONLY
#define RX_TIMEOUT_VALUE                            300000
#else
#define RX_TIMEOUT_VALUE                            3000
#endif
#define BUFFER_SIZE                                 9//64 // Define the payload size here
static unsigned int NumValue = 0;

const uint8_t PingMsg[] = "PING";
#ifndef STM_PER
const uint8_t PongMsg[] = "PONG";
#else
const uint8_t PongMsg[] = "PER";
#endif

uint16_t BufferSize = BUFFER_SIZE;
uint8_t Buffer[BUFFER_SIZE];

States_t State = LOWPOWER;

int8_t RssiValue = 0;
int8_t SnrValue = 0;

#if 1
/*!
 * Radio events function pointer
 */
static RadioEvents_t RadioEvents;

/*!
 * \brief Function to be executed on Radio Tx Done event
 */
void OnTxDone( void );

/*!
 * \brief Function to be executed on Radio Rx Done event
 */
void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );

/*!
 * \brief Function executed on Radio Tx Timeout event
 */
void OnTxTimeout( void );

/*!
 * \brief Function executed on Radio Rx Timeout event
 */
void OnRxTimeout( void );

/*!
 * \brief Function executed on Radio Rx Error event
 */
void OnRxError( void );

void OnTxDone( void )
{
	RVHAL_LORA_LOG_DEBUG("%s\n\n", __func__);//rongdebug
    Radio.Sleep( );
    State = TX;
}

void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
//	rvHal_dump_hex(payload, size, "RX pkt <----- rssi: %d, snr: %d, size: %u \n", rssi, snr, size);//rongdebug
NumValue++;//rongdebug
    Radio.Sleep( );
    BufferSize = size;
    memcpy( Buffer, payload, BufferSize );
    RssiValue = rssi;
    SnrValue = snr;
    State = RX;
}

void OnTxTimeout( void )
{
	RVHAL_LORA_LOG_ERR("%s\n", __func__);
    Radio.Sleep( );
    State = TX_TIMEOUT;
}

void OnRxTimeout( void )
{
	RVHAL_LORA_LOG_ERR("%s %u\n", __func__);
    Radio.Sleep( );
    State = RX_TIMEOUT;
}

void OnRxError( void )
{
	RVHAL_LORA_LOG_ERR("%s\n", __func__);
    Radio.Sleep( );
    State = RX_ERROR;
}
#endif

static void lora_dump_reg(char *func, int line)
{
#if 0
	int i;
	unsigned char data = 0;
	
	RVHAL_LORA_LOG_DEBUG("%s: %d\n", func, line);

	for (i=1; i<0x70; i++)
	{
		rvHal_spim_read(i, &data, 1);
		RVHAL_LORA_LOG_DEBUG("addr[ 0x%x ] spi rx data: 0x%x\n",  i, data);
	}
#endif
}

/**
 * Main application entry point.
 */
void PingpongMain( void )
{
    bool isMaster = true;
    uint8_t i;

	lora_dump_reg(__func__, __LINE__);

#if 1
    BoardInitMcu( );

//	RVHAL_LORA_LOG_DEBUG("%s\n", __func__);

    // Radio initialization
    RadioEvents.TxDone = OnTxDone;
    RadioEvents.RxDone = OnRxDone;
    RadioEvents.TxTimeout = OnTxTimeout;
    RadioEvents.RxTimeout = OnRxTimeout;
    RadioEvents.RxError = OnRxError;

    Radio.Init( &RadioEvents );

#if 1
    Radio.SetChannel( RF_FREQUENCY );

    Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
                                   LORA_SPREADING_FACTOR, LORA_CODINGRATE,
                                   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
                                   true, 0, 0, LORA_IQ_INVERSION_ON, 3000 );

    Radio.SetRxConfig( MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
                                   LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
                                   LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
                                   0, true, 0, 0, LORA_IQ_INVERSION_ON, true );

    Radio.Rx( RX_TIMEOUT_VALUE );

//	RVHAL_LORA_LOG_DEBUG("LoRa version is : 0x%x\n", Radio.Read( REG_LR_VERSION ));
	lora_dump_reg(__func__, __LINE__);

    while( 1 )
    {
#ifdef RX_ONLY
	if (State == RX && BufferSize) {
		RVHAL_TRACE("RX pkt <----- pkt%u: rssi: %d, snr: %d, size: %u\n", NumValue, RssiValue, SnrValue, BufferSize);
		rvHal_dump_hex(Buffer, BufferSize, "");//rongdebug
		rvHal_sleep(1000);
		BufferSize = 0;
	}
#else
	if (State == RX && NumValue) {
		RVHAL_TRACE("RX pkt <----- pkt%u: rssi: %d, snr: %d, size: %u\n", NumValue, RssiValue, SnrValue, BufferSize);
		rvHal_dump_hex(Buffer, BufferSize, "");//rongdebug
	}

		switch( State )
        {
        case RX:
            if( isMaster == true )
            {
                if( BufferSize > 0 )
                {
#ifdef STM_PER
                    if( strncmp( ( const char* )(Buffer+5), ( const char* )PongMsg, 3 ) == 0 )
#else
                    if( strncmp( ( const char* )Buffer, ( const char* )PongMsg, 4 ) == 0 )
#endif
                    {
                        // Indicates on a LED that the received frame is a PONG

                        // Send the next PING frame
                        Buffer[0] = 'P';
                        Buffer[1] = 'I';
                        Buffer[2] = 'N';
                        Buffer[3] = 'G';
                        // We fill the buffer with numbers for the payload
                        for( i = 4; i < BufferSize; i++ )
                        {
                            Buffer[i] = i - 4;
                        }
                        DelayMs( 1 );
                        Radio.Send( Buffer, BufferSize );
                    }
#ifdef STM_PER
                    else if( strncmp( ( const char* )(Buffer+5), ( const char* )PongMsg, 3 ) == 0 )
#else
                    else if( strncmp( ( const char* )Buffer, ( const char* )PingMsg, 4 ) == 0 )
#endif
                    { // A master already exists then become a slave
                        isMaster = false;
#ifdef STM_PER
                        Buffer[0] = 'P';
                        Buffer[1] = 'I';
                        Buffer[2] = 'N';
                        Buffer[3] = 'G';
                        // We fill the buffer with numbers for the payload
                        for( i = 4; i < BufferSize; i++ )
                        {
                            Buffer[i] = i - 4;
                        }

		RVHAL_TRACE("RX pkt <----- rssi: %d, snr: %d, size: %u \n", RssiValue, SnrValue, NumValue);
		unsigned char Buffer1[9];memcpy(Buffer1, Buffer, 9);
		rvHal_dump_hex(Buffer1, BufferSize, "");//rongdebug
#endif
                        Radio.Rx( RX_TIMEOUT_VALUE );
                    }
                    else // valid reception but neither a PING or a PONG message
                    {    // Set device as master ans start again
                        isMaster = true;
                        Radio.Rx( RX_TIMEOUT_VALUE );
                    }
                }
            }
            else
            {
                if( BufferSize > 0 )
                {
#ifdef STM_PER
                    if( strncmp( ( const char* )(Buffer+5), ( const char* )PongMsg, 3 ) == 0 )
#else
                    if( strncmp( ( const char* )Buffer, ( const char* )PingMsg, 4 ) == 0 )
#endif
                    {
                        // Indicates on a LED that the received frame is a PING

                        // Send the reply to the PONG string
                        Buffer[0] = 'P';
                        Buffer[1] = 'O';
                        Buffer[2] = 'N';
                        Buffer[3] = 'G';
                        // We fill the buffer with numbers for the payload
                        for( i = 4; i < BufferSize; i++ )
                        {
                            Buffer[i] = i - 4;
                        }
                        DelayMs( 1 );
#ifdef STM_PER
		RVHAL_TRACE("RX pkt <----- rssi: %d, snr: %d, size: %u \n", RssiValue, SnrValue, NumValue);
		unsigned char Buffer1[9];memcpy(Buffer1, Buffer, 9);Buffer[1] = 'I';
		rvHal_dump_hex(Buffer1, BufferSize, "");//rongdebug
#endif
                        Radio.Send( Buffer, BufferSize );
                    }
                    else // valid reception but not a PING as expected
                    {    // Set device as master and start again
                        isMaster = true;
                        Radio.Rx( RX_TIMEOUT_VALUE );
                    }
                }
            }
            State = LOWPOWER;
            break;
        case TX:
            // Indicates on a LED that we have sent a PING [Master]
            // Indicates on a LED that we have sent a PONG [Slave]

            Radio.Rx( RX_TIMEOUT_VALUE );
            State = LOWPOWER;
            break;
        case RX_TIMEOUT:
        case RX_ERROR:
            if( isMaster == true )
            {
				// Send the next PING frame
				Buffer[0] = 'P';
                Buffer[1] = 'I';
                Buffer[2] = 'N';
                Buffer[3] = 'G';
                for( i = 4; i < BufferSize; i++ )
                {
                    Buffer[i] = i - 4;
                }
                DelayMs( 1 );
                Radio.Send( Buffer, BufferSize );
            }
            else
            {
                Radio.Rx( RX_TIMEOUT_VALUE );
            }
            State = LOWPOWER;
            break;
        case TX_TIMEOUT:
            Radio.Rx( RX_TIMEOUT_VALUE );
            State = LOWPOWER;
            break;
        case LOWPOWER:
        default:
            // Set low power
            break;
        }
#endif
        BoardLowPowerHandler( );
        // Process Radio IRQ
        if( Radio.IrqProcess != NULL )
        {
            Radio.IrqProcess( );
        }
    }
#endif
#endif
}
