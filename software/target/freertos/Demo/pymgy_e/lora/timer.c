/*!
 * \file      timer.c
 *
 * \brief     Timer objects and scheduling management implementation
 *
 * \copyright Revised BSD License, see section \ref LICENSE.
 *
 * \code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 * \endcode
 *
 * \author    Miguel Luis ( Semtech )
 *
 * \author    Gregory Cristian ( Semtech )
 */
#include "timer.h"
#include "hal.h"
#include "timers.h"

void TimerInit( TimerEvent_t *obj, void ( *callback )( void *context ) )
{
    obj->Timestamp = 0;
    obj->ReloadValue = 0;
    obj->IsStarted = false;
    obj->IsNext2Expire = false;
    obj->Callback = callback;
    obj->Context = NULL;
    obj->Next = NULL;

#if 1
	char name[20];
	snprintf(name, sizeof(name), "T%x\n", obj);
	obj->adaptor = xTimerCreate( name, -1UL, pdFALSE, NULL, callback ); //TODO:xTimerCreateStatic
	RVHAL_ASSERT_PARAM(obj->adaptor);

//	RVHAL_LORA_LOG_DEBUG("%s adaptor: 0x%x timeout: %u---\n", __func__, obj->adaptor, obj->ReloadValue);//rongdebug
#endif	
}

void TimerSetContext( TimerEvent_t *obj, void* context )
{
    obj->Context = context;
}

void TimerStart( TimerEvent_t *obj )
{
//	RVHAL_LORA_LOG_DEBUG("%s adaptor: 0x%x timeout: %u---\n", __func__, obj->adaptor, obj->ReloadValue);//rongdebug
	if (uPortasmIsInterrupt())
		xTimerChangePeriodFromISR(obj->adaptor, obj->ReloadValue, NULL);
	else	
		xTimerChangePeriod( obj->adaptor, obj->ReloadValue, 0UL );
}

bool TimerIsStarted( TimerEvent_t *obj )
{
    return obj->IsStarted;
}
extern TimerEvent_t RxTimeoutTimer;
extern int rxtimer_flag;
void TimerStop( TimerEvent_t *obj )
{
//	if (&RxTimeoutTimer==obj && rxtimer_flag) printf("stop rx timer\n");

	if (uPortasmIsInterrupt())
		xTimerStopFromISR(obj->adaptor, NULL);
	else
		xTimerStop(obj->adaptor, 0UL);
}

void TimerReset( TimerEvent_t *obj )
{
    TimerStop( obj );
    TimerStart( obj );
}

void TimerSetValue( TimerEvent_t *obj, uint32_t value )
{
    uint32_t minValue = 0;
    uint32_t ticks = pdMS_TO_TICKS( value );

    TimerStop( obj );

    minValue = 1;

    if( ticks < minValue )
    {
        ticks = minValue;
    }

    obj->ReloadValue = ticks;
}

// TODO: for MAC cmds: rtc to nvram?
TimerTime_t TimerGetCurrentTime( void )
{
	return rvHal_get_uptime();
}

TimerTime_t TimerGetElapsedTime( TimerTime_t past )
{
    if ( past == 0 )
    {
        return 0;
    }

	return rvHal_get_uptime() - past;
}

