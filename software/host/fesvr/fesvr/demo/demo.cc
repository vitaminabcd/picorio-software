#include "demo.h"

#ifndef TARGET_ARCH
# define TARGET_ARCH "riscv64-unknown-elf"
#endif

#if !defined(PREFIX) && defined(__PCONFIGURE__PREFIX)
# define PREFIX __PCONFIGURE__PREFIX
#endif


#ifndef TARGET_DIR
# define TARGET_DIR "/" TARGET_ARCH "/bin/"
#endif


void demo::setup(){
  std::vector<func_t>::iterator it = this->boot_sequence.begin();
  for (;it != this->boot_sequence.end(); ++it){
    if(this->ht->argmap[it->key.c_str()].length()>0){
      func_pointer run_function = it->ptr;
      (this->*run_function)(this->ht->argmap[it->key.c_str()]);
    }
  }
}

void demo::io_test(std::string key) {
  uint8_t reg[8];
  time_t ltime;
  uint64_t data;
  // Write Brkpt Register
  ltime = time(NULL);
  fprintf(stderr, "Start Writing Breakpoint Register to 0x123456789abcdef0 @ %s\n", asctime(localtime(&ltime)));
  data = 0x123456789abcdef0;
  this->ht->OURSBUS_WRITE(0x8000000000 + 8 * 4, data);
  ltime = time(NULL);
  fprintf(stderr, "Done Writing Breakpoint Register to 0x123456789abcdef0 @ %s\n", asctime(localtime(&ltime)));
  // Read Brkpt Register
  ltime = time(NULL);
  fprintf(stderr, "Start Reading Breakpoint Register @ %s\n", asctime(localtime(&ltime)));
  data = 0;
  this->ht->OURSBUS_READ(0x8000000000 + 8 * 4, data);
  ltime = time(NULL);
  fprintf(stderr, "Done Reading Breakpoint Register, got 0x%x @ %s\n", data, asctime(localtime(&ltime)));
  if (data != 0x123456789abcdef0) {
    fprintf(stderr, "Error: IO Test Failed!\n");
  }
  // Write GPIO
  ltime = time(NULL);
  fprintf(stderr, "Start Writing GPIO Address 0x400001000 to 0x123456789abcdef0 @ %s\n", asctime(localtime(&ltime)));
  data = 0x123456789abcdef0;
  this->ht->OURSBUS_WRITE(0x9000001000, data);
  ltime = time(NULL);
  fprintf(stderr, "Done Writing GPIO Address 0x400001000 to 0x123456789abcdef0 @ %s\n", asctime(localtime(&ltime)));
  // Read GPIO
  ltime = time(NULL);
  fprintf(stderr, "Start Reading GPIO Address 0x400001000 @ %s\n", asctime(localtime(&ltime)));
  data = 0;
  this->ht->OURSBUS_READ(0x9000001000, data);
  ltime = time(NULL);
  fprintf(stderr, "Done Reading GPIO Address 0x400001000, got 0x%x @ %s\n", data, asctime(localtime(&ltime)));
  if (data != 0x123456789abcdef0) {
    fprintf(stderr, "Error: IO Test Failed!\n");
  }
}

void demo::program_pll(std::string key) {
  uint8_t reg[8];
  time_t ltime;
  ltime = time(NULL);
  fprintf(stderr, "Start Programming PLL @ %s\n", asctime(localtime(&ltime)));

  // Keep RESET and BYPASS
  char* fin;
  char* fout;
  char* pEnd;
  char* tmp;
  char pll_str[256];
  uint64_t nr, nf, od, nb, t;
  strcpy(pll_str, key.c_str());

  fin = strtok(pll_str, "_");
  fout = strtok(NULL, "_");

  char cmd[256];
  char cfg[256];
  //sprintf(cmd, "/home/ours-demo/TCI_PLL/TCITSMCN28HPCPGPMPLLA1_calc.csh -n %s %s | awk '{ if (NR == 3) print $1,$2,$3,$4 }'", fin, fout);
  sprintf(cmd, "/work/tech/TCI_PLL/TCITSMCN28HPCPGPMPLLA1_calc.csh -n %s %s | awk '{ if (NR == 3) print $1,$2,$3,$4 }'", fin, fout);
  //fprintf(stderr, "cmd = %s\n", cmd);
  FILE* result = popen(cmd, "r");

  fgets(cfg, 255, result);
  tmp = strtok(cfg, " ");
  nr = std::strtoul(tmp, &pEnd, 10);
  tmp = strtok(NULL, " ");
  nf = std::strtoul(tmp, &pEnd, 10);
  tmp = strtok(NULL, " ");
  od = std::strtoul(tmp, &pEnd, 10);
  tmp = strtok(NULL, " ");
  nb = std::strtoul(tmp, &pEnd, 10);

  t = ((nr - 1) << 21) + ((nf - 1) << 15) + ((od - 1) << 11) + ((nb - 1) << 5) + 0x16;

  for (int i = 0; i < 8; i++) {
    reg[i] = t >> (i * 8);
  }
  this->ht->mem.write(0x8000000000, 8, (uint8_t*)reg);

  // Assert PLL_PROG_DONE
  t = (0x1 << 25) + ((nr - 1) << 21) + ((nf - 1) << 15) + ((od - 1) << 11) + ((nb - 1) << 5) + 0x16;
  for (int i = 0; i < 8; i++) {
    reg[i] = t >> (i * 8);
  }
  this->ht->mem.write(0x8000000000, 8, (uint8_t*)reg);

  // Wait for Lock
  usleep(10);

  // Drop PLL_RESET
  t = ((nr - 1) << 21) + ((nf - 1) << 15) + ((od - 1) << 11) + ((nb - 1) << 5) + 0x06;

  for (int i = 0; i < 8; i++)
    reg[i] = t >> (i * 8);

  this->ht->mem.write(0x8000000000, 8, (uint8_t*)reg);

  // Wait for Refclk
  usleep(1);

  // Drop PLL_BYPASS and Set CLK_SEL
  t = (1 << 26) + ((nr - 1) << 21) + ((nf - 1) << 15) + ((od - 1) << 11) + ((nb - 1) << 5) + 0x04;

  for (int i = 0; i < 8; i++)
    reg[i] = t >> (i * 8);

  this->ht->mem.write(0x8000000000, 8, (uint8_t*)reg);

  ltime = time(NULL);
  fprintf(stderr, "Done Programming PLL @ %s\n", asctime(localtime(&ltime)));
}
void demo::reset_ddr(std::string key) {
  uint8_t reg[8];
  time_t ltime;
  //ltime = time(NULL);
  //fprintf(stderr, "Start Reseting DDR @ %s\n", asctime(localtime(&ltime)));
  //// Power On
  //reg[0] = 0x07;
  //reg[1] = 0x2f;
  //reg[2] = 0x00;
  //reg[3] = 0x00;
  //reg[4] = 0x00;
  //reg[5] = 0x00;
  //reg[6] = 0x00;
  //reg[7] = 0x00;
  //this->ht->mem.write(0x8000000000 + 8 * 0x1d, 8, (uint8_t*)reg);

  ////// Insert Bubble
  ////for (int i = 0; i < 1000; i++)
  ////  this->ht->mem.read(0x8000000000, 8, (uint8_t*)reg);

  //// Drop presetn
  //reg[0] = 0x07;
  //reg[1] = 0x3f;
  //reg[2] = 0x00;
  //reg[3] = 0x00;
  //reg[4] = 0x00;
  //reg[5] = 0x00;
  //reg[6] = 0x00;
  //reg[7] = 0x00;
  //this->ht->mem.write(0x8000000000 + 8 * 0x1d, 8, (uint8_t*)reg);

  ////// Insert Bubble
  ////for (int i = 0; i < 1000; i++)
  ////  this->ht->mem.read(0x8000000000, 8, (uint8_t*)reg);

  //uint64_t rdata;
  //#include "ddr_seq_bf_rst.cc"
  ////// Insert Bubble
  ////for (int i = 0; i < 1000; i++)
  ////  this->ht->mem.read(0x8000000000, 8, (uint8_t*)reg);

  //// Drop rst and arst
  //reg[0] = 0x07;
  //reg[1] = 0x33;
  //reg[2] = 0x00;
  //reg[3] = 0x00;
  //reg[4] = 0x00;
  //reg[5] = 0x00;
  //reg[6] = 0x00;
  //reg[7] = 0x00;
  //this->ht->mem.write(0x8000000000 + 8 * 0x1d, 8, (uint8_t*)reg);

  ////// Insert Bubble
  ////for (int i = 0; i < 1000; i++)
  ////  this->ht->mem.read(0x8000000000, 8, (uint8_t*)reg);

  //#include "ddr_seq_af_rst.cc"

  ////// Insert Bubble
  ////for (int i = 0; i < 1000; i++)
  ////  this->ht->mem.read(0x8000000000, 8, (uint8_t*)reg);

  //ltime = time(NULL);
  //fprintf(stderr, "Done Reseting DDR @ %s\n", asctime(localtime(&ltime)));
      ltime = time(NULL);
    fprintf(stderr, "Start Reseting DDR @ %s\n", asctime(localtime(&ltime)));
    // Power On
    reg[0] = 0x07;
    reg[1] = 0x2f;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00;
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(0x8000000000 + 8 * 0x1d, 8, (uint8_t*)reg);

    //// Insert Bubble
    //for (int i = 0; i < 1000; i++)
    //  mem.read(0x8000000000, 8, (uint8_t*)reg);

    // Drop presetn
    reg[0] = 0x07;
    reg[1] = 0x3f;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00;
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(0x8000000000 + 8 * 0x1d, 8, (uint8_t*)reg);

    //// Insert Bubble
    //for (int i = 0; i < 1000; i++)
    //  mem.read(0x8000000000, 8, (uint8_t*)reg);

    uint64_t rdata;
    #include "./ddr_seq_bf_rst.cc"

    //// Insert Bubble
    //for (int i = 0; i < 1000; i++)
    //  mem.read(0x8000000000, 8, (uint8_t*)reg);

    // Drop rst and arst
    reg[0] = 0x07;
    reg[1] = 0x33;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00;
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(0x8000000000 + 8 * 0x1d, 8, (uint8_t*)reg);
    //// Insert Bubble
    //for (int i = 0; i < 1000; i++)
    //  mem.read(0x8000000000, 8, (uint8_t*)reg);

    #include "./ddr_seq_af_rst.cc"

    //// Insert Bubble
    //for (int i = 0; i < 1000; i++)
    //  mem.read(0x8000000000, 8, (uint8_t*)reg);

    ltime = time(NULL);
    fprintf(stderr, "Done Reseting DDR @ %s\n", asctime(localtime(&ltime)));
}

void demo::update_l2_configuration(std::string key) {
  uint8_t reg[8];
  time_t ltime;
  int vls_mode = 0; // 0 - 3, 0 : 8WAY, 1 : 2WAY, 2 : 4WAY, 3 : 6WAY
  int vls_en = 0; // 0 - 1
  int pwr_off = 0; // 0 - 6, number of ways shutdown/sleep, %2 == 0
  ltime = time(NULL);
  fprintf(stderr, "Start Configuring L2 @ %s\n", asctime(localtime(&ltime)));
  if(this->ht->argmap["config_l2="] == "random") {
    int rand = random() % 100;
    if ((0 <= rand) && (rand < 20)) {
      // CASE: 8WAY CACHE
      vls_mode = random() % 4;
      vls_en = 0;
      pwr_off = (random() % 4) * 2;
    } else if ((20 <= rand) && (rand < 40)) {
      // CASE: 6WAY CACHE, 2WAY VLS
      vls_mode = 1;
      vls_en = 1;
      pwr_off = (random() % 3) * 2;
    } else if ((40 <= rand) && (rand < 60)) {
      // CASE: 4WAY CACHE, 4WAY VLS
      vls_mode = 2;
      vls_en = 1;
      pwr_off = (random() % 2) * 2;
    } else if ((60 <= rand) && (rand < 80)) {
      // CASE: 2WAY CACHE, 6WAY VLS
      vls_mode = 3;
      vls_en = 1;
      pwr_off = 0;
    } else {
      // CASE: 8WAY VLS
      vls_mode = 0;
      vls_en = 1;
      pwr_off = 0;
    }
    // Program PWR
    int pwr[8] = {0};
    for (int i = 0; i < pwr_off; i++)
      pwr[7 - i] = 1 + (random() % 2);
    reg[0] = (pwr[3] << 6) + (pwr[2] << 4) + (pwr[1] << 2) + pwr[0];
    reg[1] = (pwr[7] << 6) + (pwr[6] << 4) + (pwr[5] << 2) + pwr[4];
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x01; // enable performance counter
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(0x8000000000 + 8 * 0x1b, 8, (uint8_t*)reg);
    // Program VLS
    reg[0] = 0x00; // pbase 32bit
    reg[1] = 0x00;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = (vls_mode << 1) + vls_en; // {vls_mode, vls_en}
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(0x8000000000 + 8 * 0x1c, 8, (uint8_t*)reg);
  } else if (this->ht->argmap["config_l2="] == "default") {
    // Program PWR
    reg[0] = 0x00;
    reg[1] = 0x00;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x01; // enable performance counter
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(0x8000000000 + 8 * 0x1b, 8, (uint8_t*)reg);
    // Program VLS
    reg[0] = 0x00; // pbase 32bit
    reg[1] = 0x00;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00; // {vls_mode, vls_en}
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(0x8000000000 + 8 * 0x1c, 8, (uint8_t*)reg);
  }
  ltime = time(NULL);
  fprintf(stderr, "Done Configuring L2, vls_mode = %0d, vls_en = %0d, pwr_off = %0d @ %s\n", vls_mode, vls_en, pwr_off, asctime(localtime(&ltime)));
}
void demo::release_reset_for_l2(std::string key) {
  uint8_t reg[8];
  time_t ltime;
  //ltime = time(NULL);
  //fprintf(stderr, "Start Releasing Reset for L2 @ %s\n", asctime(localtime(&ltime)));
  //reg[0] = 0x00;
  //reg[1] = 0x00;
  //reg[2] = 0x00;
  //reg[3] = 0x00;
  //reg[4] = 0x00;
  //reg[5] = 0x00;
  //reg[6] = 0x00;
  //reg[7] = 0x00;
  //this->ht->mem.write(0x8000000000 + 8 * 0x1a, 8, (uint8_t*)reg);
  //ltime = time(NULL);
  //fprintf(stderr, "Done Releasing Reset for L2 @ %s\n", asctime(localtime(&ltime)));
    ltime = time(NULL);
    fprintf(stderr, "Start Releasing Reset for L2 @ %s\n", asctime(localtime(&ltime)));
    reg[0] = 0x00;
    reg[1] = 0x00;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00;
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(0x8000000000 + 8 * 0x1a, 8, (uint8_t*)reg);
    ltime = time(NULL);
    fprintf(stderr, "Done Releasing Reset for L2 @ %s\n", asctime(localtime(&ltime)));
}
void demo::test_l2(std::string key) {
  uint8_t reg[8];
  time_t ltime;
  ltime = time(NULL);
  fprintf(stderr, "Start Test L2 @ %s\n", asctime(localtime(&ltime)));


  ltime = time(NULL);
  fprintf(stderr, "Done Test L2 @ %s\n", asctime(localtime(&ltime)));

}
void demo::set_valid_ram_to_zero_for_l2(std::string key){
  uint8_t reg[8];
  time_t ltime;
  //ltime = time(NULL);
  //fprintf(stderr, "Start Setting Valid RAM to 0's for L2 @ %s\n", asctime(localtime(&ltime)));
  //long int ID = 0x28;
  //long int addr;
  //reg[0] = 0x00;
  //reg[1] = 0x00;
  //reg[2] = 0x00;
  //reg[3] = 0x00;
  //reg[4] = 0x00;
  //reg[5] = 0x00;
  //reg[6] = 0x00;
  //reg[7] = 0x00;
  //for (int bank = 0; bank < 8; bank ++) {
  //  for (int way = 0; way < 8; way ++) {
  //    for (int idx = 0; idx < 512; idx ++) {
  //      addr = ID;
  //      addr = addr << 17; // RSVD
  //      addr = addr << 3; // BANK_ID
  //      addr = addr + bank;
  //      addr = addr << 3; // WAY_ID
  //      addr = addr + way;
  //      addr = addr << 2; // CHUNK_ID
  //      addr = addr << 9; // BANK_INDEX
  //      addr = addr + idx;
  //      //fprintf(stderr, "addr = %x, writing\n", addr);
  //      this->ht->mem.write(addr, 8, (uint8_t*)reg);
  //      //fprintf(stderr, "addr = %x, done writing\n", addr);
  //    }
  //  }
  //}
  //ltime = time(NULL);
  //fprintf(stderr, "Done Setting Valid RAM to 0's for L2 @ %s\n", asctime(localtime(&ltime)));
    ltime = time(NULL);
    fprintf(stderr, "Start Setting Valid RAM to 0's for L2 @ %s\n", asctime(localtime(&ltime)));
    long int ID = 0x28;
    long int addr;
    reg[0] = 0x00;
    reg[1] = 0x00;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00;
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    for (int bank = 0; bank < 8; bank ++) {
      for (int way = 0; way < 8; way ++) {
        for (int idx = 0; idx < 512; idx ++) {
          addr = ID;
          addr = addr << 17; // RSVD
          addr = addr << 3; // BANK_ID
          addr = addr + bank;
          addr = addr << 3; // WAY_ID
          addr = addr + way;
          addr = addr << 2; // CHUNK_ID
          addr = addr << 9; // BANK_INDEX
          addr = addr + idx;
          //fprintf(stderr, "addr = %x, writing\n", addr);
          this->ht->mem.write(addr, 8, (uint8_t*)reg);
          //fprintf(stderr, "addr = %x, done writing\n", addr);
        }
      }
    }
    ltime = time(NULL);
    fprintf(stderr, "Done Setting Valid RAM to 0's for L2 @ %s\n", asctime(localtime(&ltime)));
}
void demo::release_reset_for_mp_l1(std::string key){
  uint8_t reg[8];
  time_t ltime;
  ltime = time(NULL);
  //fprintf(stderr, "Start Releasing Reset for MP L1 @ %s\n", asctime(localtime(&ltime)));
  //reg[0] = 0x0c;
  //reg[1] = 0x00;
  //reg[2] = 0x00;
  //reg[3] = 0x00;
  //reg[4] = 0x00;
  //reg[5] = 0x00;
  //reg[6] = 0x00;
  //reg[7] = 0x00;
  //this->ht->mem.write(0x8000000000 + 8 * 2, 8, (uint8_t*)reg);
  //ltime = time(NULL);
  //fprintf(stderr, "Done Releasing Reset for MP L1 @ %s\n", asctime(localtime(&ltime)));

  //// Releasing Reset for VCORE L1
  //// if (argmap["release_vcore_reset"].length() > 0) {
  //  for (int i = 0; i < 16; i++) {
  //    ltime = time(NULL);
  //    fprintf(stderr, "Start Releasing VCORE %0d L1 Reset @ %s\n", i, asctime(localtime(&ltime)));
  //    reg[0] = 0xfc;
  //    reg[1] = 0x00;
  //    reg[2] = 0x00;
  //    reg[3] = 0x00;
  //    reg[4] = 0x00;
  //    reg[5] = 0x00;
  //    reg[6] = 0x00;
  //    reg[7] = 0x00;
  //    this->ht->mem.write(0x8000000000 + 8 * (10 + i), 8, (uint8_t*)reg);
  //    ltime = time(NULL);
  //    fprintf(stderr, "Done Releasing VCORE %0d L1 Reset @ %s\n", i, asctime(localtime(&ltime)));
  //  }
  // }
    ltime = time(NULL);
    fprintf(stderr, "Start Releasing Reset for MP L1 @ %s\n", asctime(localtime(&ltime)));
    reg[0] = 0x0c;
    reg[1] = 0x00;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00;
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(0x8000000000 + 8 * 2, 8, (uint8_t*)reg);
    ltime = time(NULL);
    fprintf(stderr, "Done Releasing Reset for MP L1 @ %s\n", asctime(localtime(&ltime)));

    // Releasing Reset for VCORE L1
    // if (argmap["release_vcore_reset"].length() > 0) {
      for (int i = 0; i < 16; i++) {
        ltime = time(NULL);
        fprintf(stderr, "Start Releasing VCORE %0d L1 Reset @ %s\n", i, asctime(localtime(&ltime)));
        reg[0] = 0xfc;
        reg[1] = 0x00;
        reg[2] = 0x00;
        reg[3] = 0x00;
        reg[4] = 0x00;
        reg[5] = 0x00;
        reg[6] = 0x00;
        reg[7] = 0x00;
        this->ht->mem.write(0x8000000000 + 8 * (10 + i), 8, (uint8_t*)reg);
        ltime = time(NULL);
        fprintf(stderr, "Done Releasing VCORE %0d L1 Reset @ %s\n", i, asctime(localtime(&ltime)));
      }
}
void demo::test_l1(std::string key){
  uint8_t reg[8];
  time_t ltime;
  ltime = time(NULL);
  fprintf(stderr, "Start Test L1 @ %s\n", asctime(localtime(&ltime)));


  ltime = time(NULL);
  fprintf(stderr, "Done Test L1 @ %s\n", asctime(localtime(&ltime)));
}
void demo::zero_out_l1_tag_ram(std::string key){
  uint8_t reg[8];
  time_t ltime;
  //ltime = time(NULL);
  //fprintf(stderr, "Start Zeroing Out TAG RAM for L1 @ %s\n", asctime(localtime(&ltime)));
  //for (int wayid = 0; wayid < 2; wayid++) {
  //  for (int index = 0; index < 128; index++) {
  //    reg[0] = 0x55; // data == 0
  //    reg[1] = 0xaa;
  //    reg[2] = 0x00;
  //    reg[3] = 0xff;
  //    reg[4] = index; // addr = {'b10001, 1'b wayid, 'b000, 7'b index}
  //    reg[5] = (0x22 + wayid) * 4;
  //    reg[6] = 0xc2; // 'b11000010
  //    //reg[6] = 0xbe; // 'b10111110
  //    reg[7] = 0x01; // valid
  //    this->ht->mem.write(0x8000000000 + 8 * 5, 8, (uint8_t*)reg);
  //  }
  //}
  //for (int wayid = 0; wayid < 2; wayid++) {
  //  for (int index = 0; index < 128; index++) {
  //    reg[0] = 0x00; // data == 0
  //    reg[1] = 0x00;
  //    reg[2] = 0x00;
  //    reg[3] = 0x00;
  //    reg[4] = index; // addr = {'b10001, 1'b wayid, 'b000, 7'b index}
  //    reg[5] = (0x22 + wayid) * 4;
  //    reg[6] = 0xbc; // 'b10111100
  //    reg[7] = 0x01; // valid
  //    this->ht->mem.write(0x8000000000 + 8 * 5, 8, (uint8_t*)reg);
  //    this->ht->mem.read (0x8000000000 + 8 * 0x97, 8, (uint8_t*)reg);
  //    //fprintf(stderr, "w:%0d i=%0d v=0x%02x%02x%02x%02x%02x%02x%02x%02x\n", wayid, index, reg[7], reg[6], reg[5], reg[4], reg[3], reg[2], reg[1], reg[0]);
  //  }
  //}
  //for (int wayid = 0; wayid < 2; wayid++) {
  //  for (int index = 0; index < 128; index++) {
  //    reg[0] = 0x00; // data == 0
  //    reg[1] = 0x00;
  //    reg[2] = 0x00;
  //    reg[3] = 0x00;
  //    reg[4] = index; // addr = {'b10001, 1'b wayid, 'b000, 7'b index}
  //    reg[5] = (0x22 + wayid) * 4;
  //    reg[6] = 0xc2; // 'b11000010
  //    //reg[6] = 0xbe; // 'b10111110
  //    reg[7] = 0x01; // valid
  //    this->ht->mem.write(0x8000000000 + 8 * 5, 8, (uint8_t*)reg);
  //  }
  //}
  //for (int wayid = 0; wayid < 2; wayid++) {
  //  for (int index = 0; index < 128; index++) {
  //    reg[0] = 0x00; // data == 0
  //    reg[1] = 0x00;
  //    reg[2] = 0x00;
  //    reg[3] = 0x00;
  //    reg[4] = index; // addr = {'b10001, 1'b wayid, 'b000, 7'b index}
  //    reg[5] = (0x22 + wayid) * 4;
  //    reg[6] = 0xbc; // 'b10111100
  //    reg[7] = 0x01; // valid
  //    this->ht->mem.write(0x8000000000 + 8 * 5, 8, (uint8_t*)reg);
  //    this->ht->mem.read (0x8000000000 + 8 * 0x97, 8, (uint8_t*)reg);
  //    //fprintf(stderr, "w:%0d i=%0d v=0x%02x%02x%02x%02x%02x%02x%02x%02x\n", wayid, index, reg[7], reg[6], reg[5], reg[4], reg[3], reg[2], reg[1], reg[0]);
  //  }
  //}
  //for (int wayid = 0; wayid < 2; wayid++) {
  //  for (int index = 0; index < 128; index++) {
  //    reg[0] = 0x00; // data == 0
  //    reg[1] = 0x00;
  //    reg[2] = 0x00;
  //    reg[3] = 0x00;
  //    reg[4] = index; // addr = {'b10001, 1'b wayid, 'b000, 7'b index}
  //    reg[5] = (0x22 + wayid) * 4;
  //    reg[6] = 0xbc; // 'b10111100
  //    reg[7] = 0x01; // valid
  //    this->ht->mem.write(0x8000000000 + 8 * 5, 8, (uint8_t*)reg);
  //    this->ht->mem.read (0x8000000000 + 8 * 0x97, 8, (uint8_t*)reg);
  //    fprintf(stderr, "w:%0d i=%0d v=0x%02x%02x%02x%02x%02x%02x%02x%02x\n", wayid, index, reg[7], reg[6], reg[5], reg[4], reg[3], reg[2], reg[1], reg[0]);
  //  }
  //}
  //ltime = time(NULL);
  //fprintf(stderr, "Done Zeroing Out TAG RAM for L1 @ %s\n", asctime(localtime(&ltime)));
    ltime = time(NULL);
    fprintf(stderr, "Start Zeroing Out TAG RAM for L1 @ %s\n", asctime(localtime(&ltime)));
    for (int wayid = 0; wayid < 2; wayid++) {
      for (int index = 0; index < 128; index++) {
        reg[0] = 0x55; // data == 0
        reg[1] = 0xaa;
        reg[2] = 0x00;
        reg[3] = 0xff;
        reg[4] = index; // addr = {'b10001, 1'b wayid, 'b000, 7'b index}
        reg[5] = (0x22 + wayid) * 4;
        reg[6] = 0xc2; // 'b11000010
        //reg[6] = 0xbe; // 'b10111110
        reg[7] = 0x01; // valid
        this->ht->mem.write(0x8000000000 + 8 * 5, 8, (uint8_t*)reg);
      }
    }
    for (int wayid = 0; wayid < 2; wayid++) {
      for (int index = 0; index < 128; index++) {
        reg[0] = 0x00; // data == 0
        reg[1] = 0x00;
        reg[2] = 0x00;
        reg[3] = 0x00;
        reg[4] = index; // addr = {'b10001, 1'b wayid, 'b000, 7'b index}
        reg[5] = (0x22 + wayid) * 4;
        reg[6] = 0xbc; // 'b10111100
        reg[7] = 0x01; // valid
        this->ht->mem.write(0x8000000000 + 8 * 5, 8, (uint8_t*)reg);
        this->ht->mem.read (0x8000000000 + 8 * 0x97, 8, (uint8_t*)reg);
        //fprintf(stderr, "w:%0d i=%0d v=0x%02x%02x%02x%02x%02x%02x%02x%02x\n", wayid, index, reg[7], reg[6], reg[5], reg[4], reg[3], reg[2], reg[1], reg[0]);
      }
    }
    for (int wayid = 0; wayid < 2; wayid++) {
      for (int index = 0; index < 128; index++) {
        reg[0] = 0x00; // data == 0
        reg[1] = 0x00;
        reg[2] = 0x00;
        reg[3] = 0x00;
        reg[4] = index; // addr = {'b10001, 1'b wayid, 'b000, 7'b index}
        reg[5] = (0x22 + wayid) * 4;
        reg[6] = 0xc2; // 'b11000010
        //reg[6] = 0xbe; // 'b10111110
        reg[7] = 0x01; // valid
        this->ht->mem.write(0x8000000000 + 8 * 5, 8, (uint8_t*)reg);
      }
    }
    for (int wayid = 0; wayid < 2; wayid++) {
      for (int index = 0; index < 128; index++) {
        reg[0] = 0x00; // data == 0
        reg[1] = 0x00;
        reg[2] = 0x00;
        reg[3] = 0x00;
        reg[4] = index; // addr = {'b10001, 1'b wayid, 'b000, 7'b index}
        reg[5] = (0x22 + wayid) * 4;
        reg[6] = 0xbc; // 'b10111100
        reg[7] = 0x01; // valid
        this->ht->mem.write(0x8000000000 + 8 * 5, 8, (uint8_t*)reg);
        this->ht->mem.read (0x8000000000 + 8 * 0x97, 8, (uint8_t*)reg);
        //fprintf(stderr, "w:%0d i=%0d v=0x%02x%02x%02x%02x%02x%02x%02x%02x\n", wayid, index, reg[7], reg[6], reg[5], reg[4], reg[3], reg[2], reg[1], reg[0]);
      }
    }
    for (int wayid = 0; wayid < 2; wayid++) {
      for (int index = 0; index < 128; index++) {
        reg[0] = 0x00; // data == 0
        reg[1] = 0x00;
        reg[2] = 0x00;
        reg[3] = 0x00;
        reg[4] = index; // addr = {'b10001, 1'b wayid, 'b000, 7'b index}
        reg[5] = (0x22 + wayid) * 4;
        reg[6] = 0xbc; // 'b10111100
        reg[7] = 0x01; // valid
        this->ht->mem.write(0x8000000000 + 8 * 5, 8, (uint8_t*)reg);
        this->ht->mem.read (0x8000000000 + 8 * 0x97, 8, (uint8_t*)reg);
        fprintf(stderr, "w:%0d i=%0d v=0x%02x%02x%02x%02x%02x%02x%02x%02x\n", wayid, index, reg[7], reg[6], reg[5], reg[4], reg[3], reg[2], reg[1], reg[0]);
      }
    }
    ltime = time(NULL);
    fprintf(stderr, "Done Zeroing Out TAG RAM for L1 @ %s\n", asctime(localtime(&ltime)));
}
void demo::warmup_l1_cache(std::string key){
  uint8_t reg[8];
  time_t ltime;
  //ltime = time(NULL);
  //fprintf(stderr, "Start Warming Up L1 @ %s\n", asctime(localtime(&ltime)));
  //// Write L1 Cache Way0 TAG to be valid
  ////fprintf(stderr, "Write L1 Cache Way0 TAG to be valid\n");
  //for (int wayid = 0; wayid < 2; wayid++) {
  //  for (int index = 0; index < 128; index++) {
  //    reg[0] = 0x00; // data == 0
  //    reg[1] = 0x00;
  //    reg[2] = 0x80;
  //    reg[3] = 0x00;
  //    reg[4] = index; // addr = {'b10001, 1'b wayid, 'b000, 7'b index}
  //    reg[5] = (0x22 + wayid) * 4;
  //    reg[6] = 0xc2; // 'b11000010
  //    reg[7] = 0x01; // valid
  //    this->ht->mem.write(0x8000000000 + 8 * 5, 8, (uint8_t*)reg);
  //  }
  //}
  //// Warm Up
  ////fprintf(stderr, "Warm Up\n");
  //for (int i = 0; i < 128; i ++) {
  //  // Load test to L2
  //  fprintf(stderr, "Load test to L2\n");
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x10 + 32 * i, 0x0000001300000013); // Previous Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0xf  + 32 * i, 0x0000001300000013); // Previous Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0xe  + 32 * i, 0x0000001300000013); // Previous Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0xd  + 32 * i, 0x0000001300000013); // Previous Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0xc  + 32 * i, 0x0000001300000013); // Previous Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0xb  + 32 * i, 0x0000001300000013); // Previous Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0xa  + 32 * i, 0x0000001300000013); // Previous Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x9  + 32 * i, 0x0000001300000013); // Previous Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x8  + 32 * i, 0x0000001300000013); // Previous Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x7  + 32 * i, 0x0000001300000013); // Previous Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x6  + 32 * i, 0x0000001300000013); // Previous Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x5  + 32 * i, 0x0000001300000013); // Previous Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x4  + 32 * i, 0x0000001300000013); // Previous Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x3  + 32 * i, 0x0000001300000013); // Previous Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x2  + 32 * i, 0x0000001300000013); // Previous Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x1  + 32 * i, 0x0000001300000013); // Previous Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000            + 32 * i, 0x0000006f0000006f); // Current Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x1  + 32 * i, 0x0000006f0000006f); // Current Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x2  + 32 * i, 0x0000006f0000006f); // Current Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x3  + 32 * i, 0x0000006f0000006f); // Current Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x4  + 32 * i, 0x0000006f0000006f); // Current Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x5  + 32 * i, 0x0000006f0000006f); // Current Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x6  + 32 * i, 0x0000006f0000006f); // Current Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x7  + 32 * i, 0x0000006f0000006f); // Current Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x8  + 32 * i, 0x0000006f0000006f); // Current Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x9  + 32 * i, 0x0000006f0000006f); // Current Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0xa  + 32 * i, 0x0000006f0000006f); // Current Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0xb  + 32 * i, 0x0000006f0000006f); // Current Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0xc  + 32 * i, 0x0000006f0000006f); // Current Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0xd  + 32 * i, 0x0000006f0000006f); // Current Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0xe  + 32 * i, 0x0000006f0000006f); // Current Line
  //  this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0xf  + 32 * i, 0x0000006f0000006f); // Current Line
  //  // Set rst_pc
  //  fprintf(stderr, "Set rst_pc = 0x%x\n", 0x20000000 + i * 0x10000 + i * 32 - 4);
  //  this->ht->OURSBUS_WRITE(0x8000000000 + 8 * 0x3, 0x10000000 + i * 0x10000 + i * 32 - 4);
  //  // Release ORV Reset
  //  fprintf(stderr, "Release ORV Reset\n");
  //  this->ht->OURSBUS_WRITE(0x8000000000 + 8 * 0x2, 0x3f8);
  //  // Timing
  //  uint64_t data = 0;
  //  for (int i = 0; i < 50; i ++)
  //    this->ht->OURSBUS_READ(0x8000000000, data);
  //  // Reset ORV
  //  fprintf(stderr, "Reset ORV\n");
  //  this->ht->OURSBUS_WRITE(0x8000000000 + 8 * 0x2, 0x3fc);
  //}
  //if (this->ht->argmap["release_vcore_reset"].length() > 0) {
  //  // Release VCORE Reset
  //  for (int i = 0; i < 16; i++) {
  //    ltime = time(NULL);
  //    fprintf(stderr, "L1 Warm Up: Start Releasing VCORE %0d Reset @ %s\n", i, asctime(localtime(&ltime)));
  //    reg[0] = 0xf8;
  //    reg[1] = 0x00;
  //    reg[2] = 0x00;
  //    reg[3] = 0x00;
  //    reg[4] = 0x00;
  //    reg[5] = 0x00;
  //    reg[6] = 0x00;
  //    reg[7] = 0x00;
  //    this->ht->mem.write(0x8000000000 + 8 * (10 + i), 8, (uint8_t*)reg);
  //    ltime = time(NULL);
  //    fprintf(stderr, "L1 Warm Up: Done Releasing VCORE %0d Reset @ %s\n", i, asctime(localtime(&ltime)));
  //  }
  //  // Load Upper Half of L2 to be NOOPs and report done and wfe
  //  fprintf(stderr, "Load Upper Half of L2 to be NOOPs and report done and wfe\n");
  //  for (int i = 0; i < 65536; i++) {
  //    this->ht->OURSBUS_WRITE(0x20080000 + i * 8, 0x0000001300000013);
  //  }
  //  this->ht->OURSBUS_WRITE(0x20080000 + 8 * 65518, 0x79c0d07300000013);
  //  this->ht->OURSBUS_WRITE(0x20080000 + 8 * 65519, 0x1060007379c05073);
  //  //Load Lower Half of L2 to be MP program
  //  fprintf(stderr, "Load Lower Half of L2 to be MP program\n");
  //  uint32_t inst[40] = {
  //    0x00000097,
  //    0x00008093,
  //    0x00100113,
  //    0x01311113,
  //    0x001101b3,
  //    0x00300213,
  //    0x00000313,
  //    0x00c00393,
  //    0x7ff00493,
  //    0x00149493,
  //    0x00148493,
  //    0x00100513,
  //    0x01351513,
  //    0x01451513,
  //    0x02050513,
  //    0x60d19073,
  //    0x60c21073,
  //    0x00000293,
  //    0x60e022f3,
  //    0xfe028ce3,
  //    0x60e01073,
  //    0x60c01073,
  //    0x00130313,
  //    0x01020213,
  //    0xfc734ee3,
  //    0x60902473,
  //    0xfe941ee3,
  //    0x00853023,
  //    0x0000006f,
  //    0x00000000,
  //    0x00000000,
  //    0x00000000,
  //    0x00000000,
  //    0x00000000,
  //    0x00000000,
  //    0x00000000,
  //    0x00000000,
  //    0x00000000,
  //    0x00000000,
  //    0x00000000
  //  };
  //  uint64_t addr = 0x20000000;
  //  for (int i = 0; i < 20; i++) {
  //    reg[7] = (inst[i * 2 + 1] >> 24) & 0xff;
  //    reg[6] = (inst[i * 2 + 1] >> 16) & 0xff;
  //    reg[5] = (inst[i * 2 + 1] >>  8) & 0xff;
  //    reg[4] = (inst[i * 2 + 1] >>  0) & 0xff;
  //    reg[3] = (inst[i * 2 + 0] >> 24) & 0xff;
  //    reg[2] = (inst[i * 2 + 0] >> 16) & 0xff;
  //    reg[1] = (inst[i * 2 + 0] >>  8) & 0xff;
  //    reg[0] = (inst[i * 2 + 0] >>  0) & 0xff;
  //    this->ht->mem.write(addr, 8, (uint8_t*)reg);
  //    addr += 8;
  //  }
  //  // Set rst_pc
  //  fprintf(stderr, "Set rst_pc\n");
  //  this->ht->OURSBUS_WRITE(0x8000000000 + 8 * 0x3, 0x20000000);
  //  // Release ORV Reset
  //  fprintf(stderr, "Release ORV Reset\n");
  //  this->ht->OURSBUS_WRITE(0x8000000000 + 8 * 0x2, 0x3f8);
  //  // Poking BRKPT to be expected value
  //  fprintf(stderr, "Poking BRKPT\n");
  //  do {
  //    this->ht->OURSBUS_READ(0x8000000000 + 8 * 0x4, addr);
  //  } while (addr != 0xfff);
  //  // Reset VCORE
  //  fprintf(stderr, "Reset VCORE\n");
  //  for (int i = 0; i < 16; i++) {
  //    ltime = time(NULL);
  //    fprintf(stderr, "Start Setting VCORE %0d Reset @ %s\n", i, asctime(localtime(&ltime)));
  //    reg[0] = 0xfc;
  //    reg[1] = 0x00;
  //    reg[2] = 0x00;
  //    reg[3] = 0x00;
  //    reg[4] = 0x00;
  //    reg[5] = 0x00;
  //    reg[6] = 0x00;
  //    reg[7] = 0x00;
  //    this->ht->mem.write(0x8000000000 + 8 * (10 + i), 8, (uint8_t*)reg);
  //    ltime = time(NULL);
  //    fprintf(stderr, "Done Setting VCORE %0d Reset @ %s\n", i, asctime(localtime(&ltime)));
  //  }
  //  // Reset ORV
  //  fprintf(stderr, "Reset ORV\n");
  //  this->ht->OURSBUS_WRITE(0x8000000000 + 8 * 0x2, 0x3fc);
  //}
  //ltime = time(NULL);
  //fprintf(stderr, "Done Warming Up L1 @ %s\n", asctime(localtime(&ltime)));
    ltime = time(NULL);
    fprintf(stderr, "Start Warming Up L1 @ %s\n", asctime(localtime(&ltime)));
    // Write L1 Cache Way0 TAG to be valid
    //fprintf(stderr, "Write L1 Cache Way0 TAG to be valid\n");
    for (int wayid = 0; wayid < 2; wayid++) {
      for (int index = 0; index < 128; index++) {
        reg[0] = 0x00; // data == 0
        reg[1] = 0x00;
        reg[2] = 0x80;
        reg[3] = 0x00;
        reg[4] = index; // addr = {'b10001, 1'b wayid, 'b000, 7'b index}
        reg[5] = (0x22 + wayid) * 4;
        reg[6] = 0xc2; // 'b11000010
        reg[7] = 0x01; // valid
        this->ht->mem.write(0x8000000000 + 8 * 5, 8, (uint8_t*)reg);
      }
    }
    // Warm Up
    //fprintf(stderr, "Warm Up\n");
    for (int i = 0; i < 128; i ++) {
      // Load test to L2
      fprintf(stderr, "Load test to L2\n");
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x10 + 32 * i, 0x0000001300000013); // Previous Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0xf  + 32 * i, 0x0000001300000013); // Previous Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0xe  + 32 * i, 0x0000001300000013); // Previous Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0xd  + 32 * i, 0x0000001300000013); // Previous Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0xc  + 32 * i, 0x0000001300000013); // Previous Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0xb  + 32 * i, 0x0000001300000013); // Previous Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0xa  + 32 * i, 0x0000001300000013); // Previous Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x9  + 32 * i, 0x0000001300000013); // Previous Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x8  + 32 * i, 0x0000001300000013); // Previous Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x7  + 32 * i, 0x0000001300000013); // Previous Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x6  + 32 * i, 0x0000001300000013); // Previous Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x5  + 32 * i, 0x0000001300000013); // Previous Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x4  + 32 * i, 0x0000001300000013); // Previous Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x3  + 32 * i, 0x0000001300000013); // Previous Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x2  + 32 * i, 0x0000001300000013); // Previous Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 - 8 * 0x1  + 32 * i, 0x0000001300000013); // Previous Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000            + 32 * i, 0x0000006f0000006f); // Current Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x1  + 32 * i, 0x0000006f0000006f); // Current Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x2  + 32 * i, 0x0000006f0000006f); // Current Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x3  + 32 * i, 0x0000006f0000006f); // Current Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x4  + 32 * i, 0x0000006f0000006f); // Current Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x5  + 32 * i, 0x0000006f0000006f); // Current Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x6  + 32 * i, 0x0000006f0000006f); // Current Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x7  + 32 * i, 0x0000006f0000006f); // Current Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x8  + 32 * i, 0x0000006f0000006f); // Current Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0x9  + 32 * i, 0x0000006f0000006f); // Current Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0xa  + 32 * i, 0x0000006f0000006f); // Current Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0xb  + 32 * i, 0x0000006f0000006f); // Current Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0xc  + 32 * i, 0x0000006f0000006f); // Current Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0xd  + 32 * i, 0x0000006f0000006f); // Current Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0xe  + 32 * i, 0x0000006f0000006f); // Current Line
      this->ht->OURSBUS_WRITE(0x10000000 + i * 0x10000 + 8 * 0xf  + 32 * i, 0x0000006f0000006f); // Current Line
      // Set rst_pc
      fprintf(stderr, "Set rst_pc = 0x%x\n", 0x20000000 + i * 0x10000 + i * 32 - 4);
      this->ht->OURSBUS_WRITE(0x8000000000 + 8 * 0x3, 0x10000000 + i * 0x10000 + i * 32 - 4);
      // Release ORV Reset
      fprintf(stderr, "Release ORV Reset\n");
      this->ht->OURSBUS_WRITE(0x8000000000 + 8 * 0x2, 0x3f8);
      // Timing
      uint64_t data;
      for (int i = 0; i < 50; i ++)
        this->ht->OURSBUS_READ(0x8000000000, data);
      // Reset ORV
      fprintf(stderr, "Reset ORV\n");
      this->ht->OURSBUS_WRITE(0x8000000000 + 8 * 0x2, 0x3fc);
    }
    if (this->ht->argmap["release_vcore_reset"].length() > 0) {
      // Release VCORE Reset
      for (int i = 0; i < 16; i++) {
        ltime = time(NULL);
        fprintf(stderr, "L1 Warm Up: Start Releasing VCORE %0d Reset @ %s\n", i, asctime(localtime(&ltime)));
        reg[0] = 0xf8;
        reg[1] = 0x00;
        reg[2] = 0x00;
        reg[3] = 0x00;
        reg[4] = 0x00;
        reg[5] = 0x00;
        reg[6] = 0x00;
        reg[7] = 0x00;
        this->ht->mem.write(0x8000000000 + 8 * (10 + i), 8, (uint8_t*)reg);
        ltime = time(NULL);
        fprintf(stderr, "L1 Warm Up: Done Releasing VCORE %0d Reset @ %s\n", i, asctime(localtime(&ltime)));
      }
      // Load Upper Half of L2 to be NOOPs and report done and wfe
      fprintf(stderr, "Load Upper Half of L2 to be NOOPs and report done and wfe\n");
      for (int i = 0; i < 65536; i++) {
        this->ht->OURSBUS_WRITE(0x20080000 + i * 8, 0x0000001300000013);
      }
      this->ht->OURSBUS_WRITE(0x20080000 + 8 * 65518, 0x79c0d07300000013);
      this->ht->OURSBUS_WRITE(0x20080000 + 8 * 65519, 0x1060007379c05073);
      //Load Lower Half of L2 to be MP program
      fprintf(stderr, "Load Lower Half of L2 to be MP program\n");
      uint32_t inst[40] = {
        0x00000097,
        0x00008093,
        0x00100113,
        0x01311113,
        0x001101b3,
        0x00300213,
        0x00000313,
        0x00c00393,
        0x7ff00493,
        0x00149493,
        0x00148493,
        0x00100513,
        0x01351513,
        0x01451513,
        0x02050513,
        0x60d19073,
        0x60c21073,
        0x00000293,
        0x60e022f3,
        0xfe028ce3,
        0x60e01073,
        0x60c01073,
        0x00130313,
        0x01020213,
        0xfc734ee3,
        0x60902473,
        0xfe941ee3,
        0x00853023,
        0x0000006f,
        0x00000000,
        0x00000000,
        0x00000000,
        0x00000000,
        0x00000000,
        0x00000000,
        0x00000000,
        0x00000000,
        0x00000000,
        0x00000000,
        0x00000000
      };
      uint64_t addr = 0x20000000;
      for (int i = 0; i < 20; i++) {
        reg[7] = (inst[i * 2 + 1] >> 24) & 0xff;
        reg[6] = (inst[i * 2 + 1] >> 16) & 0xff;
        reg[5] = (inst[i * 2 + 1] >>  8) & 0xff;
        reg[4] = (inst[i * 2 + 1] >>  0) & 0xff;
        reg[3] = (inst[i * 2 + 0] >> 24) & 0xff;
        reg[2] = (inst[i * 2 + 0] >> 16) & 0xff;
        reg[1] = (inst[i * 2 + 0] >>  8) & 0xff;
        reg[0] = (inst[i * 2 + 0] >>  0) & 0xff;
        this->ht->mem.write(addr, 8, (uint8_t*)reg);
        addr += 8;
      }
      // Set rst_pc
      fprintf(stderr, "Set rst_pc\n");
      this->ht->OURSBUS_WRITE(0x8000000000 + 8 * 0x3, 0x20000000);
      // Release ORV Reset
      fprintf(stderr, "Release ORV Reset\n");
      this->ht->OURSBUS_WRITE(0x8000000000 + 8 * 0x2, 0x3f8);
      // Poking BRKPT to be expected value
      fprintf(stderr, "Poking BRKPT\n");
      do {
        this->ht->OURSBUS_READ(0x8000000000 + 8 * 0x4, addr);
      } while (addr != 0xfff);
      // Reset VCORE
      fprintf(stderr, "Reset VCORE\n");
      for (int i = 0; i < 16; i++) {
        ltime = time(NULL);
        fprintf(stderr, "Start Setting VCORE %0d Reset @ %s\n", i, asctime(localtime(&ltime)));
        reg[0] = 0xfc;
        reg[1] = 0x00;
        reg[2] = 0x00;
        reg[3] = 0x00;
        reg[4] = 0x00;
        reg[5] = 0x00;
        reg[6] = 0x00;
        reg[7] = 0x00;
        this->ht->mem.write(0x8000000000 + 8 * (10 + i), 8, (uint8_t*)reg);
        ltime = time(NULL);
        fprintf(stderr, "Done Setting VCORE %0d Reset @ %s\n", i, asctime(localtime(&ltime)));
      }
      // Reset ORV
      fprintf(stderr, "Reset ORV\n");
      this->ht->OURSBUS_WRITE(0x8000000000 + 8 * 0x2, 0x3fc);
    }
    ltime = time(NULL);
    fprintf(stderr, "Done Warming Up L1 @ %s\n", asctime(localtime(&ltime)));
}

void demo::load_kernel(std::string key){
  uint8_t reg[8];
  time_t ltime;
   //std::map<std::string, uint64_t> symbols;
  //ltime = time(NULL);
  //if (this->ht->argmap["load_pk"].length() > 0) {
  //  fprintf(stderr, "Start Loading Kernel @ %s\n", asctime(localtime(&ltime)));
  //  this->ht->symbols = load_elf(this->ht->path.c_str(), &(this->ht->mem), &(this->ht->entry));
  //  if (this->ht->argmap["load_2nd_file"].length() == 0) {
  //    this->ht->program_symbols = this->ht->symbols;
  //  }
  //} else {
  //  fprintf(stderr, "Start Loading Kernel Symbols @ %s\n", asctime(localtime(&ltime)));
  //  this->ht->symbols = load_elf_symbols_only(this->ht->path.c_str(), &this->ht->mem, &(this->ht->entry));
  //}
  //for (std::map<std::string, uint64_t>::iterator it = this->ht->symbols.begin(); it != this->ht->symbols.end(); ++it) {
  //  if (it->first.substr(0, 6) == "tohost") {
  //    if (it->first.length() == 6) {
  //      this->ht->tohost_addr = it->second;
  //    } else if (it->first.length() > 6) {
  //      if (it->first.substr(6, 1) == ".") {
  //        this->ht->tohost_addr = it->second;
  //      }
  //    }
  //  }
  //  if (it->first.substr(0, 8) == "fromhost") {
  //    if (it->first.length() == 8) {
  //      this->ht->fromhost_addr = it->second;
  //    } else if (it->first.length() > 8) {
  //      if (it->first.substr(8, 1) == ".") {
  //        this->ht->fromhost_addr = it->second;
  //      }
  //    }
  //  }
  //}
  //if ((this->ht->tohost_addr == 0) && (this->ht->fromhost_addr == 0)) {
  //  fprintf(stderr, "warning: tohost and fromhost symbols not in ELF; can't communicate with target\n");
  //}
  //ltime = time(NULL);
  //if (this->ht->argmap["load_pk"].length() > 0) {
  //  fprintf(stderr, "Done Loading Kernel @ %s\n", asctime(localtime(&ltime)));
  //} else {
  //  fprintf(stderr, "Done Loading Kernel Symbols @ %s\n", asctime(localtime(&ltime)));
  //}
  //if (this->ht->argmap["filename2"].length() > 0) {
  //  std::string path;
  //  if (access(this->ht->argmap["filename2"].c_str(), F_OK) == 0)
  //    path = this->ht->argmap["filename2"];
  //  else if (this->ht->argmap["filename2"].find('/') == std::string::npos)
  //  {
  //    std::string test_path = PREFIX TARGET_DIR + this->ht->argmap["filename2"];
  //    if (access(test_path.c_str(), F_OK) == 0)
  //      path = test_path;
  //  }

  //  if (path.empty())
  //    throw std::runtime_error("could not open " + this->ht->argmap["filename2"]);

  //  ltime = time(NULL);
  //  if (this->ht->argmap["load_2nd_file"].length() > 0) {
  //    fprintf(stderr, "Start Loading 2nd File as ELF, Symbols will be put in program_symbols @ %s\n", asctime(localtime(&ltime)));
  //    this->ht->program_symbols = load_elf(path.c_str(), &this->ht->mem, &(this->ht->entry));
  //  } else {
  //    fprintf(stderr, "Start Loading 2nd File Symbols, will be put in program_symbols @ %s\n", asctime(localtime(&ltime)));
  //    this->ht->program_symbols = load_elf_symbols_only(path.c_str(), &this->ht->mem, &(this->ht->entry));
  //  }
  //  ltime = time(NULL);
  //  if (this->ht->argmap["load_2nd_file"].length() > 0) {
  //    fprintf(stderr, "Done Loading 2nd File as ELF @ %s\n", asctime(localtime(&ltime)));
  //  } else {
  //    fprintf(stderr, "Done Loading 2nd File Symbols @ %s\n", asctime(localtime(&ltime)));
  //  }
  //}

  //if (this->ht->argmap["filename3"].length() > 0) {
  //  std::string path;
  //  if (access(this->ht->argmap["filename3"].c_str(), F_OK) == 0)
  //    path = this->ht->argmap["filename3"];
  //  else if (this->ht->argmap["filename3"].find('/') == std::string::npos)
  //  {
  //    std::string test_path = PREFIX TARGET_DIR + this->ht->argmap["filename3"];
  //    if (access(test_path.c_str(), F_OK) == 0)
  //      path = test_path;
  //  }

  //  if (path.empty())
  //    throw std::runtime_error("could not open " + this->ht->argmap["filename3"]);
  //  ltime = time(NULL);
  //  if (this->ht->argmap["load_3rd_file"].length() > 0) {
  //    fprintf(stderr, "Start Loading 3rd File as ELF, Symbols will be put in program_symbols @ %s\n", asctime(localtime(&ltime)));
  //    this->ht->program_symbols = load_elf(path.c_str(), &this->ht->mem, &(this->ht->entry));
  //  } else {
  //    fprintf(stderr, "Start Loading 3rd File Symbols, will be put in program_symbols @ %s\n", asctime(localtime(&ltime)));
  //    this->ht->program_symbols = load_elf_symbols_only(path.c_str(), &this->ht->mem, &(this->ht->entry));
  //  }
  //  ltime = time(NULL);
  //  if (this->ht->argmap["load_3rd_file"].length() > 0) {
  //    fprintf(stderr, "Done Loading 3rd File as ELF @ %s\n", asctime(localtime(&ltime)));
  //  } else {
  //    fprintf(stderr, "Done Loading 3rd File Symbols @ %s\n", asctime(localtime(&ltime)));
  //  }
  //}
  if (this->ht->argmap["load_pk"].length() > 0) {
    fprintf(stderr, "Start Loading Kernel @ %s\n", asctime(localtime(&ltime)));
    this->ht->symbols = load_elf(this->ht->path.c_str(), &(this->ht->mem), &(this->ht->entry));
    if (this->ht->argmap["load_2nd_file"].length() == 0) {
      this->ht->program_symbols = this->ht->symbols;
    }
  } else {
    fprintf(stderr, "Start Loading Kernel Symbols @ %s\n", asctime(localtime(&ltime)));
    this->ht->symbols = load_elf_symbols_only(this->ht->path.c_str(), &(this->ht->mem), &(this->ht->entry));
  }
  for (std::map<std::string, uint64_t>::iterator it = this->ht->symbols.begin(); it != this->ht->symbols.end(); ++it) {
    if (it->first.substr(0, 6) == "tohost") {
      if (it->first.length() == 6) {
        this->ht->tohost_addr = it->second;
      } else if (it->first.length() > 6) {
        if (it->first.substr(6, 1) == ".") {
          this->ht->tohost_addr = it->second;
        }
      }
    }
    if (it->first.substr(0, 8) == "fromhost") {
      if (it->first.length() == 8) {
        this->ht->fromhost_addr = it->second;
      } else if (it->first.length() > 8) {
        if (it->first.substr(8, 1) == ".") {
          this->ht->fromhost_addr = it->second;
        }
      }
    }
  }
  if ((this->ht->tohost_addr == 0) && (this->ht->fromhost_addr == 0)) {
    fprintf(stderr, "warning: tohost and fromhost symbols not in Kernel ELF\n");
  }
  ltime = time(NULL);
  if (this->ht->argmap["load_pk"].length() > 0) {
    fprintf(stderr, "Done Loading Kernel @ %s\n", asctime(localtime(&ltime)));
  } else {
    fprintf(stderr, "Done Loading Kernel Symbols @ %s\n", asctime(localtime(&ltime)));
  }

  if (this->ht->argmap["filename2"].length() > 0) {
    std::string path;
    if (access(this->ht->argmap["filename2"].c_str(), F_OK) == 0)
      this->ht->path = this->ht->argmap["filename2"];
    else if (this->ht->argmap["filename2"].find('/') == std::string::npos)
    {
      std::string test_path = PREFIX TARGET_DIR + this->ht->argmap["filename2"];
      if (access(test_path.c_str(), F_OK) == 0)
        this->ht->path = test_path;
    }
    if (this->ht->path.empty())
      throw std::runtime_error("could not open " + this->ht->argmap["filename2"]);

    ltime = time(NULL);
    if (this->ht->argmap["load_2nd_file"].length() > 0) {
      fprintf(stderr, "Start Loading 2nd File as ELF, Symbols will be put in program_symbols @ %s\n", asctime(localtime(&ltime)));
      this->ht->program_symbols = load_elf(this->ht->path.c_str(), &(this->ht->mem), &(this->ht->entry));
    } else {
      fprintf(stderr, "Start Loading 2nd File Symbols, will be put in program_symbols @ %s\n", asctime(localtime(&ltime)));
      this->ht->program_symbols = load_elf_symbols_only(this->ht->path.c_str(), &(this->ht->mem), &(this->ht->entry));
    }
    if ((this->ht->tohost_addr == 0) && (this->ht->fromhost_addr == 0)) {
      for (std::map<std::string, uint64_t>::iterator it = this->ht->program_symbols.begin(); it != this->ht->program_symbols.end(); ++it) {
        if (it->first.substr(0, 6) == "tohost") {
          if (it->first.length() == 6) {
            this->ht->tohost_addr = it->second;
          } else if (it->first.length() > 6) {
            if (it->first.substr(6, 1) == ".") {
              this->ht->tohost_addr = it->second;
            }
          }
        }
        if (it->first.substr(0, 8) == "fromhost") {
          if (it->first.length() == 8) {
            this->ht->fromhost_addr = it->second;
          } else if (it->first.length() > 8) {
            if (it->first.substr(8, 1) == ".") {
              this->ht->fromhost_addr = it->second;
            }
          }
        }
      }
    }
    if ((this->ht->tohost_addr == 0) && (this->ht->fromhost_addr == 0)) {
      fprintf(stderr, "warning: tohost and fromhost symbols not in 2nd ELF; can't communicate with target\n");
    }
    ltime = time(NULL);
    if (this->ht->argmap["load_2nd_file"].length() > 0) {
      fprintf(stderr, "Done Loading 2nd File as ELF @ %s\n", asctime(localtime(&ltime)));
    } else {
      fprintf(stderr, "Done Loading 2nd File Symbols @ %s\n", asctime(localtime(&ltime)));
    }
  }
}

void demo::load_image(std::string key){
  uint8_t reg[8];
  time_t ltime;
  // Load Image
  if (this->ht->argmap["load_image="].length() > 0) {
    fprintf(stderr, "Start Loading Image @ %s\n", asctime(localtime(&ltime)));
    uint64_t image_addr = 0;
    // Search 1st symbol with image
    for (std::map<std::string, uint64_t>::iterator it = this->ht->symbols.begin(); it != this->ht->symbols.end(); ++it) {
      if (it->first.substr(0, 6) == "image.") {
        image_addr = it->second;
        fprintf(stderr, "Loading Image File: %s\n", this->ht->argmap["load_image="].c_str());
        fprintf(stderr, "Loading Image to Symbol: %s (Address: %lx)\n", it->first.c_str(), image_addr);
        break;
      }
    }
    if (image_addr == 0) {
      for (std::map<std::string, uint64_t>::iterator it = this->ht->program_symbols.begin(); it != this->ht->program_symbols.end(); ++it) {
        if (it->first.substr(0, 6) == "image.") {
          image_addr = it->second;
          fprintf(stderr, "Loading Image File: %s\n", this->ht->argmap["load_image="].c_str());
          fprintf(stderr, "Loading Image to Symbol: %s (Address: %lx)\n", it->first.c_str(), image_addr);
          break;
        }
      }
      if (image_addr == 0) {
        fprintf(stderr, "Image Symbol: image.* is not able to be found!!\n");
        abort();
      }
    }
    int fd = open(this->ht->argmap["load_image="].c_str(), O_RDONLY);
    struct stat s;
    assert (fd != -1);
    if (fstat(fd, &s) < 0)
      abort();
    size_t size = s.st_size;
    char* buf = (char*) mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
    assert(buf != MAP_FAILED);
    close(fd);
    fprintf(stderr, "Image Size: %ld Bytes\n", size);
    size_t cnt = 0;
    while (cnt < size) {
      reg[0] = *(buf + cnt + 0);
      reg[1] = *(buf + cnt + 1);
      reg[2] = *(buf + cnt + 2);
      reg[3] = *(buf + cnt + 3);
      reg[4] = *(buf + cnt + 4);
      reg[5] = *(buf + cnt + 5);
      reg[6] = *(buf + cnt + 6);
      reg[7] = *(buf + cnt + 7);
      this->ht->mem.write(image_addr + cnt, 8, (uint8_t*)reg);
      cnt += 8;
    }
    fprintf(stderr, "Done Loading Image @ %s\n", asctime(localtime(&ltime)));
  }
}

void demo::load_weight(std::string key){
  uint8_t reg[8];
  time_t ltime;
  fprintf(stderr, "Start Loading Weight @ %s\n", asctime(localtime(&ltime)));
  uint64_t weight_addr = 0;
  // Search 1st symbol with weight
  for (std::map<std::string, uint64_t>::iterator it = this->ht->symbols.begin(); it != this->ht->symbols.end(); ++it) {
    if (it->first.substr(0, 8) == "weights.") {
      weight_addr = it->second;
      fprintf(stderr, "Loading Weight File: %s\n",this->ht->argmap["load_weight="].c_str());
      fprintf(stderr, "Loading Weight to Symbol: %s (Address: %lx)\n", it->first.c_str(), weight_addr);
      break;
    }
  }
  if (weight_addr == 0) {
    for (std::map<std::string, uint64_t>::iterator it = this->ht->program_symbols.begin(); it != this->ht->program_symbols.end(); ++it) {
      if (it->first.substr(0, 8) == "weights.") {
        weight_addr = it->second;
        fprintf(stderr, "Loading Weight File: %s\n", this->ht->argmap["load_weight="].c_str());
        fprintf(stderr, "Loading Weight to Symbol: %s (Address: %lx)\n", it->first.c_str(), weight_addr);
        break;
      }
    }
    if (weight_addr == 0) {
      fprintf(stderr, "Weight Symbol: weight.* is not able to be found!!\n");
      abort();
    }
  }
  int fd = open(this->ht->argmap["load_weight="].c_str(), O_RDONLY);
  struct stat s;
  assert (fd != -1);
  if (fstat(fd, &s) < 0)
    abort();
  size_t size = s.st_size;
  char* buf = (char*) mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
  assert(buf != MAP_FAILED);
  close(fd);
  fprintf(stderr, "Weight Size: %ld Bytes\n", size);
  size_t cnt = 0;
  while (cnt < size) {
    reg[0] = *(buf + cnt + 0);
    reg[1] = *(buf + cnt + 1);
    reg[2] = *(buf + cnt + 2);
    reg[3] = *(buf + cnt + 3);
    reg[4] = *(buf + cnt + 4);
    reg[5] = *(buf + cnt + 5);
    reg[6] = *(buf + cnt + 6);
    reg[7] = *(buf + cnt + 7);
    this->ht->mem.write(weight_addr + cnt, 8, (uint8_t*)reg);
    cnt += 8;
  }
  fprintf(stderr, "Done Loading Weight @ %s\n", asctime(localtime(&ltime)));
}

//agrmap
void demo::alloc_stack(std::string key){
  uint8_t reg[8];
  time_t ltime;
  ltime = time(NULL);
  fprintf(stderr, "Start Allocating 8K Stack with Address 0x%s @ %s\n", this->ht->argmap["alloc_sp="].c_str(), asctime(localtime(&ltime)));
  char * pEnd;
  for (int i = 0; i < 1024; i++) {
    reg[0] = 0x00;
    reg[1] = 0x00;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00;
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(std::strtol(this->ht->argmap["alloc_sp="].c_str(), &pEnd, 16) - (i * 8), 8, (uint8_t*)reg);
  }
  for (int i = 0; i < 1024; i++) {
    reg[0] = 0x00;
    reg[1] = 0x00;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00;
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(std::strtol(this->ht->argmap["alloc_sp="].c_str(), &pEnd, 16) + ((i+1) * 8), 8, (uint8_t*)reg);
  }

  ltime = time(NULL);
  fprintf(stderr, "Done Allocating 8K Stack @ %s\n", asctime(localtime(&ltime)));
}

void demo::set_magic_mem_address(std::string key){
  uint8_t reg[8];
  time_t ltime;
  //ltime = time(NULL);
  //fprintf(stderr, "Start Setting Magic Mem Address @ %s\n", asctime(localtime(&ltime)));
  //// Zero out???
  //this->ht->mem.read(0x844000d000, 8, (uint8_t*)reg);
  //reg[0] = 0x00;
  //reg[1] = 0x00;
  //reg[2] = 0x00;
  //reg[3] = 0x00;
  //reg[4] = 0x00;
  //reg[5] = 0x00;
  //reg[6] = 0x00;
  //reg[7] = 0x00;
  //this->ht->mem.write(0x844000d000, 8, (uint8_t*)reg);
  //this->ht->mem.read(0x844000d000, 8, (uint8_t*)reg);
  //this->ht->mem.read(0x844000d008, 8, (uint8_t*)reg);
  //reg[0] = 0x00;
  //reg[1] = 0x00;
  //reg[2] = 0x00;
  //reg[3] = 0x00;
  //reg[4] = 0x00;
  //reg[5] = 0x00;
  //reg[6] = 0x00;
  //reg[7] = 0x00;
  //this->ht->mem.write(0x844000d008, 8, (uint8_t*)reg);
  //this->ht->mem.read(0x844000d008, 8, (uint8_t*)reg);
  //// Start Address
  //reg[0] = 0x00;
  //reg[1] = 0x00;
  //reg[2] = 0x00;
  //reg[3] = 0x00;
  //reg[4] = 0x00;
  //reg[5] = 0x00;
  //reg[6] = 0x00;
  //reg[7] = 0x00;
  //this->ht->mem.write(0x8000000000 + 8 * 6, 8, (uint8_t*)reg);
  //// End Address
  //reg[0] = 0x00;
  //reg[1] = 0x00;
  //reg[2] = 0x00;
  //reg[3] = 0x00;
  //reg[4] = 0x00;
  //reg[5] = 0x00;
  //reg[6] = 0x00;
  //reg[7] = 0x00;
  //this->ht->mem.write(0x8000000000 + 8 * 7, 8, (uint8_t*)reg);
  //// From Host
  ////reg[0] = 0x00;
  ////reg[1] = 0x00;
  ////reg[2] = 0x00;
  ////reg[3] = 0x00;
  ////reg[4] = 0x00;
  ////reg[5] = 0x00;
  ////reg[6] = 0x00;
  ////reg[7] = 0x00;
  //reg[0] = (this->ht->fromhost_addr >>  0) & 0xff;
  //reg[1] = (this->ht->fromhost_addr >>  8) & 0xff;
  //reg[2] = (this->ht->fromhost_addr >> 16) & 0xff;
  //reg[3] = (this->ht->fromhost_addr >> 24) & 0xff;
  //reg[4] = (this->ht->fromhost_addr >> 32) & 0xff;
  //reg[5] = (this->ht->fromhost_addr >> 40) & 0xff;
  //reg[6] = (this->ht->fromhost_addr >> 48) & 0xff;
  //reg[7] = (this->ht->fromhost_addr >> 56) & 0xff;
  //this->ht->mem.write(0x8000000000 + 8 * 8, 8, (uint8_t*)reg);
  //// To Host
  ////reg[0] = 0x00;
  ////reg[1] = 0x00;
  ////reg[2] = 0x00;
  ////reg[3] = 0x00;
  ////reg[4] = 0x00;
  ////reg[5] = 0x00;
  ////reg[6] = 0x00;
  ////reg[7] = 0x00;
  //reg[0] = (this->ht->tohost_addr >>  0) & 0xff;
  //reg[1] = (this->ht->tohost_addr >>  8) & 0xff;
  //reg[2] = (this->ht->tohost_addr >> 16) & 0xff;
  //reg[3] = (this->ht->tohost_addr >> 24) & 0xff;
  //reg[4] = (this->ht->tohost_addr >> 32) & 0xff;
  //reg[5] = (this->ht->tohost_addr >> 40) & 0xff;
  //reg[6] = (this->ht->tohost_addr >> 48) & 0xff;
  //reg[7] = (this->ht->tohost_addr >> 56) & 0xff;
  //this->ht->mem.write(0x8000000000 + 8 * 9, 8, (uint8_t*)reg);
  //// Zero Out Magic Mem
  //// TODO: Zero Out Magic Mem Other Entries
  //reg[0] = 0x00;
  //reg[1] = 0x00;
  //reg[2] = 0x00;
  //reg[3] = 0x00;
  //reg[4] = 0x00;
  //reg[5] = 0x00;
  //reg[6] = 0x00;
  //reg[7] = 0x00;
  //uint8_t rdata[8];
  //do {
  //  this->ht->mem.write(this->ht->fromhost_addr, 8, (uint8_t*)reg);
  //  this->ht->mem.read(this->ht->fromhost_addr, 8, (uint8_t*)rdata);
  //} while ((rdata[0] | rdata[1] | rdata[2] | rdata[3] | rdata[4] | rdata[5] | rdata[6] | rdata[7]) != 0);
  //do {
  //  this->ht->mem.write(this->ht->tohost_addr, 8, (uint8_t*)reg);
  //  this->ht->mem.read(this->ht->tohost_addr, 8, (uint8_t*)rdata);
  //} while ((rdata[0] | rdata[1] | rdata[2] | rdata[3] | rdata[4] | rdata[5] | rdata[6] | rdata[7]) != 0);
  //ltime = time(NULL);
  //fprintf(stderr, "Done Setting Magic Mem Address @ %s\n", asctime(localtime(&ltime)));
    ltime = time(NULL);
    fprintf(stderr, "Start Setting Magic Mem Address @ %s\n", asctime(localtime(&ltime)));
    // Zero out???
    this->ht->mem.read(0x844000d000, 8, (uint8_t*)reg);
    reg[0] = 0x00;
    reg[1] = 0x00;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00;
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(0x844000d000, 8, (uint8_t*)reg);
    this->ht->mem.read(0x844000d000, 8, (uint8_t*)reg);
    this->ht->mem.read(0x844000d008, 8, (uint8_t*)reg);
    reg[0] = 0x00;
    reg[1] = 0x00;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00;
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(0x844000d008, 8, (uint8_t*)reg);
    this->ht->mem.read(0x844000d008, 8, (uint8_t*)reg);
    // Start Address
    reg[0] = 0x00;
    reg[1] = 0x00;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00;
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(0x8000000000 + 8 * 6, 8, (uint8_t*)reg);
    // End Address
    reg[0] = 0x00;
    reg[1] = 0x00;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00;
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(0x8000000000 + 8 * 7, 8, (uint8_t*)reg);
    // From Host
    //reg[0] = 0x00;
    //reg[1] = 0x00;
    //reg[2] = 0x00;
    //reg[3] = 0x00;
    //reg[4] = 0x00;
    //reg[5] = 0x00;
    //reg[6] = 0x00;
    //reg[7] = 0x00;
    reg[0] = (this->ht->fromhost_addr >>  0) & 0xff;
    reg[1] = (this->ht->fromhost_addr >>  8) & 0xff;
    reg[2] = (this->ht->fromhost_addr >> 16) & 0xff;
    reg[3] = (this->ht->fromhost_addr >> 24) & 0xff;
    reg[4] = (this->ht->fromhost_addr >> 32) & 0xff;
    reg[5] = (this->ht->fromhost_addr >> 40) & 0xff;
    reg[6] = (this->ht->fromhost_addr >> 48) & 0xff;
    reg[7] = (this->ht->fromhost_addr >> 56) & 0xff;
    this->ht->mem.write(0x8000000000 + 8 * 8, 8, (uint8_t*)reg);
    // To Host
    //reg[0] = 0x00;
    //reg[1] = 0x00;
    //reg[2] = 0x00;
    //reg[3] = 0x00;
    //reg[4] = 0x00;
    //reg[5] = 0x00;
    //reg[6] = 0x00;
    //reg[7] = 0x00;
    reg[0] = (this->ht->tohost_addr >>  0) & 0xff;
    reg[1] = (this->ht->tohost_addr >>  8) & 0xff;
    reg[2] = (this->ht->tohost_addr >> 16) & 0xff;
    reg[3] = (this->ht->tohost_addr >> 24) & 0xff;
    reg[4] = (this->ht->tohost_addr >> 32) & 0xff;
    reg[5] = (this->ht->tohost_addr >> 40) & 0xff;
    reg[6] = (this->ht->tohost_addr >> 48) & 0xff;
    reg[7] = (this->ht->tohost_addr >> 56) & 0xff;
    this->ht->mem.write(0x8000000000 + 8 * 9, 8, (uint8_t*)reg);
    // Zero Out Magic Mem
    // TODO: Zero Out Magic Mem Other Entries
    reg[0] = 0x00;
    reg[1] = 0x00;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00;
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    uint8_t rdata[8];
    do {
      this->ht->mem.write(this->ht->fromhost_addr, 8, (uint8_t*)reg);
      this->ht->mem.read(this->ht->fromhost_addr, 8, (uint8_t*)rdata);
    } while ((rdata[0] | rdata[1] | rdata[2] | rdata[3] | rdata[4] | rdata[5] | rdata[6] | rdata[7]) != 0);
    do {
      this->ht->mem.write(this->ht->tohost_addr, 8, (uint8_t*)reg);
      this->ht->mem.read(this->ht->tohost_addr, 8, (uint8_t*)rdata);
    } while ((rdata[0] | rdata[1] | rdata[2] | rdata[3] | rdata[4] | rdata[5] | rdata[6] | rdata[7]) != 0);
    ltime = time(NULL);
    fprintf(stderr, "Done Setting Magic Mem Address @ %s\n", asctime(localtime(&ltime)));
}

//argrmap
void demo::set_reset_pc(std::string key){
  uint8_t reg[8];
  time_t ltime;
  // Setting Reset PC
  ltime = time(NULL);
  //fprintf(stderr, "Start Setting Reset PC @ %s\n", asctime(localtime(&ltime)));
  //size_t sz;
  //uint64_t rst_pc = std::stoul(this->ht->argmap["set_rst_pc="], &sz, 16);
  ////fprintf(stderr, "rst_pc = %lx\n", rst_pc);
  //reg[0] = (rst_pc >>  0) & 0xff;
  //reg[1] = (rst_pc >>  8) & 0xff;
  //reg[2] = (rst_pc >> 16) & 0xff;
  //reg[3] = (rst_pc >> 24) & 0xff;
  //reg[4] = (rst_pc >> 32) & 0xff;
  //reg[5] = (rst_pc >> 40) & 0xff;
  //reg[6] = (rst_pc >> 48) & 0xff;
  //reg[7] = (rst_pc >> 56) & 0xff;
  //this->ht->mem.write(0x8000000000 + 8 * 3, 8, (uint8_t*)reg);
  //ltime = time(NULL);
  //fprintf(stderr, "Done Setting Reset PC @ %s\n", asctime(localtime(&ltime)));
    ltime = time(NULL);
    fprintf(stderr, "Start Setting Reset PC @ %s\n", asctime(localtime(&ltime)));
    size_t sz;
    uint64_t rst_pc = std::stoul(this->ht->argmap["set_rst_pc="], &sz, 16);
    fprintf(stderr, "rst_pc = %lx\n", rst_pc);
    reg[0] = (rst_pc >>  0) & 0xff;
    reg[1] = (rst_pc >>  8) & 0xff;
    reg[2] = (rst_pc >> 16) & 0xff;
    reg[3] = (rst_pc >> 24) & 0xff;
    reg[4] = (rst_pc >> 32) & 0xff;
    reg[5] = (rst_pc >> 40) & 0xff;
    reg[6] = (rst_pc >> 48) & 0xff;
    reg[7] = (rst_pc >> 56) & 0xff;
    this->ht-> mem.write(0x8000000000 + 8 * 3, 8, (uint8_t*)reg);
    ltime = time(NULL);
    fprintf(stderr, "Done Setting Reset PC @ %s\n", asctime(localtime(&ltime)));
}
void demo::load_rom(std::string key){
  uint8_t reg[8];
  time_t ltime;
  //ltime = time(NULL);
  //fprintf(stderr, "Start Loading ROM @ %s\n", asctime(localtime(&ltime)));
  //
  //  uint32_t rom[256] = { 0x00000297,
  //                        0x02028593,
  //                        0xf1402573,
  //                        0x0182b283,
  //                        0x00028067,
  //                        0x00000000,
  //                        0x40000000,
  //                        0x00000000,
  //                        0xedfe0dd0,
  //                        0xe0030000,
  //                        0x38000000,
  //                        0x18030000,
  //                        0x28000000,
  //                        0x11000000,
  //                        0x10000000,
  //                        0x00000000,
  //                        0xc8000000,
  //                        0xe0020000,
  //                        0x00000000,
  //                        0x00000000,
  //                        0x00000000,
  //                        0x00000000,
  //                        0x01000000,
  //                        0x00000000,
  //                        0x03000000,
  //                        0x04000000,
  //                        0x00000000,
  //                        0x02000000,
  //                        0x03000000,
  //                        0x04000000,
  //                        0x0f000000,
  //                        0x02000000,
  //                        0x03000000,
  //                        0x16000000,
  //                        0x1b000000,
  //                        0x62626375,
  //                        0x732c7261,
  //                        0x656b6970,
  //                        0x7261622d,
  //                        0x65642d65,
  //                        0x00000076,
  //                        0x03000000,
  //                        0x12000000,
  //                        0x26000000,
  //                        0x62626375,
  //                        0x732c7261,
  //                        0x656b6970,
  //                        0x7261622d,
  //                        0x00000065,
  //                        0x01000000,
  //                        0x73757063,
  //                        0x00000000,
  //                        0x03000000,
  //                        0x04000000,
  //                        0x00000000,
  //                        0x01000000,
  //                        0x03000000,
  //                        0x04000000,
  //                        0x0f000000,
  //                        0x00000000,
  //                        0x03000000,
  //                        0x04000000,
  //                        0x2c000000,
  //                        0x80969800,
  //                        0x01000000,
  //                        0x40757063,
  //                        0x00000030,
  //                        0x03000000,
  //                        0x04000000,
  //                        0x3f000000,
  //                        0x00757063,
  //                        0x03000000,
  //                        0x04000000,
  //                        0x4b000000,
  //                        0x00000000,
  //                        0x03000000,
  //                        0x05000000,
  //                        0x4f000000,
  //                        0x79616b6f,
  //                        0x00000000,
  //                        0x03000000,
  //                        0x06000000,
  //                        0x1b000000,
  //                        0x63736972,
  //                        0x00000076,
  //                        0x03000000,
  //                        0x0b000000,
  //                        0x56000000,
  //                        0x34367672,
  //                        0x66616d69,
  //                        0x00006364,
  //                        0x03000000,
  //                        0x0b000000,
  //                        0x60000000,
  //                        0x63736972,
  //                        0x76732c76,
  //                        0x00003834,
  //                        0x03000000,
  //                        0x04000000,
  //                        0x69000000,
  //                        0x00ca9a3b,
  //                        0x01000000,
  //                        0x65746e69,
  //                        0x70757272,
  //                        0x6f632d74,
  //                        0x6f72746e,
  //                        0x72656c6c,
  //                        0x00000000,
  //                        0x03000000,
  //                        0x04000000,
  //                        0x79000000,
  //                        0x01000000,
  //                        0x03000000,
  //                        0x00000000,
  //                        0x8a000000,
  //                        0x03000000,
  //                        0x0f000000,
  //                        0x1b000000,
  //                        0x63736972,
  //                        0x70632c76,
  //                        0x6e692d75,
  //                        0x00006374,
  //                        0x03000000,
  //                        0x04000000,
  //                        0x9f000000,
  //                        0x01000000,
  //                        0x03000000,
  //                        0x04000000,
  //                        0xa5000000,
  //                        0x01000000,
  //                        0x02000000,
  //                        0x02000000,
  //                        0x02000000,
  //                        0x01000000,
  //                        0x6f6d656d,
  //                        0x34407972,
  //                        0x30303030,
  //                        0x00303030,
  //                        0x03000000,
  //                        0x07000000,
  //                        0x3f000000,
  //                        0x6f6d656d,
  //                        0x00007972,
  //                        0x03000000,
  //                        0x10000000,
  //                        0x4b000000,
  //                        0x00000000,
  //                        0x00000040,
  //                        0x00000000,
  //                        0x00000080,
  //                        0x02000000,
  //                        0x01000000,
  //                        0x00636f73,
  //                        0x03000000,
  //                        0x04000000,
  //                        0x00000000,
  //                        0x02000000,
  //                        0x03000000,
  //                        0x04000000,
  //                        0x0f000000,
  //                        0x02000000,
  //                        0x03000000,
  //                        0x21000000,
  //                        0x1b000000,
  //                        0x62626375,
  //                        0x732c7261,
  //                        0x656b6970,
  //                        0x7261622d,
  //                        0x6f732d65,
  //                        0x69730063,
  //                        0x656c706d,
  //                        0x7375622d,
  //                        0x00000000,
  //                        0x03000000,
  //                        0x00000000,
  //                        0xad000000,
  //                        0x01000000,
  //                        0x6e696c63,
  //                        0x30324074,
  //                        0x30303030,
  //                        0x00000030,
  //                        0x03000000,
  //                        0x0d000000,
  //                        0x1b000000,
  //                        0x63736972,
  //                        0x6c632c76,
  //                        0x30746e69,
  //                        0x00000000,
  //                        0x03000000,
  //                        0x10000000,
  //                        0xb4000000,
  //                        0x01000000,
  //                        0x03000000,
  //                        0x01000000,
  //                        0x07000000,
  //                        0x03000000,
  //                        0x10000000,
  //                        0x4b000000,
  //                        0x00000000,
  //                        0x00000002,
  //                        0x00000000,
  //                        0x00000c00,
  //                        0x02000000,
  //                        0x02000000,
  //                        0x02000000,
  //                        0x09000000,
  //                        0x64646123,
  //                        0x73736572,
  //                        0x6c65632d,
  //                        0x2300736c,
  //                        0x657a6973,
  //                        0x6c65632d,
  //                        0x6300736c,
  //                        0x61706d6f,
  //                        0x6c626974,
  //                        0x6f6d0065,
  //                        0x006c6564,
  //                        0x656d6974,
  //                        0x65736162,
  //                        0x6572662d,
  //                        0x6e657571,
  //                        0x64007963,
  //                        0x63697665,
  //                        0x79745f65,
  //                        0x72006570,
  //                        0x73006765,
  //                        0x75746174,
  //                        0x69720073,
  //                        0x2c766373,
  //                        0x00617369,
  //                        0x2d756d6d,
  //                        0x65707974,
  //                        0x6f6c6300,
  //                        0x662d6b63,
  //                        0x75716572,
  //                        0x79636e65,
  //                        0x6e692300,
  //                        0x72726574,
  //                        0x2d747075,
  //                        0x6c6c6563,
  //                        0x6e690073,
  //                        0x72726574,
  //                        0x2d747075,
  //                        0x746e6f63,
  //                        0x6c6c6f72,
  //                        0x6c007265,
  //                        0x78756e69,
  //                        0x6168702c,
  //                        0x656c646e,
  //                        0x6e617200,
  //                        0x00736567,
  //                        0x65746e69,
  //                        0x70757272,
  //                        0x652d7374,
  //                        0x6e657478,
  //                        0x00646564 };

  ////uint64_t addr = 0x40011c00;
  //uint64_t addr = 0x40022c00;
  //for (int i = 0; i < 128; i++) {
  //  reg[7] = (rom[i * 2 + 1] >> 24) & 0xff;
  //  reg[6] = (rom[i * 2 + 1] >> 16) & 0xff;
  //  reg[5] = (rom[i * 2 + 1] >>  8) & 0xff;
  //  reg[4] = (rom[i * 2 + 1] >>  0) & 0xff;
  //  reg[3] = (rom[i * 2 + 0] >> 24) & 0xff;
  //  reg[2] = (rom[i * 2 + 0] >> 16) & 0xff;
  //  reg[1] = (rom[i * 2 + 0] >>  8) & 0xff;
  //  reg[0] = (rom[i * 2 + 0] >>  0) & 0xff;
  //  this->ht->mem.write(addr, 8, (uint8_t*)reg, DDR);
  //  addr += 8;
  //}
  //ltime = time(NULL);
  //fprintf(stderr, "Done Loading ROM @ %s\n", asctime(localtime(&ltime)));
    ltime = time(NULL);
    fprintf(stderr, "Start Loading ROM @ %s\n", asctime(localtime(&ltime)));
    uint32_t rom[256] = { 0x00000297,
                          0x02028593,
                          0xf1402573,
                          0x0182b283,
                          0x00028067,
                          0x00000000,
                          0x40000000,
                          0x00000000,
                          0xedfe0dd0,
                          0xe0030000,
                          0x38000000,
                          0x18030000,
                          0x28000000,
                          0x11000000,
                          0x10000000,
                          0x00000000,
                          0xc8000000,
                          0xe0020000,
                          0x00000000,
                          0x00000000,
                          0x00000000,
                          0x00000000,
                          0x01000000,
                          0x00000000,
                          0x03000000,
                          0x04000000,
                          0x00000000,
                          0x02000000,
                          0x03000000,
                          0x04000000,
                          0x0f000000,
                          0x02000000,
                          0x03000000,
                          0x16000000,
                          0x1b000000,
                          0x62626375,
                          0x732c7261,
                          0x656b6970,
                          0x7261622d,
                          0x65642d65,
                          0x00000076,
                          0x03000000,
                          0x12000000,
                          0x26000000,
                          0x62626375,
                          0x732c7261,
                          0x656b6970,
                          0x7261622d,
                          0x00000065,
                          0x01000000,
                          0x73757063,
                          0x00000000,
                          0x03000000,
                          0x04000000,
                          0x00000000,
                          0x01000000,
                          0x03000000,
                          0x04000000,
                          0x0f000000,
                          0x00000000,
                          0x03000000,
                          0x04000000,
                          0x2c000000,
                          0x80969800,
                          0x01000000,
                          0x40757063,
                          0x00000030,
                          0x03000000,
                          0x04000000,
                          0x3f000000,
                          0x00757063,
                          0x03000000,
                          0x04000000,
                          0x4b000000,
                          0x00000000,
                          0x03000000,
                          0x05000000,
                          0x4f000000,
                          0x79616b6f,
                          0x00000000,
                          0x03000000,
                          0x06000000,
                          0x1b000000,
                          0x63736972,
                          0x00000076,
                          0x03000000,
                          0x0b000000,
                          0x56000000,
                          0x34367672,
                          0x66616d69,
                          0x00006364,
                          0x03000000,
                          0x0b000000,
                          0x60000000,
                          0x63736972,
                          0x76732c76,
                          0x00003834,
                          0x03000000,
                          0x04000000,
                          0x69000000,
                          0x00ca9a3b,
                          0x01000000,
                          0x65746e69,
                          0x70757272,
                          0x6f632d74,
                          0x6f72746e,
                          0x72656c6c,
                          0x00000000,
                          0x03000000,
                          0x04000000,
                          0x79000000,
                          0x01000000,
                          0x03000000,
                          0x00000000,
                          0x8a000000,
                          0x03000000,
                          0x0f000000,
                          0x1b000000,
                          0x63736972,
                          0x70632c76,
                          0x6e692d75,
                          0x00006374,
                          0x03000000,
                          0x04000000,
                          0x9f000000,
                          0x01000000,
                          0x03000000,
                          0x04000000,
                          0xa5000000,
                          0x01000000,
                          0x02000000,
                          0x02000000,
                          0x02000000,
                          0x01000000,
                          0x6f6d656d,
                          0x34407972,
                          0x30303030,
                          0x00303030,
                          0x03000000,
                          0x07000000,
                          0x3f000000,
                          0x6f6d656d,
                          0x00007972,
                          0x03000000,
                          0x10000000,
                          0x4b000000,
                          0x00000000,
                          0x00000040,
                          0x00000000,
                          0x00000080,
                          0x02000000,
                          0x01000000,
                          0x00636f73,
                          0x03000000,
                          0x04000000,
                          0x00000000,
                          0x02000000,
                          0x03000000,
                          0x04000000,
                          0x0f000000,
                          0x02000000,
                          0x03000000,
                          0x21000000,
                          0x1b000000,
                          0x62626375,
                          0x732c7261,
                          0x656b6970,
                          0x7261622d,
                          0x6f732d65,
                          0x69730063,
                          0x656c706d,
                          0x7375622d,
                          0x00000000,
                          0x03000000,
                          0x00000000,
                          0xad000000,
                          0x01000000,
                          0x6e696c63,
                          0x30324074,
                          0x30303030,
                          0x00000030,
                          0x03000000,
                          0x0d000000,
                          0x1b000000,
                          0x63736972,
                          0x6c632c76,
                          0x30746e69,
                          0x00000000,
                          0x03000000,
                          0x10000000,
                          0xb4000000,
                          0x01000000,
                          0x03000000,
                          0x01000000,
                          0x07000000,
                          0x03000000,
                          0x10000000,
                          0x4b000000,
                          0x00000000,
                          0x00000002,
                          0x00000000,
                          0x00000c00,
                          0x02000000,
                          0x02000000,
                          0x02000000,
                          0x09000000,
                          0x64646123,
                          0x73736572,
                          0x6c65632d,
                          0x2300736c,
                          0x657a6973,
                          0x6c65632d,
                          0x6300736c,
                          0x61706d6f,
                          0x6c626974,
                          0x6f6d0065,
                          0x006c6564,
                          0x656d6974,
                          0x65736162,
                          0x6572662d,
                          0x6e657571,
                          0x64007963,
                          0x63697665,
                          0x79745f65,
                          0x72006570,
                          0x73006765,
                          0x75746174,
                          0x69720073,
                          0x2c766373,
                          0x00617369,
                          0x2d756d6d,
                          0x65707974,
                          0x6f6c6300,
                          0x662d6b63,
                          0x75716572,
                          0x79636e65,
                          0x6e692300,
                          0x72726574,
                          0x2d747075,
                          0x6c6c6563,
                          0x6e690073,
                          0x72726574,
                          0x2d747075,
                          0x746e6f63,
                          0x6c6c6f72,
                          0x6c007265,
                          0x78756e69,
                          0x6168702c,
                          0x656c646e,
                          0x6e617200,
                          0x00736567,
                          0x65746e69,
                          0x70757272,
                          0x652d7374,
                          0x6e657478,
                          0x00646564 };
    //uint64_t addr = 0x40011c00;
    uint64_t addr = 0x40022c00;
    for (int i = 0; i < 128; i++) {
      reg[7] = (rom[i * 2 + 1] >> 24) & 0xff;
      reg[6] = (rom[i * 2 + 1] >> 16) & 0xff;
      reg[5] = (rom[i * 2 + 1] >>  8) & 0xff;
      reg[4] = (rom[i * 2 + 1] >>  0) & 0xff;
      reg[3] = (rom[i * 2 + 0] >> 24) & 0xff;
      reg[2] = (rom[i * 2 + 0] >> 16) & 0xff;
      reg[1] = (rom[i * 2 + 0] >>  8) & 0xff;
      reg[0] = (rom[i * 2 + 0] >>  0) & 0xff;
      this->ht->mem.write(addr, 8, (uint8_t*)reg, DDR);
      //this->ht->mem.write(addr, 8, (uint8_t*)reg);
      addr += 8;
    }
    ltime = time(NULL);
    fprintf(stderr, "Done Loading ROM @ %s\n", asctime(localtime(&ltime)));
}

// argmap
void demo::dump_l2_after_loading(std::string key){
  uint8_t reg[8];
  time_t ltime;
  // Dump L2 After Loading
  FILE * fd = fopen(this->ht->argmap["dump_l2_after_loading="].c_str(), "w+");
  ltime = time(NULL);
  fprintf(stderr, "Start Dumping L2 After Loading @ %s\n", asctime(localtime(&ltime)));
  // DATA
  long int ID = 0x2b;
  long int addr = 0;
  long int offset = 0;
  for (int way = 0; way < 8; way ++) {
    for (int idx = 0; idx < 256; idx ++) {
      for (int bank = 0; bank < 8; bank ++) {
        for (int chunk = 0; chunk < 4; chunk ++) {
          reg[0] = 0x00;
          reg[1] = 0x00;
          reg[2] = 0x00;
          reg[3] = 0x00;
          reg[4] = 0x00;
          reg[5] = 0x00;
          reg[6] = 0x00;
          reg[7] = 0x00;
          addr = ID;
          addr = addr << 18; // RSVD
          addr = addr << 3; // BANK_ID
          addr = addr + bank;
          addr = addr << 3; // WAY_ID
          addr = addr + way;
          addr = addr << 2; // CHUNK_ID
          addr = addr + chunk;
          addr = addr << 8; // BANK_INDEX
          this->ht->mem.read(addr, 8, (uint8_t*)reg);
          offset = way; // WAY_ID
          offset = offset << 8; // BANK_INDEX
          offset = offset + idx;
          offset = offset << 3; // BANK_ID
          offset = offset + bank;
          offset = offset << 2; // CHUNK_ID
          offset = offset + chunk;
          offset = offset << 3; // Align to 8 Byte
          fprintf(fd, "AddrOffset: %020x | %02x%02x%02x%02x%02x%02x%02x%02x\n", offset, reg[7], reg[6], reg[5], reg[4], reg[3], reg[2], reg[1], reg[0]);
        }
      }
    }
  }
  fclose(fd);
  ltime = time(NULL);
  fprintf(stderr, "Done Dumping L2 After Loading @ %s\n", asctime(localtime(&ltime)));
}
void demo::release_vcore_reset(std::string key){
  uint8_t reg[8];
  time_t ltime;
  // Releasing VCORE RST
  //for (int i = 0; i < 16; i++) {
  //  ltime = time(NULL);
  //  fprintf(stderr, "Start Releasing VCORE %0d Reset @ %s\n", i, asctime(localtime(&ltime)));
  //  reg[0] = 0xf8;
  //  reg[1] = 0x00;
  //  reg[2] = 0x00;
  //  reg[3] = 0x00;
  //  reg[4] = 0x00;
  //  reg[5] = 0x00;
  //  reg[6] = 0x00;
  //  reg[7] = 0x00;
  //  this->ht->mem.write(0x8000000000 + 8 * (10 + i), 8, (uint8_t*)reg);
  //  ltime = time(NULL);
  //  fprintf(stderr, "Done Releasing VCORE %0d Reset @ %s\n", i, asctime(localtime(&ltime)));
  //}
    for (int i = 0; i < 16; i++) {
      ltime = time(NULL);
      fprintf(stderr, "Start Releasing VCORE %0d Reset @ %s\n", i, asctime(localtime(&ltime)));
      reg[0] = 0xf8;
      reg[1] = 0x00;
      reg[2] = 0x00;
      reg[3] = 0x00;
      reg[4] = 0x00;
      reg[5] = 0x00;
      reg[6] = 0x00;
      reg[7] = 0x00;
      this->ht->mem.write(0x8000000000 + 8 * (10 + i), 8, (uint8_t*)reg);
      ltime = time(NULL);
      fprintf(stderr, "Done Releasing VCORE %0d Reset @ %s\n", i, asctime(localtime(&ltime)));
    }
}

//argmap
void demo::mem_test(std::string key){
  uint8_t reg[8];
  time_t ltime;
  // Memory Test
  ltime = time(NULL);
  fprintf(stderr, "Start Mem Test @ %s\n", asctime(localtime(&ltime)));

  std::ifstream f (this->ht->argmap["this->ht->mem_test="].c_str());
  if (!f) {
    fprintf(stderr, "%s is not presented!\n", this->ht->argmap["this->ht->mem_test="].c_str());
  }
  else {
    std::string str[3];
    while (f) {
      f >> str[0];
      f >> str[1];
      f >> str[2];
      if (str[0].compare("WR") == 0) {
        uint64_t data = std::stoul(str[2], nullptr, 0);
        for (int i = 0; i < 8; i++)
          reg[i] = (data >> (i * 8)) & 0xff;
        this->ht->mem.write(std::stoul(str[1], nullptr, 0), 8, (uint8_t*)reg);
        fprintf(stderr, "Done writing to address %s with data %s\n", str[1].c_str(), str[2].c_str());
      } else {
        this->ht->mem.read(std::stoul(str[1], nullptr, 0), 8, (uint8_t*)reg);
        uint64_t data = 0;
        for (int i = 7; i >= 0; i--)
          data = (data << 8) + reg[i];
        if (str[2].compare("X") != 0) {
          if (data == std::stoul(str[2], nullptr, 0)) {
            fprintf(stderr, "Done reading to address %s, got data 0x%lx, match expectation\n", str[1].c_str(), data);
          } else {
            fprintf(stderr, "Done reading to address %s, got data 0x%lx, expecting %s\n", str[1].c_str(), data, str[2].c_str());
          }
        } else {
          fprintf(stderr, "Done reading to address %s, got data 0x%lx, do not check\n", str[1].c_str(), data);
        }
      }
    }
  }
  ltime = time(NULL);
  fprintf(stderr, "Done Mem Test @ %s\n", asctime(localtime(&ltime)));
}

void demo::enable_dram_tester(std::string key){
  uint8_t reg[8];
  time_t ltime;
  ltime = time(NULL);
  fprintf(stderr, "Start Enabling DRAM Tester @ %s\n", asctime(localtime(&ltime)));
  int target, idx, ram_addr, mode, loop;
  uint64_t poly, lfsr; // MSB - 5
  poly = random() % 0x7ffffff;
  lfsr = random() % 0x7ffffff;

  // LFSR
  for (int j = 0; j < 4; j++) {
    for (int k = 0; k < 3; k++) {
      idx = j;
      target = k;
      ram_addr = 0;
      if (k == 0) { // POLY
        reg[0] = (poly >> (8 * 0)) & 0xff;
        reg[1] = (poly >> (8 * 1)) & 0xff;
        reg[2] = (poly >> (8 * 2)) & 0xff;
        reg[3] = (poly >> (8 * 3)) & 0xff;
        reg[4] = (poly >> (8 * 4)) & 0xff;
        reg[5] = (poly >> (8 * 5)) & 0xff;
        reg[6] = (poly >> (8 * 6)) & 0xff;
        reg[7] = (poly >> (8 * 7)) & 0xff;
      } else if (k == 1) { // MSB = 32 - 5
        reg[0] = 0x1b;
        reg[1] = 0x00;
        reg[2] = 0x00;
        reg[3] = 0x00;
        reg[4] = 0x00;
        reg[5] = 0x00;
        reg[6] = 0x00;
        reg[7] = 0x00;
      } else if (k == 2) {
        reg[0] = (lfsr >> (8 * 0)) & 0xff;
        reg[1] = (lfsr >> (8 * 1)) & 0xff;
        reg[2] = (lfsr >> (8 * 2)) & 0xff;
        reg[3] = (lfsr >> (8 * 3)) & 0xff;
        reg[4] = (lfsr >> (8 * 4)) & 0xff;
        reg[5] = (lfsr >> (8 * 5)) & 0xff;
        reg[6] = (lfsr >> (8 * 6)) & 0xff;
        reg[7] = (lfsr >> (8 * 7)) & 0xff;
      }
      this->ht->mem.write(0x9400000000 + (idx << 11) + (ram_addr << 3) + target, 8, (uint8_t*)reg);
    }
  }
  // Program Entry
  for (int j = 0; j < 256; j++) {
    target = 4;
    ram_addr = j;
    if (j < 128) {
      reg[0] = j * 2;
      reg[1] = 0x00;
      reg[2] = 0x00;
      reg[3] = 0x00;
      reg[4] = 0x00;
      reg[5] = 0x00;
      reg[6] = 0x00;
      reg[7] = 0x00;
    } else {
      reg[0] = (j - 128) * 2 + 1;
      reg[1] = 0x00;
      reg[2] = 0x00;
      reg[3] = 0x00;
      reg[4] = 0x00;
      reg[5] = 0x00;
      reg[6] = 0x00;
      reg[7] = 0x00;
    }
    this->ht->mem.write(0x9400000000 + (ram_addr << 3) + target, 8, (uint8_t*)reg);
  }

  // Start
  target = 3;
  ram_addr = 0;
  mode = random() % 2; // 0: Random, 1: Program
  loop = 0;
  reg[0] = 0x40; // {mode, loop, 1'b0, 1'b1, 1'b0, 1'b0, 4'b0}
  reg[1] = mode * 2 + loop;
  reg[2] = 0x00;
  reg[3] = 0x00;
  reg[4] = 0x00;
  reg[5] = 0x00;
  reg[6] = 0x00;
  reg[7] = 0x00;
  this->ht->mem.write(0x9400000000 + (ram_addr << 3) + target, 8, (uint8_t*)reg);

  ltime = time(NULL);
  fprintf(stderr, "Done Enabling DRAM Tester @ %s\n", asctime(localtime(&ltime)));
  // Stop
  ltime = time(NULL);
  fprintf(stderr, "Start Disabling DRAM Tester @ %s\n", asctime(localtime(&ltime)));
  target = 3;
  ram_addr = 0;
  mode = 0;
  loop = 0;
  reg[0] = 0x00; // {mode, loop, 1'b0, 1'b0, 1'b0, 1'b0, 4'b0}
  reg[1] = 0x04; // Do Not Clear Error
  reg[2] = 0x00;
  reg[3] = 0x00;
  reg[4] = 0x00;
  reg[5] = 0x00;
  reg[6] = 0x00;
  reg[7] = 0x00;
  this->ht->mem.write(0x9400000000 + (ram_addr << 3) + target, 8, (uint8_t*)reg);

  do {
    this->ht->mem.read(0x9400000000 + (ram_addr << 3) + target, 8, (uint8_t*)reg);
  } while (((reg[0] >> 5) & 0x1) == 0x1);

  ltime = time(NULL);
  fprintf(stderr, "Done Disabling DRAM Tester @ %s\n", asctime(localtime(&ltime)));

  // Check Error
  if (((reg[0] >> 4) & 0x1) == 0x1) {
    fprintf(stderr, "Error: DT finds error when running random test, Please review the waveform\n");
  }
}

// symbols
void demo::release_orv_reset(std::string key){
  uint8_t reg[8];
  time_t ltime;
  //  ltime = time(NULL);
  //  fprintf(stderr, "Start Releasing ORV Reset @ %s\n", asctime(localtime(&ltime)));
////    reg[0] = 0x01;
////    reg[1] = 0x00;
////    reg[2] = 0x00;
////    reg[3] = 0x00;
////    reg[4] = 0x00;
////    reg[5] = 0x00;
////    reg[6] = 0x00;
////    reg[7] = 0x00;
////    this->ht->mem.write(0x8000000000 + 8 * 4, 8, (uint8_t*)reg);
  //  reg[0] = 0x08;
  //  reg[1] = 0x00;
  //  reg[2] = 0x00;
  //  reg[3] = 0x00;
  //  reg[4] = 0x00;
  //  reg[5] = 0x00;
  //  reg[6] = 0x00;
  //  reg[7] = 0x00;
  //  this->ht->mem.write(0x8000000000 + 8 * 2, 8, (uint8_t*)reg);
  //  ltime = time(NULL);
  //  fprintf(stderr, "Done Releasing ORV Reset @ %s\n", asctime(localtime(&ltime)));
    ltime = time(NULL);
    fprintf(stderr, "Start Releasing ORV Reset @ %s\n", asctime(localtime(&ltime)));
//    reg[0] = 0x01;
//    reg[1] = 0x00;
//    reg[2] = 0x00;
//    reg[3] = 0x00;
//    reg[4] = 0x00;
//    reg[5] = 0x00;
//    reg[6] = 0x00;
//    reg[7] = 0x00;
//    mem.write(0x8000000000 + 8 * 4, 8, (uint8_t*)reg);
    reg[0] = 0x08;
    reg[1] = 0x00;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00;
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(0x8000000000 + 8 * 2, 8, (uint8_t*)reg);
    ltime = time(NULL);
    fprintf(stderr, "Done Releasing ORV Reset @ %s\n", asctime(localtime(&ltime)));
}
void demo::trigger_afterwards(std::string key){
  uint8_t reg[8];
  time_t ltime;
  // Trigger after everything is done
    ltime = time(NULL);
    fprintf(stderr, "Start Triggering Afterwards @ %s\n", asctime(localtime(&ltime)));
    reg[0] = 0x00;
    reg[1] = 0x00;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00;
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(0xffffffff00, 8, (uint8_t*)reg);
    ltime = time(NULL);
    fprintf(stderr, "Done Triggering Afterwards @ %s\n", asctime(localtime(&ltime)));
}

void demo::set_numvcore(std::string key){
  uint8_t reg[8];
  time_t ltime;

  size_t sz;
  uint64_t numVcore = std::stoul(key, &sz, 10);
  for (std::map<std::string, uint64_t>::iterator it = this->ht->program_symbols.begin(); it != this->ht->program_symbols.end(); ++it) {
    if (it->first.substr(0, 8) == "numVcore") {
      if (it->first.length() == 8) {
        reg[0] = numVcore;
        reg[1] = 0x00;
        reg[2] = 0x00;
        reg[3] = 0x00;
        reg[4] = 0x00;
        reg[5] = 0x00;
        reg[6] = 0x00;
        reg[7] = 0x00;
        this->ht->mem.write(it->second, 8, (uint8_t*)reg);
        fprintf(stderr, "Writing numVcore value (%d) to address 0x%x\n", numVcore, it->second);
      } else if (it->first.length() > 8) {
        if (it->first.substr(8, 1) == ".") {
          reg[0] = numVcore;
          reg[1] = 0x00;
          reg[2] = 0x00;
          reg[3] = 0x00;
          reg[4] = 0x00;
          reg[5] = 0x00;
          reg[6] = 0x00;
          reg[7] = 0x00;
          this->ht->mem.write(it->second, 8, (uint8_t*)reg);
          fprintf(stderr, "Writing numVcore value (%d) to address 0x%x\n", numVcore, it->second);
        }
      }
    }
  }
}

void demo::set_loopcount(std::string key) {
  uint8_t reg[8];
  time_t ltime;
  size_t sz;
  uint64_t loopCount = std::stoul(key, &sz, 10);
  for (std::map<std::string, uint64_t>::iterator it = this->ht->program_symbols.begin(); it != this->ht->program_symbols.end(); ++it) {
    if (it->first.substr(0, 9) == "loopCount") {
      if (it->first.length() == 9) {
        reg[0] = loopCount;
        reg[1] = 0x00;
        reg[2] = 0x00;
        reg[3] = 0x00;
        reg[4] = 0x00;
        reg[5] = 0x00;
        reg[6] = 0x00;
        reg[7] = 0x00;
        this->ht->mem.write(it->second, 8, (uint8_t*)reg);
        fprintf(stderr, "Writing loopCount value (%d) to address 0x%x\n", loopCount, it->second);
      } else if (it->first.length() > 9) {
        if (it->first.substr(9, 1) == ".") {
          reg[0] = loopCount;
          reg[1] = 0x00;
          reg[2] = 0x00;
          reg[3] = 0x00;
          reg[4] = 0x00;
          reg[5] = 0x00;
          reg[6] = 0x00;
          reg[7] = 0x00;
          this->ht->mem.write(it->second, 8, (uint8_t*)reg);
          fprintf(stderr, "Writing loopCount value (%d) to address 0x%x\n", loopCount, it->second);
        }
      }
    }
  }
}

