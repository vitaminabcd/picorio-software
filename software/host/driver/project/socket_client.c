#ifndef _SOCKET_CLIENT_C
#define _SOCKET_CLIENT_C
#include "socket_client.h"

//! Only for Stand ALone Operations
///*
  int main(int argc, char *argv[]) {
  //! Declaration
  int clientSock; //Client Socket Descriptor
  uint8_t *dataToBeSent;
  int32_t ack;
  struct sockaddr_in serverAddr; // Local Address
  unsigned short serverPort; 
  if(argc != 2) { //Passed Argument chk
    fprintf(stderr,"Usage: %s <Server Port>\n",argv[0]);
    exit(EXIT_FAILURE);
  }
  serverPort = atoi(argv[1]); //First Argument is Local Port
  createClientSock(&clientSock);
  //! Construct Local Address Struct
  //! Get Server Address
  configureServerAddr(&serverAddr,&serverPort);
  connectClientSock(&clientSock,&serverAddr);
  dataToBeSent = (uint8_t *) malloc (sizeof(uint8_t) * 100);
  for(int i=0;i<1;i++){
    uint64_t addr;
    uint8_t  p[sizeof(addr)];
    addr =  RST_ADDR_OFFSET;
    fprintf(stdout,"Client: Do WR to RST\n");
    memcpy(p,&addr,sizeof(addr));
    dataToBeSent[0]   = CMD_RESET;
    dataToBeSent[1]   = p[4];
    dataToBeSent[2]   = p[3];
    dataToBeSent[3]   = p[2];
    dataToBeSent[4]   = p[1];
    dataToBeSent[5]   = p[0];
    dataToBeSent[6]=0x00;
    dataToBeSent[7]=0x00;
    dataToBeSent[8]=0x00;
    dataToBeSent[9]=0x00;
    dataToBeSent[10]=0x00;
    dataToBeSent[11]=0x00;
    dataToBeSent[12]=0x00;
    dataToBeSent[13]=0x00;
    dataToBeSent[14]=0x00;
    dataToBeSent[15]=0x00;
    sendWaitForAck(&serverAddr,&clientSock,dataToBeSent,&ack);
    addr =  RST_ADDR_OFFSET;
    fprintf(stdout,"Client: Do WR to RST\n");
    memcpy(p,&addr,sizeof(addr));
    dataToBeSent[0]   = CMD_RESET;
    dataToBeSent[1]   = p[4];
    dataToBeSent[2]   = p[3];
    dataToBeSent[3]   = p[2];
    dataToBeSent[4]   = p[1];
    dataToBeSent[5]   = p[0];
    dataToBeSent[6]=0x01;
    dataToBeSent[7]=0x00;
    dataToBeSent[8]=0x00;
    dataToBeSent[9]=0x00;
    dataToBeSent[10]=0x00;
    dataToBeSent[11]=0x00;
    dataToBeSent[12]=0x00;
    dataToBeSent[13]=0x00;
    dataToBeSent[14]=0x00;
    dataToBeSent[15]=0x00;
    sendWaitForAck(&serverAddr,&clientSock,dataToBeSent,&ack);
  }
  for(int i=0;i<1;i++){
    uint64_t addr;
    uint8_t  p[sizeof(addr)];
    addr = addr + i*8;
    fprintf(stdout,"Client: Do WR to SPI\n");
    memcpy(p,&addr,sizeof(addr));
    dataToBeSent[0]   = CMD_SPI_WRITE;
    dataToBeSent[1]   = p[4];
    dataToBeSent[2]   = p[3];
    dataToBeSent[3]   = p[2];
    dataToBeSent[4]   = p[1];
    dataToBeSent[5]   = p[0];
    dataToBeSent[6]   = 0x55;
    dataToBeSent[7]   = 0x55;
    dataToBeSent[8]   = 0x55;
    dataToBeSent[9]   = 0x55;
    dataToBeSent[10]  = 0x55;
    dataToBeSent[11]  = 0x55;
    dataToBeSent[12]  = 0x55;
    dataToBeSent[13]  = 0x55;
    dataToBeSent[14]  = 0x55;
    dataToBeSent[15]  = 0x55;
    sendWaitForAck(&serverAddr,&clientSock,dataToBeSent,&ack);
    fprintf(stdout,"Client: Do RD From SPI\n");
    dataToBeSent[0]   = CMD_SPI_READ;
    dataToBeSent[1]   = p[4];
    dataToBeSent[2]   = p[3];
    dataToBeSent[3]   = p[2];
    dataToBeSent[4]   = p[1];
    dataToBeSent[5]   = p[0];
    dataToBeSent[6]   = 0x00;
    dataToBeSent[7]   = 0x00;
    dataToBeSent[8]   = 0x00;
    dataToBeSent[9]   = 0x00;
    dataToBeSent[10]  = 0x00;
    dataToBeSent[11]  = 0x00;
    dataToBeSent[12]  = 0x00;
    dataToBeSent[13]  = 0x00;
    dataToBeSent[14]  = 0x00;
    dataToBeSent[15]  = 0x00;
    sendWaitForAck(&serverAddr,&clientSock,dataToBeSent,&ack);
  }
  for(int i=0; i<0; i++){
    uint64_t addr;
    uint8_t  p[sizeof(addr)];
    addr   = BRAM_ADDR_OFFSET;
    addr = addr + i*4096;
    fprintf(stdout,"Client: Do WR to BRAM addr: 0x%" PRIx64 "\n",addr);
    memcpy(p,&addr,sizeof(addr));
    dataToBeSent[1]   = p[4];
    dataToBeSent[2]   = p[3];
    dataToBeSent[3]   = p[2];
    dataToBeSent[4]   = p[1];
    dataToBeSent[5]   = p[0];
    dataToBeSent[0]   = CMD_BRAM_WRITE;
    dataToBeSent[6]   = 0x12;
    dataToBeSent[7]   = 0x34;
    dataToBeSent[8]   = 0x56;
    dataToBeSent[9]   = 0x78;
    dataToBeSent[10]  = 0xde;
    dataToBeSent[11]  = 0xad;
    dataToBeSent[12]  = 0xbe;
    dataToBeSent[13]  = 0xef;
    dataToBeSent[14]  = p[4];
    dataToBeSent[15]  = p[3];
    sendWaitForAck(&serverAddr,&clientSock,dataToBeSent,&ack);
    fprintf(stdout,"Client: Do RD to BRAM addr: 0x%" PRIx64 "\n",addr);
    dataToBeSent[1]   = p[4];
    dataToBeSent[2]   = p[3];
    dataToBeSent[3]   = p[2];
    dataToBeSent[4]   = p[1];
    dataToBeSent[5]   = p[0];
    dataToBeSent[0]   = CMD_BRAM_READ;
    dataToBeSent[6]   = 0x00;
    dataToBeSent[7]   = 0x00;
    dataToBeSent[8]   = 0x00;
    dataToBeSent[9]   = 0x00;
    dataToBeSent[10]  = 0x00;
    dataToBeSent[11]  = 0x00;
    dataToBeSent[12]  = 0x00;
    dataToBeSent[13]  = 0x00;
    dataToBeSent[14]  = 0x00;
    dataToBeSent[15]  = 0x00;
    sendWaitForAck(&serverAddr,&clientSock,dataToBeSent,&ack);
  }
  for(int i=0; i<20; i++){ 
    uint64_t addr;
    uint8_t  p[sizeof(addr)];
    addr = DDR_ADDR_OFFSET;
    addr = addr + i*4096;
    fprintf(stdout,"Client: Do WR to DDR addr: 0x%" PRIx64 "\n",addr);
    memcpy(p,&addr,sizeof(addr));
    dataToBeSent[1]   = p[4];
    dataToBeSent[2]   = p[3];
    dataToBeSent[3]   = p[2];
    dataToBeSent[4]   = p[1];
    dataToBeSent[5]   = p[0];
    dataToBeSent[0]   = CMD_DDR_WRITE;
    dataToBeSent[6]   = 0x12;
    dataToBeSent[7]   = 0x34;
    dataToBeSent[8]   = 0x56;
    dataToBeSent[9]   = 0x78;
    dataToBeSent[10]  = 0xde;
    dataToBeSent[11]  = p[4];
    dataToBeSent[12]  = p[3];
    dataToBeSent[13]  = p[2];
    dataToBeSent[14]  = p[1];
    dataToBeSent[15]  = p[0];
    sendWaitForAck(&serverAddr,&clientSock,dataToBeSent,&ack);
    fprintf(stdout,"Client: Do RD to DDR addr: 0x%" PRIx64 "\n",addr);
    dataToBeSent[1]   = p[4];
    dataToBeSent[2]   = p[3];
    dataToBeSent[3]   = p[2];
    dataToBeSent[4]   = p[1];
    dataToBeSent[5]   = p[0];
    dataToBeSent[0]   = CMD_DDR_READ;
    dataToBeSent[6]   = 0x00;
    dataToBeSent[7]   = 0x00;
    dataToBeSent[8]   = 0x00;
    dataToBeSent[9]   = 0x00;
    dataToBeSent[10]  = 0x00;
    dataToBeSent[11]  = 0x00;
    dataToBeSent[12]  = 0x00;
    dataToBeSent[13]  = 0x00;
    dataToBeSent[14]  = 0x00;
    dataToBeSent[15]  = 0x00;
    sendWaitForAck(&serverAddr,&clientSock,dataToBeSent,&ack);
  }
  return 0;
}
void sendWaitForAck(struct sockaddr_in *serverAddr, int *clientSock, uint8_t *dataToBeSent, int32_t *ack) {
  size_t len;
  len = 16;//In Bytes//sizeof(&*dataToBeSent);
  //! Send Message
  if(send(*clientSock,dataToBeSent,len,0) != len){
      fprintf(stderr,"Send Failed!\n");
      exit(EXIT_FAILURE);
  }
  for(int i=0; i< len;i++){
    //fprintf(stdout,"Client: Sent Buffer[%d]: 0x%" PRIx8 "\n",i,dataToBeSent[i]);
  }
  //! ACK Received
  for (int i = 0; i < len; i ++) {
    dataToBeSent[i] = 0;
  }
  if(recv(*clientSock,dataToBeSent,len,0)<0){
    fprintf(stderr,"Client: Rd Data Recv Failed!\n");
    exit(EXIT_FAILURE);
  }
  else {
    //fprintf(stdout,"Client: Got Ack From Server!\n");
    for(int i=0; i< len;i++){
      //fprintf(stdout,"Client: Recv Buffer[%0d]: 0x%" PRIx8 "\n",i,dataToBeSent[i]);
    }
  }
}

//! API For Feservver Integration 
void send_pkt(uint8_t *buffer) {
  int                 rxMsgSize;
  int                 clientSock; //Client Socket Descriptor
  struct sockaddr_in  serverAddr; // Local Address
  unsigned short      serverPort; 
  size_t len;
  len = 16; //Bytes sizeof(buffer);
  serverPort = 8081;
  createClientSock(&clientSock);
  configureServerAddr(&serverAddr,&serverPort);
  connectClientSock(&clientSock,&serverAddr);
  //! Send Message
  //fprintf(stdout,"Client: Sending Buff: 0x%" PRIx8 "\n",buffer[0]);
  if(send(clientSock,buffer,len,0) != len){
      fprintf(stderr,"Send Failed!\n");
      exit(EXIT_FAILURE);
  }
  for(int i=0; i< len;i++){
    //fprintf(stdout,"Client: Sent Buffer[%0d]: 0x%" PRIx8 "\n",i,buffer[i]);
  }
  //! Rd Data Received
  //memset(&buffer, '0', sizeof(buffer));
  for (int i = 0; i < len; i ++) {
    buffer[i] = 0;
  }
  if((rxMsgSize = recv(clientSock,buffer,len,0))<0){
    fprintf(stderr,"Rd Data Recv Failed! MsgSize: %d exp_length: %0d\n",rxMsgSize,len);
    exit(EXIT_FAILURE);
  }
  else {
    //fprintf(stdout,"Client: Got Ack From Server!\n");
    for(int i=0; i< len;i++){
      //fprintf(stdout,"Client: Recv Buffer[%0d]: 0x%" PRIx8 "\n",i,buffer[i]);
    }
  }
}

void createClientSock(int *clientSock){
  if((*clientSock = socket(PF_INET,SOCK_STREAM,IPPROTO_TCP)) < 0){
    fprintf(stderr,"Server socket creation failed!\n");
    exit(EXIT_FAILURE);
  }
  fprintf(stdout,"createClientSock: *clientSock= %0d\n",*clientSock);
}
void configureServerAddr(struct sockaddr_in *serverAddr,unsigned short *serverPort){
  memset(&*serverAddr, 0, sizeof(*serverAddr));
  serverAddr->sin_family = AF_INET;
  //Using server_port
  serverAddr->sin_port = htons(*serverPort);
  //Using IP address for FPGA
  //Comment out if testing locally
  serverAddr->sin_addr.s_addr = inet_addr("192.168.0.68");
  //fprintf(stdout,"configureServerAddr: *serverPort=%0d!\n",*serverPort);
  //fprintf(stdout,"configureServerAddr: s_addr=%0d!\n",serverAddr->sin_addr.s_addr);
}
void connectClientSock(int *clientSock,struct sockaddr_in *serverAddr){
  if(connect(*clientSock,(struct sockaddr *)&*serverAddr,sizeof(*serverAddr))<0){
    fprintf(stderr,"Connect failed\n");
    exit(EXIT_FAILURE);
  }
  fprintf(stdout,"connectClientSock successful!\n");
}
#endif

