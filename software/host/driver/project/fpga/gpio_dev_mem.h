#ifndef _GPIO_DEV_MEM_H
#define _GPIO_DEV_MEM_H
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <inttypes.h>
#include "fpga_cmds_base.h"
#include "fpga_cmds.h"
enum trgt_t {TRGT_RST,TRGT_SPI,TRGT_BRAM,TRGT_DDR};
int do_write(uint64_t addr, uint64_t val, enum trgt_t trgt);
uint64_t do_read(uint64_t addr, enum trgt_t trgt);
void open_fd();
void close_fd();
#endif
