// for uint*_t data types
#include <stdint.h>
// for print statements
#include <inttypes.h>
#include <stdio.h>
// for ctrl-c interrupt handling
#include <csignal>
// write to files
#include <cstdio>

#include "pcie_driver.c"
#define WORD_SIZE AXI_DATA_BUS_WIDTH_IN_BYTES //size in bytes
#define MEMORY_SIZE 0x80000 // 512kb bram
#define NUM_ADDRESSES MEMORY_SIZE/WORD_SIZE

#define DEBUG


int main() {  
  uint32_t addr;
  uint64_t rddata;
  uint64_t wrdata;
  
  printf("Running pcie torture test...\n");
  wrdata = 0x0000000000000000;
	do_reset(wrdata);
  sleep(1);
  wrdata = 0x0000000000000001;
	do_reset(wrdata);
  sleep(1);
  //// make sure address is bound to available memory and aligned
  ////STATION_ORV_DA_IF_PC_ADDR;
  //addr = (0x00000000%NUM_ADDRESSES) * WORD_SIZE;
  //printf("Writing pcie torture test...\n");
  //do_write(addr,0xdeadfacedeadface); 
  //printf("Reading pcie torture test...\n");
  //rddata = do_read(addr); 
  //printf("Exiting pcie torture test... rddata=0x%0x\n",rddata);
  uint8_t * buf = pcie_read(DBG_REG_ADDR);
}

