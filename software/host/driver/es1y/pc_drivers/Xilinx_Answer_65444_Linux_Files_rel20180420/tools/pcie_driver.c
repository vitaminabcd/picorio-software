// for uint*_t data types
#include <stdint.h>
// for open() on device files
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

// for malloc
#include <stdlib.h>

// for print statements
#include <stdio.h>

#define WDEVICE_NAME_DEFAULT "/dev/xdma0_h2c_0"
#define RDEVICE_NAME_DEFAULT "/dev/xdma0_c2h_0"
#define AXI_DATA_BUS_WIDTH_IN_BYTES 64
#define CMD_REG_ADDR 0x000
#define EXE_REG_ADDR 0x100

//#define DEBUG

// shove a 64bit number into an 8 bit buffer
void dataToBuffer(uint8_t *buf, uint64_t data) {
	int i = 0;
	for(i = 0; i<8; i++) {
#ifdef DEBUG
//		printf("%016lx\n", (data >> i*8) & 0XFF );
#endif
	        *buf = (data >> i*8) & 0xFF;
		buf++;
	}
}

// size here should be the number of elements in dataArr
// user needs to guarantee buf is big enough to hold the entire dataArr
void dataArrToBuffer(uint8_t *buf, uint64_t *dataArr, uint16_t size) {
	int i = 0;
	for(i=0; i<size; i++) {
		dataToBuffer(buf+(i*8), dataArr[i]);
	}
}

void printBuffer(uint8_t *buf, uint8_t size) {
	int i = 0;
        for(i=size - 1; i >= 0; i--)
        {
        	printf("%02x", buf[i]);
        }
}

int compareBuffers(uint8_t *buf1, uint8_t *buf2, int size){
	int i = 0;
	for(i = 0; i < size; i++) {
		if(buf1[i] != buf2[i]) {
			return 0;
		}
	}
	return 1;
}

// make sure writes are aligned with AXI DATA BUS WIDTH or the driver will shift things out of order on the data channel to make it align
// eg for 512 bit bus, addresses should be multiples of 0x40
// addr should not be larger than 40bit
// data going out to AXI is little endian and should be a buf created with
// uint8_t * buf = (uint8_t *) malloc (sizeof(uint8_t) * AXI_DATA_BUS_WIDTH_IN_BYTES);
int pcie_write(uint64_t addr, uint8_t *buf)
{
	int fpga_fd = open(WDEVICE_NAME_DEFAULT, O_RDWR);
	uint8_t rc = 0;
#ifdef DEBUG
	printf("Writing data 0x");
	printBuffer(buf, AXI_DATA_BUS_WIDTH_IN_BYTES);
        printf("\n");
#endif
	// set the address of the AXI channel
	if (addr != 0)
	{
		rc = lseek(fpga_fd, addr, SEEK_SET);
#ifdef DEBUG
		printf("Seeked %x bytes\n", rc);
#endif

	}

	rc = write(fpga_fd, buf, AXI_DATA_BUS_WIDTH_IN_BYTES);
#ifdef DEBUG
	printf("Wrote %d bytes\n", rc);
#endif
	close(fpga_fd);
	return rc;
}

uint8_t * pcie_read(uint64_t addr) {
	int fpga_fd = open(RDEVICE_NAME_DEFAULT, O_RDWR | O_NONBLOCK);
	uint8_t rc = 0;
	uint8_t * buf = (uint8_t *) malloc (sizeof(uint8_t) * AXI_DATA_BUS_WIDTH_IN_BYTES);
	if (addr != 0)
	{
		rc = lseek(fpga_fd, addr, SEEK_SET);
#ifdef DEBUG
		printf("Seeked %x bytes\n", rc);
#endif

	}

        rc = read(fpga_fd, buf, AXI_DATA_BUS_WIDTH_IN_BYTES);

#ifdef DEBUG
	printf("Read %d bytes\n", rc);
	printf("Reading data 0x");
	printBuffer(buf, AXI_DATA_BUS_WIDTH_IN_BYTES);
        printf("\n");
#endif

	close(fpga_fd);
	return buf;
}

int do_write(uint64_t addr, uint64_t data) {
	// format command packet
	// 63:0 -> data
	// 128:64 -> addr
	// 192:129 -> chksum
	// 256:193 -> seq
	// 512:257 -> unused
	uint64_t chksum = 0xABABABABABABABAB;
	uint64_t seq    = 0xDEDEDEDEDEDEDEDE;
	uint64_t dataArr[8] = {data, addr, chksum, seq, 0, 0, 0, 0};
	uint8_t * buf = (uint8_t *) malloc (sizeof(uint8_t) * AXI_DATA_BUS_WIDTH_IN_BYTES);
        dataArrToBuffer(buf, dataArr, 8);

	// write to command register
	pcie_write(CMD_REG_ADDR, buf);
	// write to execute register
	pcie_write(EXE_REG_ADDR, buf);

	free(buf);
}

uint64_t do_read(uint64_t addr) {
	// format command packet
	// 63:0 -> fake_data
	// 128:64 -> addr
	// 192:129 -> chksum
	// 256:193 -> seq
	// 512:257 -> unused
	uint64_t chksum = 0xCDCDCDCDCDCDCDCD;
	uint64_t seq    = 0xEFEFEFEFEFEFEFEF;
	uint64_t dataArr[8] = {0xf415ed474, addr, chksum, seq, 0, 0, 0, 0};
	uint8_t * buf = (uint8_t *) malloc (sizeof(uint8_t) * AXI_DATA_BUS_WIDTH_IN_BYTES);
        dataArrToBuffer(buf, dataArr, 8);

	// write to command register
	pcie_write(CMD_REG_ADDR, buf);
	// write to execute register
	pcie_read(EXE_REG_ADDR);

	free(buf);
}


