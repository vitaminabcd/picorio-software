//#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>

#include "ftd2xx.h"
#include "libft4222.h"
#include "common.h"
#include <unistd.h>

std::vector< FT_DEVICE_LIST_INFO_NODE > g_FT4222DevList;

int main (int argc, char const *argv[])
{
  uint8 *  readBuffer;
  uint8 *  writeBuffer;
  uint8    singleWriteBytes = 1;
  uint16   multiWriteBytes = 13;
  uint16   multiReadBytes = 8;
  uint32 * sizeOfRead;
  std::string status;

  uint16   sizeToTransfer = 23;
  uint16 * sizeTransferred;
  BOOL     isEndTransaction;

  FT_STATUS ftStatus;
  FT_HANDLE ftHandle;

  if (1) {
    GPIO_Dir dir[4];
    dir[0] = GPIO_OUTPUT;
    dir[1] = GPIO_OUTPUT;
    dir[2] = GPIO_OUTPUT;
    dir[3] = GPIO_OUTPUT;
    ftHandle = setup_gpio(dir);
    printf ("ftHandle = %d]\n", ftHandle);
    FT4222_SetSuspendOut(ftHandle, false);
    FT4222_SetWakeUpInterrupt(ftHandle, false);
    do_write_gpio(ftHandle, GPIO_PORT0, 0);
    do_write_gpio(ftHandle, GPIO_PORT1, 0);
    do_write_gpio(ftHandle, GPIO_PORT2, 0);
    do_write_gpio(ftHandle, GPIO_PORT3, 0);
  } else {
    ftHandle = setup(SPI_IO_SINGLE, CLK_DIV_128, CLK_IDLE_LOW, CLK_LEADING);
    while (1) {
      do_write(SPI_IO_SINGLE, ftHandle, 0x50505050, 0x50505050);
    }
  }

  FT4222_UnInitialize(ftHandle);
  FT_Close(ftHandle);
  return 0;
}
