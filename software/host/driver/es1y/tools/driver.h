
#include "ftd2xx.h"
#include "libft4222.h"
#include "spi_driver.h"
#include "pcie_driver.h"

uint64_t do_read (uint64_t addr, int target = 0);
int do_write (uint64_t addr, uint64_t data, int target = 0);
void setup ();
void do_status_check();


