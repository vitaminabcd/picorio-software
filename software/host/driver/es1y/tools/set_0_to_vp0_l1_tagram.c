// for uint*_t data types
#include <stdint.h>
// for print statements
#include <inttypes.h>
#include <stdio.h>
// for ctrl-c interrupt handling
#include <csignal>
// write to files
#include <cstdio>

#include "pcie_driver.h"
#include "station_vp.h"

#define WORD_SIZE AXI_DATA_BUS_WIDTH_IN_BYTES //size in bytes
#define MEMORY_SIZE 0x80000 // 512kb bram
#define NUM_ADDRESSES MEMORY_SIZE/WORD_SIZE

#define DEBUG

using namespace std;

int main(int argc, char* argv[]) {
  printf("Settint 0 to l1 tagram to vp0...\n");
  for (int index = 0; index < 128; index ++) { 
    do_write(STATION_VP_IC_TAG_WAY_0_ADDR_0 + index * 4, 0); 
    do_write(STATION_VP_IC_TAG_WAY_1_ADDR_0 + index * 4, 0); 
  } 
}

