
// for uint*_t data types
#include <stdint.h>
// for open() on device files
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

// for malloc
#include <stdlib.h>

// for print statements
#include <stdio.h>

#define WDEVICE_NAME_DEFAULT "/dev/xdma0_h2c_0"
#define RDEVICE_NAME_DEFAULT "/dev/xdma0_c2h_0"
#define AXI_DATA_BUS_WIDTH_IN_BYTES 64
#define CMD_REG_ADDR 0x000
#define EXE_REG_ADDR 0x100
#define DBG_REG_ADDR 0x200
#define RST_REG_ADDR    0x300

void dataToBuffer(uint8_t *buf, uint64_t data);

void dataArrToBuffer(uint8_t *buf, uint64_t *dataArr, uint16_t size);

void printBuffer(uint8_t *buf, uint8_t size);

int compareBuffers(uint8_t *buf1, uint8_t *buf2, int size);

int pcie_write(uint64_t addr, uint8_t *buf);

void pcie_read(uint64_t addr, uint8_t* buf);

int do_reset(uint64_t data);

int do_write_pcie(uint64_t addr, uint64_t data);

uint64_t do_read_pcie(uint64_t addr);





