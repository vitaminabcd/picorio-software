#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <inttypes.h>
#include "ftd2xx.h"
#include "libft4222.h"

inline std::string DeviceFlagToString(DWORD flags);
std::vector< FT_DEVICE_LIST_INFO_NODE > ListFtUsbDevices();
FT_HANDLE setup_spi(FT4222_SPIMode mode, FT4222_SPIClock div, FT4222_SPICPOL cpol, FT4222_SPICPHA cpha);

void do_status_check_spi(FT4222_SPIMode mode, FT_HANDLE ftHandle);
void do_write_spi(FT4222_SPIMode mode, FT_HANDLE ftHandle, uint64 addr, uint64 data);
uint64 do_read_spi(FT4222_SPIMode mode, FT_HANDLE ftHandle, uint64 addr);

