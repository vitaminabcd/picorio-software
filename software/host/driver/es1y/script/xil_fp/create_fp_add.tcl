set module XIL_fp_add

create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name $module
set_property -dict [list \
	CONFIG.Add_Sub_Value {Add} \
	CONFIG.Axi_Optimize_Goal {Resources} \
	CONFIG.Maximum_Latency {false} \
	CONFIG.C_Latency {2} \
	CONFIG.A_Precision_Type {Half} \
	CONFIG.Flow_Control {Blocking} \
	CONFIG.Has_ACLKEN {true} \
	CONFIG.Has_ARESETn {true} \
	CONFIG.C_Has_UNDERFLOW {true} \
	CONFIG.C_Has_OVERFLOW {true} \
	CONFIG.C_Has_INVALID_OP {true} \
	CONFIG.C_A_Exponent_Width {5} \
	CONFIG.C_A_Fraction_Width {11} \
	CONFIG.Result_Precision_Type {Half} \
	CONFIG.C_Result_Exponent_Width {5} \
	CONFIG.C_Result_Fraction_Width {11} \
	CONFIG.C_Accum_Msb {32} \
	CONFIG.C_Accum_Lsb {-24} \
	CONFIG.C_Accum_Input_Msb {15} \
	CONFIG.C_Mult_Usage {Full_Usage} \
	CONFIG.Has_RESULT_TREADY {true} \
	CONFIG.C_Rate {1}] [get_ips $module]
