set module XIL_fp_cmp
create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name $module
set_property -dict [list \
	CONFIG.Operation_Type {Compare}  \
	CONFIG.C_Compare_Operation {Condition_Code} \
	CONFIG.A_Precision_Type {Half}  \
	CONFIG.Axi_Optimize_Goal {Resources} \
	CONFIG.Has_ACLKEN {true}  \
	CONFIG.Has_ARESETn {true}  \
	CONFIG.Flow_Control {NonBlocking} \
	CONFIG.Maximum_Latency {false} \
	CONFIG.C_Latency {1} \
	CONFIG.Has_RESULT_TREADY {false} \
	CONFIG.C_Latency {1} \
	CONFIG.C_A_Exponent_Width {5}  \
	CONFIG.C_A_Fraction_Width {11}  \
	CONFIG.Result_Precision_Type {Custom}  \
	CONFIG.C_Result_Exponent_Width {4} \
	CONFIG.C_Result_Fraction_Width {0}  \
	CONFIG.C_Accum_Msb {32}  \
	CONFIG.C_Accum_Lsb {-24}  \
	CONFIG.C_Accum_Input_Msb {15}  \
	CONFIG.C_Mult_Usage {No_Usage}  \
	CONFIG.C_Rate {1}] [get_ips $module]
