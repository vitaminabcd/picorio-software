##################################################################
# CREATE IP XIL_fp_div_single
##################################################################

set floating_point XIL_fp_div_single
create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name $floating_point

set_property -dict { 
  CONFIG.Operation_Type {Divide}
  CONFIG.A_Precision_Type {Single}
  CONFIG.C_A_Exponent_Width {8}
  CONFIG.C_A_Fraction_Width {24}
  CONFIG.Result_Precision_Type {Single}
  CONFIG.C_Result_Exponent_Width {8}
  CONFIG.C_Result_Fraction_Width {24}
  CONFIG.C_Mult_Usage {No_Usage}
  CONFIG.Flow_Control {NonBlocking}
  CONFIG.Has_RESULT_TREADY {false}
  CONFIG.C_Latency {28}
  CONFIG.C_Rate {1}
  CONFIG.Has_ACLKEN {true}
  CONFIG.Has_ARESETn {true}
  CONFIG.C_Has_UNDERFLOW {true}
  CONFIG.C_Has_OVERFLOW {true}
  CONFIG.C_Has_INVALID_OP {true}
  CONFIG.C_Has_DIVIDE_BY_ZERO {true}
} [get_ips $floating_point]

##################################################################

