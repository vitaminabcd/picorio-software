##################################################################
# CREATE IP div_s_32
##################################################################

set div_gen div_s_32
create_ip -name div_gen -vendor xilinx.com -library ip -version 5.1 -module_name $div_gen

set_property -dict { 
  CONFIG.dividend_and_quotient_width {32}
  CONFIG.divisor_width {32}
  CONFIG.fractional_width {32}
  CONFIG.clocks_per_division {8}
  CONFIG.FlowControl {Blocking}
  CONFIG.OptimizeGoal {Resources}
  CONFIG.latency {38}
  CONFIG.ACLKEN {true}
} [get_ips $div_gen]

##################################################################

