#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <iostream>
#include <unistd.h>

#include "socket_client.h"
#include "common.h"
#include "sw_header.h"
#include "common-socket.h"
#include "pygmy_es1y_addr_translater.h"

#define BIT(nr)         (1UL << (nr))

static void process_reset(struct sockaddr_in *serverAddr, int *clientSock)
{
  do_write(serverAddr, clientSock, 0x7ffff000, 0); // set reset
  printf("Set RESET\n");
  do_write(serverAddr, clientSock, 0x7ffff000, 1); // release reset
  printf("Release RESET\n");
}

int main (int argc, char const *argv[])
{
	std::map<std::string, uint64_t> debug_map;
	static uint64_t icache_hit_start = 0;
	static uint64_t icache_true_miss_start = 0;
	static uint64_t icache_late_miss_start = 0;

  debug_map.insert(std::pair<std::string, uint64_t>("rst_pc", STATION_ORV32_S2B_CFG_RST_PC_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("bp_pc_0", STATION_ORV32_S2B_BP_PC_0_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("bp_pc_1", STATION_ORV32_S2B_BP_PC_1_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("bp_pc_2", STATION_ORV32_S2B_BP_PC_2_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("bp_pc_3", STATION_ORV32_S2B_BP_PC_3_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("bp_mem_cfg_0", STATION_ORV32_S2B_BP_MEM_CFG_0_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("bp_mem_addr_0", STATION_ORV32_S2B_BP_MEM_ADDR_0_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("if_pc", STATION_ORV32_IF_PC_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("id_pc", STATION_ORV32_ID_PC_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ma_pc", STATION_ORV32_MA_PC_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("mcycle", STATION_ORV32_MCYCLE_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("minstret", STATION_ORV32_MINSTRET_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("mstatus", STATION_ORV32_MSTATUS_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("mie", STATION_ORV32_MSTATUS_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("mip", STATION_ORV32_MIP_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("mcause", STATION_ORV32_MCAUSE_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("mepc", STATION_ORV32_MEPC_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("mtval", STATION_ORV32_MTVAL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("mtime", STATION_DMA_MTIME_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("mtimecmp", STATION_DMA_MTIMECMP_VP_0_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("mtimeen", STATION_DMA_MTIME_EN_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("hpmcounter_3", STATION_ORV32_HPMCOUNTER_3_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("hpmcounter_4", STATION_ORV32_HPMCOUNTER_4_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("hpmcounter_5", STATION_ORV32_HPMCOUNTER_5_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("hpmcounter_6", STATION_ORV32_HPMCOUNTER_6_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("hpmcounter_7", STATION_ORV32_HPMCOUNTER_7_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("hpmcounter_8", STATION_ORV32_HPMCOUNTER_8_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("hpmcounter_9", STATION_ORV32_HPMCOUNTER_9_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("hpmcounter_10", STATION_ORV32_HPMCOUNTER_10_ADDR));
  //debug_map.insert(std::pair<std::string, uint64_t>("lfsr_seed", STATION_ORV32_S2B_CFG_LFSR_SEED_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("early_rstn", STATION_ORV32_S2B_EARLY_RSTN_ADDR));
  //debug_map.insert(std::pair<std::string, uint64_t>("rstn", STATION_ORV32_S2B_RSTN_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ext_event", STATION_ORV32_S2B_EXT_EVENT_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("stall_show", STATION_ORV32_S2B_DEBUG_STALL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("b2s_debug_stall_out", STATION_ORV32_B2S_DEBUG_STALL_OUT_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("resume_show", STATION_ORV32_S2B_DEBUG_RESUME_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("en_hpmcounter", STATION_ORV32_S2B_CFG_EN_HPMCOUNTER_ADDR));
  //debug_map.insert(std::pair<std::string, uint64_t>("pwr_on", STATION_ORV32_S2B_CFG_PWR_ON_ADDR));
  //debug_map.insert(std::pair<std::string, uint64_t>("sleep", STATION_ORV32_S2B_CFG_SLEEP_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("bypass_ic", STATION_ORV32_S2B_CFG_BYPASS_IC_ADDR));
  //debug_map.insert(std::pair<std::string, uint64_t>("s2icg_clk_en", STATION_SLOW_IO_S2ICG_ORV32_CLK_EN_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("itb_en", STATION_ORV32_S2B_CFG_ITB_EN_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("itb_wrap_around", STATION_ORV32_S2B_CFG_ITB_WRAP_AROUND_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("en_bp_pc_0", STATION_ORV32_S2B_EN_BP_PC_0_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("en_bp_pc_1", STATION_ORV32_S2B_EN_BP_PC_1_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("en_bp_pc_2", STATION_ORV32_S2B_EN_BP_PC_2_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("en_bp_pc_3", STATION_ORV32_S2B_EN_BP_PC_3_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("if_stall", STATION_ORV32_IF_STALL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("if_kill", STATION_ORV32_IF_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("if_valid", STATION_ORV32_IF_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("if_ready", STATION_ORV32_IF_READY_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("id_stall", STATION_ORV32_ID_STALL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("id_kill", STATION_ORV32_ID_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("id_valid", STATION_ORV32_ID_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("id_ready", STATION_ORV32_ID_READY_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ex_stall", STATION_ORV32_EX_STALL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ex_kill", STATION_ORV32_EX_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ex_valid", STATION_ORV32_EX_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ex_ready", STATION_ORV32_EX_READY_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ma_stall", STATION_ORV32_MA_STALL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ma_kill", STATION_ORV32_MA_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ma_valid", STATION_ORV32_MA_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ma_ready", STATION_ORV32_MA_READY_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("wb_valid", STATION_ORV32_WB_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("wb_ready", STATION_ORV32_WB_READY_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("cs2if_kill", STATION_ORV32_CS2IF_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("cs2id_kill", STATION_ORV32_CS2ID_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("cs2ex_kill", STATION_ORV32_CS2EX_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("cs2ma_kill", STATION_ORV32_CS2MA_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("is_wfe", STATION_ORV32_IS_WFE_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ma2if_npc_valid", STATION_ORV32_MA2IF_NPC_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ex2if_kill", STATION_ORV32_EX2IF_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ex2id_kill", STATION_ORV32_EX2ID_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("branch_solved", STATION_ORV32_BRANCH_SOLVED_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("if2ic_pc", STATION_ORV32_IF2IC_PC_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("if2id_excp_valid", STATION_ORV32_IF2ID_EXCP_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("id2ex_excp_valid", STATION_ORV32_ID2EX_EXCP_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ex2ma_excp_valid", STATION_ORV32_EX2MA_EXCP_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ma2cs_excp_valid", STATION_ORV32_MA2CS_EXCP_VALID_ADDR));

  unsigned short serverPort = 0;
  struct sockaddr_in serverAddr;
  int clientSock;
  int reset = 0;

  for (int i = 1; i < argc; i++) {
    if (strncmp(argv[i], "serverPort=", 11) == 0) {
      serverPort = atoi(argv[i] + 11);
    } else if (strncmp(argv[i], "reset", 5) == 0) { /* for script usage and dont need enter the internal handshake mode */
		reset = 1;
	}
  }
  if (serverPort == 0) {
    serverPort = 9900; // Default
  }
  printf ("serverPort = %0d\n", serverPort);
  setup(serverPort, &serverAddr, &clientSock);

  if (reset) {
      process_reset(&serverAddr, &clientSock);
	  return 0;
  }

  while (1) {
    std::string cmd;
    std::cout << "Please enter command: (All Data in HEX no matter 0x is added or not)\n: ";
    std::getline(std::cin, cmd);
    std::string delimiter = " ";
    size_t pos = 0;
    std::vector <std::string> tokenQ;
    int dma_cmd_vld = 0;

    while ((pos = cmd.find(delimiter)) != std::string::npos) {
      tokenQ.push_back(cmd.substr(0, pos));
      cmd.erase(0, pos + delimiter.length());
    }
    if (!cmd.empty())
      tokenQ.push_back(cmd);

    if ((tokenQ.size() == 1) && (tokenQ[0] == "status")) {
      //do_status_check(mode, ftHandle);
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "setpc")) {
      do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_CFG_RST_PC_ADDR, 0x80000000);
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "list_debug")) {
      for (std::map<std::string, uint64_t>::iterator it = debug_map.begin(); it != debug_map.end(); ++it) {
        printf("\t%s\n", it->first.c_str());
      }
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "reset")) {
      process_reset(&serverAddr, &clientSock);
    } else if ((tokenQ.size() == 1) && (debug_map.count(tokenQ[0]) > 0)) {
      uint64_t addr = debug_map[tokenQ[0]];
      uint64_t data = do_read(&serverAddr, &clientSock, addr);
      printf("Do Read to Addr 0x%llx (%s), Got Data 0x%llx\n", addr, tokenQ[0].c_str(), data);
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "test_ddr")) {
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "release_vp0_reset")) {
      //do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_RSTN_ADDR, 1);
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "stall")) { //stall
        do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_DEBUG_STALL_ADDR, 1);
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "c")) { //continue
      	uint64_t pc;
        do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_DEBUG_STALL_ADDR, 0);
        do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_DEBUG_RESUME_ADDR, 1);
        pc = do_read(&serverAddr, &clientSock, STATION_ORV32_EX_PC_ADDR);
        fprintf(stderr, "pc = 0x%llx\n", pc);
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "disable_int")) {
		uint64_t value;
        value = do_read(&serverAddr, &clientSock, STATION_DMA_MTIME_EN_ADDR);
        fprintf(stderr, "mtimeen = 0x%llx \n", value);
        do_write(&serverAddr, &clientSock, STATION_DMA_MTIME_EN_ADDR, 0);
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "enable_int")) {
        do_write(&serverAddr, &clientSock, STATION_DMA_MTIME_EN_ADDR, 1);
	} else if ((tokenQ.size() == 1) && (tokenQ[0] == "wp")) {
		uint64_t wp_cfg_x;
		uint64_t wp_addr_x;
		wp_cfg_x = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_BP_MEM_CFG_0_ADDR);
		wp_addr_x = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_BP_MEM_ADDR_0_ADDR);
		fprintf(stderr, "Do Read to Addr 0x%llx, wp_cfg_0 = 0x%llx\n", STATION_ORV32_S2B_BP_MEM_CFG_0_ADDR, wp_cfg_x);
		fprintf(stderr, "Do Read to Addr 0x%llx, wp_addr_0 = 0x%llx\n", STATION_ORV32_S2B_BP_MEM_ADDR_0_ADDR, wp_addr_x);
		wp_cfg_x = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_BP_MEM_CFG_1_ADDR);
		wp_addr_x = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_BP_MEM_ADDR_1_ADDR);
		fprintf(stderr, "Do Read to Addr 0x%llx, wp_cfg_1 = 0x%llx\n", STATION_ORV32_S2B_BP_MEM_CFG_1_ADDR, wp_cfg_x);
		fprintf(stderr, "Do Read to Addr 0x%llx, wp_addr_1 = 0x%llx\n", STATION_ORV32_S2B_BP_MEM_ADDR_1_ADDR, wp_addr_x);
		wp_cfg_x = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_BP_MEM_CFG_2_ADDR);
		wp_addr_x = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_BP_MEM_ADDR_2_ADDR);
		fprintf(stderr, "Do Read to Addr 0x%llx, wp_cfg_2 = 0x%llx\n", STATION_ORV32_S2B_BP_MEM_CFG_2_ADDR, wp_cfg_x);
		fprintf(stderr, "Do Read to Addr 0x%llx, wp_addr_2 = 0x%llx\n", STATION_ORV32_S2B_BP_MEM_ADDR_2_ADDR, wp_addr_x);
		wp_cfg_x = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_BP_MEM_CFG_3_ADDR);
		wp_addr_x = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_BP_MEM_ADDR_3_ADDR);
		fprintf(stderr, "Do Read to Addr 0x%llx, wp_cfg_3 = 0x%llx\n", STATION_ORV32_S2B_BP_MEM_CFG_3_ADDR, wp_cfg_x);
		fprintf(stderr, "Do Read to Addr 0x%llx, wp_addr_3 = 0x%llx\n", STATION_ORV32_S2B_BP_MEM_ADDR_3_ADDR,   wp_addr_x);
	} else if ((tokenQ.size() == 1) && (tokenQ[0] == "bp")) {
		uint64_t bp_cfg_x;
		uint64_t en_bp_cfg_x;
		bp_cfg_x = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_BP_PC_0_ADDR);
		fprintf(stderr, "Do Read to Addr 0x%llx, bp_cfg_0 = 0x%llx\n", STATION_ORV32_S2B_BP_PC_0_ADDR, bp_cfg_x);
		en_bp_cfg_x = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_EN_BP_PC_0_ADDR);
		fprintf(stderr, "Do Read to Addr 0x%llx, en_bp_cfg_0 = 0x%llx\n", STATION_ORV32_S2B_EN_BP_PC_0_ADDR, en_bp_cfg_x);
		bp_cfg_x = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_BP_PC_1_ADDR);
		fprintf(stderr, "Do Read to Addr 0x%llx, bp_cfg_1 = 0x%llx\n", STATION_ORV32_S2B_BP_PC_1_ADDR, bp_cfg_x);
		en_bp_cfg_x = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_EN_BP_PC_1_ADDR);
		fprintf(stderr, "Do Read to Addr 0x%llx, en_bp_cfg_1 = 0x%llx\n", STATION_ORV32_S2B_EN_BP_PC_1_ADDR, en_bp_cfg_x);
		bp_cfg_x = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_BP_PC_2_ADDR);
		fprintf(stderr, "Do Read to Addr 0x%llx, bp_cfg_2 = 0x%llx\n", STATION_ORV32_S2B_BP_PC_2_ADDR, bp_cfg_x);
		en_bp_cfg_x = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_EN_BP_PC_2_ADDR);
		fprintf(stderr, "Do Read to Addr 0x%llx, en_bp_cfg_2 = 0x%llx\n", STATION_ORV32_S2B_EN_BP_PC_2_ADDR, en_bp_cfg_x);
		bp_cfg_x = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_BP_PC_3_ADDR);
		fprintf(stderr, "Do Read to Addr 0x%llx, bp_cfg_3 = 0x%llx\n", STATION_ORV32_S2B_BP_PC_3_ADDR, bp_cfg_x);
		en_bp_cfg_x = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_EN_BP_PC_3_ADDR);
		fprintf(stderr, "Do Read to Addr 0x%llx, en_bp_cfg_3 = 0x%llx\n", STATION_ORV32_S2B_EN_BP_PC_3_ADDR, en_bp_cfg_x);
	} else if ((tokenQ.size() == 1) && (tokenQ[0] == "gpr")) {
      	uint64_t gpr;
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR);
        fprintf(stderr, "Do Read to Addr 0x%llx, x1(ra)(Return Address) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 8);
        fprintf(stderr, "Do Read to Addr 0x%llx, x2(sp)(Stack Pointer) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 8, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x10);
        fprintf(stderr, "Do Read to Addr 0x%llx, x3(gp)(Global Pointer) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x10, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x18);
        fprintf(stderr, "Do Read to Addr 0x%llx, x4(tp)(Thread Pointer) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x18, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x20);
        fprintf(stderr, "Do Read to Addr 0x%llx, x5(t0)(Temp) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x20, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x28);
        fprintf(stderr, "Do Read to Addr 0x%llx, x6(t1)(Temp) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x28, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x30);
        fprintf(stderr, "Do Read to Addr 0x%llx, x7(t2)(Temp) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x30, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x38);
        fprintf(stderr, "Do Read to Addr 0x%llx, x8(s0/fp)(Saved Register/Frame Register) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x38, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x40);
        fprintf(stderr, "Do Read to Addr 0x%llx, x9(s1)(Saved Register) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x40, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x48);
        fprintf(stderr, "Do Read to Addr 0x%llx, x10(a0)(Func Arg) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x48, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x50);
        fprintf(stderr, "Do Read to Addr 0x%llx, x11(a1)(Func Arg) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x50, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x58);
        fprintf(stderr, "Do Read to Addr 0x%llx, x12(a2)(Func Arg) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x58, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x60);
        fprintf(stderr, "Do Read to Addr 0x%llx, x13(a3)(Func Arg) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x60, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x68);
        fprintf(stderr, "Do Read to Addr 0x%llx, x14(a4)(Func Arg) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x68, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x70);
        fprintf(stderr, "Do Read to Addr 0x%llx, x15(a5)(Func Arg) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x70, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x78);
        fprintf(stderr, "Do Read to Addr 0x%llx, x16(a6)(Func Arg) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x78, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x80);
        fprintf(stderr, "Do Read to Addr 0x%llx, x17(a7)(Func Arg) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x80, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x88);
        fprintf(stderr, "Do Read to Addr 0x%llx, x18(s2)(Saved Register) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x88, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x90);
        fprintf(stderr, "Do Read to Addr 0x%llx, x19(s3)(Saved Register) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x90, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0x98);
        fprintf(stderr, "Do Read to Addr 0x%llx, x20(s4)(Saved Register) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0x98, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0xa0);
        fprintf(stderr, "Do Read to Addr 0x%llx, x21(s5)(Saved Register) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0xa0, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0xa8);
        fprintf(stderr, "Do Read to Addr 0x%llx, x22(s6)(Saved Register) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0xa8, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0xb0);
        fprintf(stderr, "Do Read to Addr 0x%llx, x23(s7)(Saved Register) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0xb0, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0xb8);
        fprintf(stderr, "Do Read to Addr 0x%llx, x24(s8)(Saved Register) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0xb8, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0xc0);
        fprintf(stderr, "Do Read to Addr 0x%llx, x25(s9)(Saved Register) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0xc0, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0xc8);
        fprintf(stderr, "Do Read to Addr 0x%llx, x26(s10)(Saved Register) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0xc8, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0xd0);
        fprintf(stderr, "Do Read to Addr 0x%llx, x27(s11)(Saved Register) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0xd0, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0xd8);
        fprintf(stderr, "Do Read to Addr 0x%llx, x28(t3)(Temp) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0xd8, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0xe0);
        fprintf(stderr, "Do Read to Addr 0x%llx, x29(t4)(Temp) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0xe0, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0xe8);
        fprintf(stderr, "Do Read to Addr 0x%llx, x30(t5)(Temp) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0xe8, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0xf0);
        fprintf(stderr, "Do Read to Addr 0x%llx, x31(t6)(Temp) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0xf0, gpr);
        gpr = do_read(&serverAddr, &clientSock, STATION_ORV32_REGFILE_ADDR + 0xf8);
        fprintf(stderr, "Do Read to Addr 0x%llx, x32(pc)(Program Counter) = 0x%llx\n", STATION_ORV32_REGFILE_ADDR + 0xf8, gpr);
	} else if ((tokenQ.size() == 1) && (tokenQ[0] == "itb")) {
		uint64_t pc;
		uint64_t last = do_read(&serverAddr, &clientSock, STATION_ORV32_B2S_ITB_LAST_PTR_ADDR);
		uint64_t size = do_read(&serverAddr, &clientSock, STATION_ORV32_B2S_ITB_SIZE_ADDR);
		uint64_t mode = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_CFG_ITB_SEL_ADDR);
		fprintf(stderr, "last = 0x%llx, size = %d, mode = %d\n", last, size, mode);
		fprintf(stderr, "0: if pc | 1: id pc | 2: ex pc | 3: ma pc | 4: stack mode \n");
		for (int i = 0; i < 32; i++) {
			pc = do_read(&serverAddr, &clientSock, STATION_ORV32_ITB_ADDR + (i<<3));
			if(last == i)
				fprintf(stderr, "this is latest entry -> \n");
			fprintf(stderr, "addr = 0x%llx pc = 0x%llx\n", STATION_ORV32_ITB_ADDR + (i<<3), pc);
		}
	} else if ((tokenQ.size() == 2) && (tokenQ[0] == "itb_cfg")) {
		char * p;
		uint32_t sel = strtoul(tokenQ[1].c_str(), &p, 10);
		//disable itb first
		do_write(&serverAddr, &clientSock, STATION_ORV32_DEBUG_INFO_ENABLE_ADDR, 0);
		//backup itb info
		uint64_t last = do_read(&serverAddr, &clientSock, STATION_ORV32_B2S_ITB_LAST_PTR_ADDR);
		uint64_t size = do_read(&serverAddr, &clientSock, STATION_ORV32_B2S_ITB_SIZE_ADDR);
		uint64_t mode = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_CFG_ITB_SEL_ADDR);
		fprintf(stderr, "before enable new cfg: last = 0x%llx, size = %d, mode = %d\n", last, size, mode);
		//switch mode and enable
		do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_CFG_ITB_SEL_ADDR, sel);
		do_write(&serverAddr, &clientSock, STATION_ORV32_DEBUG_INFO_ENABLE_ADDR, 1);
	} else if ((tokenQ.size() == 1) && (tokenQ[0] == "pmu_start")) {
		icache_hit_start = do_read(&serverAddr, &clientSock, STATION_ORV32_HPMCOUNTER_9_ADDR); // icache hit
		icache_true_miss_start = do_read(&serverAddr, &clientSock, STATION_ORV32_HPMCOUNTER_10_ADDR); // icache true miss
		icache_late_miss_start = do_read(&serverAddr, &clientSock, STATION_ORV32_HPMCOUNTER_11_ADDR); // icache late miss
		fprintf(stderr, "icache_hit_start = %lld icache_true_miss_start = %lld icache_late_miss_start = %lld \n", icache_hit_start, icache_true_miss_start, icache_late_miss_start);
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "pmu_end")) {
		uint64_t icache_hit_end = do_read(&serverAddr, &clientSock, STATION_ORV32_HPMCOUNTER_9_ADDR); // icache hit
		uint64_t icache_true_miss_end = do_read(&serverAddr, &clientSock, STATION_ORV32_HPMCOUNTER_10_ADDR); // icache true miss
		uint64_t icache_late_miss_end = do_read(&serverAddr, &clientSock, STATION_ORV32_HPMCOUNTER_11_ADDR); // icache late miss
		uint64_t icache_hit_offset = icache_hit_end - icache_hit_start;
		uint64_t icache_true_miss_offset = icache_true_miss_end - icache_true_miss_start;
		uint64_t icache_late_miss_offset = icache_late_miss_end - icache_late_miss_start;
		fprintf(stderr, "icache_hit_end = %lld icache_true_miss_end = %lld icache_late_miss_end = %lld \n", icache_hit_end, icache_true_miss_end, icache_late_miss_end);
		fprintf(stderr, "icache_hit_offset = %lld icache_true_miss_offset = %lld icache_late_miss_offset = %lld \n", icache_hit_offset, icache_true_miss_offset, icache_late_miss_offset);
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "gpio")) {
      uint64_t value;
	  for (int i = 0; i < 32; i++) {
        value = do_read(&serverAddr, &clientSock, GPIO_DR_ADDR_0 + (i<<2));
        fprintf(stderr, "addr = 0x%x value = 0x%x\n", GPIO_DR_ADDR_0 + (i<<2), value);
	  }
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "wdt")) {
      uint64_t value;
	  for (int i = 0; i < 32; i++) {
        value = do_read(&serverAddr, &clientSock, STATION_SLOW_IO_WDT_BLOCK_REG_ADDR + (i<<2));
        fprintf(stderr, "addr = 0x%x value = 0x%x\n", STATION_SLOW_IO_WDT_BLOCK_REG_ADDR + (i<<2), value);
	  }
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "rtc")) {
      uint64_t value;
	  for (int i = 0; i < 16; i++) {
        value = do_read(&serverAddr, &clientSock, STATION_SLOW_IO_RTC_BLOCK_REG_ADDR + (i<<2));
        fprintf(stderr, "addr = 0x%x value = 0x%x\n", STATION_SLOW_IO_RTC_BLOCK_REG_ADDR + (i<<2), value);
	  }
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "i2c0")) {
      uint64_t value;
	  for (int i = 0; i < 64; i++) {
        value = do_read(&serverAddr, &clientSock, STATION_SLOW_IO_I2C0_BLOCK_REG_ADDR + (i<<2));
        fprintf(stderr, "addr = 0x%x value = 0x%x\n", STATION_SLOW_IO_I2C0_BLOCK_REG_ADDR + (i<<2), value);
	  }
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "flash_boot")) {
      uint64_t value;
        value = do_read(&serverAddr, &clientSock, STATION_SLOW_IO_SCU_FRST2S_BLOCKS_ADDR);
        fprintf(stderr, "FRST2S addr = 0x%x value = 0x%x\n", STATION_SLOW_IO_SCU_FRST2S_BLOCKS_ADDR, value);
        value = do_read(&serverAddr, &clientSock, STATION_SLOW_IO_SCU_S2ICG_BLOCKS_ADDR);
        fprintf(stderr, "S2ICG addr = 0x%x value = 0x%x\n", STATION_SLOW_IO_SCU_S2ICG_BLOCKS_ADDR, value);
		value |= (BIT(BLOCK_ORV32) | BIT(BLOCK_FLASH_CTRL) | BIT(BLOCK_BANK)); //0x24; //de-assert flash ctrl reset and orv32
        //do_write(&serverAddr, &clientSock, STATION_SLOW_IO_SCU_S2ICG_BLOCKS_ADDR, 1);

        value = do_read(&serverAddr, &clientSock, STATION_SLOW_IO_SCU_S2FRST_BLOCKS_ADDR);
        fprintf(stderr, "S2FRST addr = 0x%x value = 0x%x\n", STATION_SLOW_IO_SCU_S2FRST_BLOCKS_ADDR, value);
		value |= (BIT(BLOCK_ORV32) | BIT(BLOCK_FLASH_CTRL) | BIT(BLOCK_BANK)); //0x24; //de-assert flash ctrl reset and orv32
        do_write(&serverAddr, &clientSock, STATION_SLOW_IO_SCU_S2FRST_BLOCKS_ADDR, value);
        fprintf(stderr, "write S2FRST addr = 0x%x value = 0x%x\n", STATION_SLOW_IO_SCU_S2FRST_BLOCKS_ADDR, value);
        value = do_read(&serverAddr, &clientSock, STATION_SLOW_IO_SCU_S2FRST_BLOCKS_ADDR);
        fprintf(stderr, "after write: S2FRST addr = 0x%x value = 0x%x\n", STATION_SLOW_IO_SCU_S2FRST_BLOCKS_ADDR, value);
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "clk")) {
      uint64_t value;
        value = do_read(&serverAddr, &clientSock, STATION_SLOW_IO_SCU_FRST2S_BLOCKS_ADDR);
        fprintf(stderr, "FRST2S addr = 0x%x value = 0x%x\n", STATION_SLOW_IO_SCU_FRST2S_BLOCKS_ADDR, value);
        value = do_read(&serverAddr, &clientSock, STATION_SLOW_IO_SCU_S2ICG_BLOCKS_ADDR);
        fprintf(stderr, "S2ICG addr = 0x%x value = 0x%x\n", STATION_SLOW_IO_SCU_S2ICG_BLOCKS_ADDR, value);
        value = do_read(&serverAddr, &clientSock, STATION_SLOW_IO_SCU_S2FRST_BLOCKS_ADDR);
        fprintf(stderr, "S2FRST addr = 0x%x value = 0x%x\n", STATION_SLOW_IO_SCU_S2FRST_BLOCKS_ADDR, value);
	} else if ((tokenQ.size() == 1) && (tokenQ[0] == "ex_pc")) {
		char * p;
		uint64_t value;
		value = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_EARLY_RSTN_ADDR);
		if(0 == value) {
			fprintf(stderr, "release EARLY_RSTN first.\n");
			do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_EARLY_RSTN_ADDR, 1);
		}
		value = do_read(&serverAddr, &clientSock, STATION_ORV32_EX_PC_ADDR);
		fprintf(stderr, "Do Read to Addr 0x%llx , Got Data 0x%llx\n", STATION_ORV32_EX_PC_ADDR, value);
	} else if ((tokenQ.size() == 1) && (tokenQ[0] == "d1")) {
      char * p;
	  uint64_t pc;
	  fprintf(stderr, "del hw breakpoint1 \n");
	  do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_EN_BP_PC_1_ADDR, 0);
	} else if ((tokenQ.size() == 1) && (tokenQ[0] == "d2")) {
      char * p;
	  uint64_t pc;
	  fprintf(stderr, "del hw breakpoint2 \n");
	  do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_EN_BP_PC_2_ADDR, 0);
	} else if ((tokenQ.size() == 1) && (tokenQ[0] == "d3")) {
      char * p;
	  uint64_t pc;
	  fprintf(stderr, "del hw breakpoint3 \n");
	  do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_EN_BP_PC_3_ADDR, 0);
	} else if ((tokenQ.size() == 2) && (tokenQ[0] == "step")) {
      char * p;
      uint32_t cnt = strtoul(tokenQ[1].c_str(), & p, 10);
      uint64_t pc;
      for (int i = 0; i < cnt; i ++) {
        do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_DEBUG_STALL_ADDR, 1);
        do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_DEBUG_RESUME_ADDR, 1);
        pc = do_read(&serverAddr, &clientSock, STATION_ORV32_EX_PC_ADDR);
        fprintf(stderr, "pc = 0x%llx\n", pc);
      }
	} else if ((tokenQ.size() == 2) && (tokenQ[0] == "b1")) {
      char * p;
      uint64_t addr = strtoul(tokenQ[1].c_str(), & p, 16);
	  uint64_t pc;
	  fprintf(stderr, "set hw breakpoint1 at ex_pc = 0x%llx\n", addr);
	  do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_BP_PC_1_ADDR, addr);
	  do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_EN_BP_PC_1_ADDR, 1);
	} else if ((tokenQ.size() == 2) && (tokenQ[0] == "b2")) {
      char * p;
      uint64_t addr = strtoul(tokenQ[1].c_str(), & p, 16);
	  uint64_t pc;
	  fprintf(stderr, "set hw breakpoint2 at ex_pc = 0x%llx\n", addr);
	  do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_BP_PC_2_ADDR, addr);
	  do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_EN_BP_PC_2_ADDR, 1);
	} else if ((tokenQ.size() == 2) && (tokenQ[0] == "b3")) {
      char * p;
      uint64_t addr = strtoul(tokenQ[1].c_str(), & p, 16);
	  uint64_t pc;
	  fprintf(stderr, "set hw breakpoint3 at ex_pc = 0x%llx\n", addr);
	  do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_BP_PC_3_ADDR, addr);
	  do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_EN_BP_PC_3_ADDR, 1);
    } else if ((tokenQ.size() == 0) || (tokenQ[0] == "help") || (tokenQ[0] == "h")) {
      std::cout << "This is Help Info\n";
    } else if ((tokenQ[0] == "quit") || (tokenQ[0] == "q") || (tokenQ[0] == "exit")) {
      break;
    } else if ((tokenQ.size() == 3) && (tokenQ[0] == "read")) {
      char * p;
      uint64_t data = 0;
      uint64_t addr = strtoul(tokenQ[1].c_str(), & p, 16);
      std::string target = tokenQ[2];
      if (*p == 0) {
        if (target == "rb") {
          data = do_read(&serverAddr, &clientSock, addr);
        } else if (target == "dma") {
          do_write(&serverAddr, &clientSock, STATION_DMA_DMA_DEBUG_ADDR_ADDR, addr);
          do_write(&serverAddr, &clientSock, STATION_DMA_DMA_DEBUG_REQ_TYPE_ADDR, 0);
          data = do_read(&serverAddr, &clientSock, STATION_DMA_DMA_DEBUG_RD_DATA_ADDR);
        } else if (target == "dt") {
          //do_write(&serverAddr, &clientSock, STATION_DT_DBG_ADDR_ADDR, addr);
          //data = do_read(&serverAddr, &clientSock, STATION_DT_DBG_DATA_ADDR);
        } else {
          data = do_read(&serverAddr, &clientSock, addr2oraddr(target, addr));
        }
        printf("Do Read to Addr 0x%llx, Got Data 0x%llx\n", addr, data);
      }
    } else if ((tokenQ.size() == 4) && (tokenQ[0] == "write")) {
      char * p;
      uint64_t addr = strtoul(tokenQ[1].c_str(), & p, 16);
      uint64_t data = strtoul(tokenQ[2].c_str(), & p, 16);
      std::string target = tokenQ[3];
      if (*p == 0) {
        if (target == "rb") {
          do_write(&serverAddr, &clientSock, addr, data);
        } else if (target == "dma") {
          do_write(&serverAddr, &clientSock, STATION_DMA_DMA_DEBUG_ADDR_ADDR, addr);
          do_write(&serverAddr, &clientSock, STATION_DMA_DMA_DEBUG_REQ_TYPE_ADDR, 2);
          do_write(&serverAddr, &clientSock, STATION_DMA_DMA_DEBUG_WR_DATA_ADDR, data);
        } else if (target == "dt") {
          //do_write(&serverAddr, &clientSock, STATION_DT_DBG_ADDR_ADDR, addr);
          //do_write(&serverAddr, &clientSock, STATION_DT_DBG_DATA_ADDR, data);
        } else {
          do_write(&serverAddr, &clientSock, addr2oraddr(target, addr), data);
        }
        printf("Do Write to Addr 0x%llx with Data 0x%llx\n", addr, data);
      }
    } else if ((tokenQ.size() == 4) && (tokenQ[0] == "dump")) {
      char * p;
      uint64_t data = 0;
      uint64_t addr_lo = strtoul(tokenQ[1].c_str(), & p, 16);
      uint64_t addr_hi = strtoul(tokenQ[2].c_str(), & p, 16);
      std::string target = tokenQ[3];
      if (*p == 0) {
        for (uint64_t addr = addr_lo; addr <= addr_hi; addr += 8) {
          if (target == "rb") {
            data = do_read(&serverAddr, &clientSock, addr);
          } else if (target == "dma") {
            do_write(&serverAddr, &clientSock, STATION_DMA_DMA_DEBUG_ADDR_ADDR, addr);
            do_write(&serverAddr, &clientSock, STATION_DMA_DMA_DEBUG_REQ_TYPE_ADDR, 0);
            data = do_read(&serverAddr, &clientSock, STATION_DMA_DMA_DEBUG_RD_DATA_ADDR);
          } else if (target == "dt") {
            //do_write(&serverAddr, &clientSock, STATION_DT_DBG_ADDR_ADDR, addr);
            //data = do_read(&serverAddr, &clientSock, STATION_DT_DBG_DATA_ADDR);
          } else {
            data = do_read(&serverAddr, &clientSock, addr2oraddr(target, addr));
          }
          printf("0x%llx: 0x%08llx\n", addr + 0, (data >>  0) & 0xffffffff);
          printf("0x%llx: 0x%08llx\n", addr + 4, (data >> 32) & 0xffffffff);
        }
      }
    } else {
      std::cout << "Unrecognized Command; Please use help or h to see supported command list.\n";
    }
  }
  return 0;
}
