#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <iostream>

#include "ftd2xx.h"
#include "libft4222.h"
#include "debug_tick.h"
#include "common.h"
#include "pygmy_es1_addr_translater.h"

#include <time.h>

int main () {
  std::string status;
  FT4222_SPIMode mode = SPI_IO_SINGLE;

  FT_HANDLE ftHandle = setup(mode, CLK_DIV_64, CLK_IDLE_LOW, CLK_LEADING);
  if (ftHandle == NULL)
    return 0;

  do_status_check(mode, ftHandle);

  //// PLL 600MHz
  //do_write(mode, ftHandle, 0x8000000000, 0x40280a4);

  // VCore Power Off
  for (int i = 0; i < 16; i++) {
    do_write(mode, ftHandle, 0x8000000000 + (i + 0xA) * 8, 0x06);
  }

  //// L2 Way Sleep
  //do_write(mode, ftHandle, 0x8000000000 + 0x1B * 8, 0x100005555);

  // L2 Way Down
  do_write(mode, ftHandle, 0x8000000000 + 0x1B * 8, 0x10000ffff);

  //// MP Power Off
  //do_write(mode, ftHandle, 0x8000000000 + 0x2 * 8, 0x306);

}
