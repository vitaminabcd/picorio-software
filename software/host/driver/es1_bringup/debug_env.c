#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <iostream>

#include "ftd2xx.h"
#include "libft4222.h"
#include "debug_tick.h"
#include "common.h"
#include "pygmy_es1_addr_translater.h"

int reset_mp(FT4222_SPIMode mode, FT_HANDLE ftHandle) {
  do_write(mode, ftHandle, 0x8000000000 + 8 * 2, 0xc);
};

int reset_uncore(FT4222_SPIMode mode, FT_HANDLE ftHandle) {
  fprintf(stderr, "1\n");
};

int reset_vcore(FT4222_SPIMode mode, FT_HANDLE ftHandle) {
  fprintf(stderr, "1\n");
};

typedef int (*func_t) (FT4222_SPIMode, FT_HANDLE);

int main (int argc, char const *argv[])
{
  std::string status;
  FT4222_SPIMode mode = SPI_IO_SINGLE;

  FT_HANDLE ftHandle = setup(mode, CLK_DIV_64, CLK_IDLE_LOW, CLK_LEADING);
  if (ftHandle == NULL)
    return 0;

  do_status_check(mode, ftHandle);

  std::map<std::string, func_t> commands = {{"reset_mp", &reset_mp}, {"reset_uncore", &reset_uncore}, {"reset_vcore", &reset_vcore}};

  if (argc == 1) {
    while (1) {
      std::string cmd;
      std::cout << "Please enter command: (All Data in HEX no matter 0x is added or not)\n: ";
      std::getline(std::cin, cmd);
      std::string delimiter = " ";
      size_t pos = 0;
      std::vector <std::string> tokenQ;
  
      while ((pos = cmd.find(delimiter)) != std::string::npos) {
        tokenQ.push_back(cmd.substr(0, pos));
        cmd.erase(0, pos + delimiter.length());
      }
      if (!cmd.empty())
        tokenQ.push_back(cmd);
  
      if ((tokenQ.size() == 1) && (tokenQ[0] == "status")) {
        do_status_check(mode, ftHandle);
      } else if ((tokenQ.size() == 0) || (tokenQ[0] == "help") || (tokenQ[0] == "h")) {
        std::cout << "This is Help Info\n";
      } else if ((tokenQ[0] == "quit") || (tokenQ[0] == "q") || (tokenQ[0] == "exit")) {
        break;
      } else if ((tokenQ.size() == 3) && (tokenQ[0] == "read")) {
        char * p;
        uint64_t data = 0;
        uint64_t addr = strtoul(tokenQ[1].c_str(), & p, 16);
        std::string target = tokenQ[2];
        if (*p == 0) {
          if (target == "rb")
            data = do_read(mode, ftHandle, addr);
          else
            data = do_read(mode, ftHandle, addr2obaddr(target, addr));
          printf("Do Read to Addr 0x%llx, Got Data 0x%llx\n", addr, data);
        }
      } else if ((tokenQ.size() == 4) && (tokenQ[0] == "write")) {
        char * p;
        uint64_t addr = strtoul(tokenQ[1].c_str(), & p, 16);
        uint64_t data = strtoul(tokenQ[2].c_str(), & p, 16);
        std::string target = tokenQ[3];
        if (*p == 0) {
          if (target == "rb")
            do_write(mode, ftHandle, addr, data);
          else
            do_write(mode, ftHandle, addr2obaddr(target, addr), data);
          printf("Do Write to Addr 0x%llx with Data 0x%llx\n", addr, data);
        }
      } else if ((tokenQ.size() == 4) && (tokenQ[0] == "dump")) {
        char * p;
        uint64_t data = 0;
        uint64_t addr_lo = strtoul(tokenQ[1].c_str(), & p, 16);
        uint64_t addr_hi = strtoul(tokenQ[2].c_str(), & p, 16);
        std::string target = tokenQ[3];
        if (*p == 0) {
          for (uint64_t addr = addr_lo; addr <= addr_hi; addr += 8) {
            if (target == "rb") {
              data = do_read(mode, ftHandle, addr);
            } else {
              data = do_read(mode, ftHandle, addr2obaddr(target, addr));
            }
            printf("0x%llx: 0x%08llx\n", addr + 0, (data >>  0) & 0xffffffff);
            printf("0x%llx: 0x%08llx\n", addr + 4, (data >> 32) & 0xffffffff);
          }
        }
      } else {
        std::cout << "Unrecognized Command; Please use help or h to see supported command list.\n";
      }
    }
  } else if (argc == 2) {
    // Command Mode:
    int a = 0;
    fprintf(stderr, "Command: %s\n", argv[1]);
    for (std::map<std::string, func_t>::iterator it = commands.begin(); it != commands.end(); ++it) {
      if (strcmp(argv[1], it->first.c_str()) == 0) {
        (it->second)(mode, ftHandle);
      }
    }
  } else {
    fprintf(stderr, "TODO: Add description\n");
  }

  FT4222_UnInitialize(ftHandle);
  FT_Close(ftHandle);
  return 0;
}
