#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <vector>
#include <string>
#include <string.h>
#include <map>

std::map<std::string, uint8_t> obid_map = {
  {"rb",      0b100000},
  {"mmem",    0b100001},
  {"mp",      0b100010},
  {"ddr",     0b100011},
  {"dt",      0b100101},
  {"dma",     0b100111},
  {"l2_vld",  0b101000},
  {"l2_dirty",0b101001},
  {"l2_tag",  0b101010},
  {"l2_data", 0b101011},
  {"l2_lru",  0b101100},
  {"l2_perf", 0b101101}
};

uint64_t addr2obaddr (std::string target, uint64_t addr) {
  uint64_t obaddr = 0;
  if (target.substr(0, 2) == "l2") {
    int chk = (addr >> 3) & 0x3; // 2 bits
    if ((target.substr(3, 3) == "vld") || (target.substr(3, 3) == "tag")) {
      chk = 0;
    }
    int bid = (addr >> 5) & 0x7; // 3 bits
    int bix = (addr >> 8) & 0x1ff; // 9 bits
    int wid = (addr >> 17) & 0x7; // 3 bits
    int obid = obid_map[target];
    obaddr += obid;
    obaddr = (obaddr << 20) + bid;
    obaddr = (obaddr << 3) + wid;
    obaddr = (obaddr << 2) + chk;
    obaddr = (obaddr << 9) + bix;
  } else if (target.substr(0, 2) == "dt") {
    int obid = obid_map[target];
    obaddr += obid;
    obaddr = (obaddr << 31) + (addr >> 3);
    obaddr = (obaddr << 3) + 0x5;
  } else if (target == "mmem") {
    int obid = obid_map[target];
    obaddr += obid;
    obaddr = (obaddr << 34) + ((addr >> 3) << 3);
  } else if (target == "dma") {
    int obid = obid_map[target];
    obaddr += obid;
    obaddr = (obaddr << 31) + (addr >> 3);
    obaddr = (obaddr << 3) + 0x3;
  }
  return obaddr;
}

