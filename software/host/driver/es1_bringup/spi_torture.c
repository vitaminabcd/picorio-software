#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <iostream>
#include <map>

#include "ftd2xx.h"
#include "libft4222.h"
#include "debug_tick.h"
#include "common.h"

int main (int argc, char const *argv[])
{
  std::string status;
  FT4222_SPIMode mode = SPI_IO_SINGLE;

  FT_HANDLE ftHandle = setup(mode, CLK_DIV_64, CLK_IDLE_LOW, CLK_LEADING);
  if (ftHandle == NULL)
    return 0;

  do_status_check(mode, ftHandle);

  std::map<int, uint64_t> sb;
  int fail = 0;
  int weight;

  while (fail == 0) {
    weight = rand() % 100;
    if (weight < 45) {
      // DO READ
      uint32_t data = 0;
      uint64_t addr = (rand() % 0x2000) * 8;
      data = do_read(mode, ftHandle, addr);
      if (sb.find(addr) != sb.end()) {
        // DO DATA CHECK
        if ((sb[addr] & 0xffffffff) != data) {
          fail = 1;
          printf("FAILED: addr = %x, exp_data = %llx, received data = %x, exiting...\n", addr, sb[addr], data);
        }
      }
    } else if (weight < 90) {
      // DO WRITE
      uint64_t addr = (rand() % 0x2000) * 8;
      uint64_t data = rand();
      data = data << 32;
      data = data | rand();
      do_write(mode, ftHandle, addr, data);
      sb[addr] = data;
    } else {
      // DO STATUS
      do_status_check(mode, ftHandle);
    }
  }

  FT4222_UnInitialize(ftHandle);
  FT_Close(ftHandle);
  return 0;
}
