#!/usr/bin/env python3

import ctypes
import numpy as np
import operator
import copy
from dbg_defines import *
import csv, os, sys, string
from driver import driver
import collections


# The physical layer represents a description of the hardware, interacting only with
# the driver API and providing appropriate setter/getter functions

# General address convention: store as a decimal integer but always print in
# hexadecimal (e.g. '0x...')

class FIELD:
    #def __init__(self, name, start_bit, num_bits, desc, value, valid_vals):
    def __init__(self):
        # self.NAME = name
        # self.START_BIT = int(start_bit)
        # self.NUM_BITS = int(num_bits)
        # self.DESCRIPTION = desc
        # self.VALUE = value                                  # int (decimal)
        # self.VALID_VALUES = valid_vals
        self.NAME = ""
        self.START_BIT = UNINITIALIZED
        self.NUM_BITS = UNINITIALIZED
        self.DESCRIPTION = ""
        self.VALUE = UNINITIALIZED
        self.VALID_VALUES = UNINITIALIZED
        self.RESET_VALUE = UNINITIALIZED

    def __repr__(self):
        s = 'START BIT: %d\n' % self.START_BIT
        s += 'NUM BITS: %d\n' % self.NUM_BITS
        # s += 'VALUE: %d\n' % self.VALUE
        s += 'DESCRIPTION: %s\n' % self.DESCRIPTION
        return s

    def get_name(self):
        return self.NAME

    def get_start_bit(self):
        return int(self.START_BIT)

    def get_num_bits(self):
        return int(self.NUM_BITS)

    def get_value(self):
        return int(self.VALUE)

    def get_bin_value(self):
        bin_val = bin(self.VALUE).split('b')[1]
        while (len(bin_val) < int(self.NUM_BITS)):
            bin_val = '0' + str(bin_val)
        return bin_val

    def set_value(self, value):
        self.VALUE = value

    def get_description(self):
        return self.DESCRIPTION

class REG:
    def __init__(self):
        self.REGBLK = None
        self.NAME = ""
        self.WIDTH = ""
        self.RESET_VALUE = ""
        self.LOCATION = ""
        self.ACCESS_TYPE = ""
        self.TYPE = LOGIC
        self.SW_ACCESS_PERMISSION = ""
        self.UNDER_SHARED_ICG = "YES"
        self.BLOCK_WRITABLE = ""
        self.BLOCK_WRITE_ENABLE_INFERRED = "NO"
        self.WIDTH_IN_BITS = UNINITIALIZED
        self.OFFSET = UNINITIALIZED
        self.AUTO_CLEAR = "NO"
        self.ADDR = UNINITIALIZED                               # int (decimal)
        self.ADDR_SPACE = 'LOCAL'
        self.BASE_OFFSET = 0
        self.DEPTH = 1
        self.HW_WRITABLE = WR_NO
        self.ACCESS_IN_32_BITS = "NO"

        # Read into both, update only STAGED_FIELDS, write based on STAGED_FIELDS
        self.FIELDS = []
        self.STAGED_FIELDS = []

        # For keeping track of whether this register was initially read yet
        self.INITIALIZED = False

    def print2python(self, indent, nindent):
        s = "%sREG (\n" % (nindent * indent)
        for item in dir(self):
          if (item in []) or (callable(getattr(self, item))) or (item.startswith("__") and item.endswith("__")):
            continue
          else:
            s += "%s%s = %s,\n" % ((nindent + 1) * indent, item, printVal2Python(getattr(self, item)))
        s += "%s),\n" % (nindent * indent)
        return (s)

    def typecast_pre(self):
      if self.TYPE == LOGIC:
        return ""
      else:
        return "%s'(" % self.TYPE

    def typecast_post(self):
      if self.TYPE == LOGIC:
        return ""
      else:
        return ")"

    def __repr__(self):
        s = '==========\n'
        s += "NAME: %s\n" % self.NAME
        for item in dir(self):
            if (item in ['ADDR', 'OFFSET']):
                s += '%s: 0x%x\n' % (item, int(getattr(self, item)))
            elif (item in ['FIELDS', 'WIDTH', 'LOCATION', 'ACCESS_TYPE', 'BLOCK_WRITABLE', 'ADDR_SPACE', 'BASE_OFFSET', 'STAGED_FIELDS', 'INITIALIZED', 'OFFSET']):
                continue
            elif (item != 'NAME' and not callable(getattr(self, item)) and not ('__' in item)):
                s += '%s: %s\n' % (item, getattr(self, item))
        s += '----------\n'
        for field in self.FIELDS:
            s += 'Field: ' + field.get_name() + '\n'
            s += field.__repr__()
        return s

    def check_permission(self):
        # Check that this register has RW, RO, or WO access
        if self.SW_ACCESS_PERMISSION not in [RW, RO, WO]:
            print('Reg %s does not have RW/RO/WO access: %s' % (self.NAME, self.SW_ACCESS_PERMISSION))
            return False

        return True

    def get_location(self):
        return self.LOCATION

    def get_name(self):
        return ('station_' + self.REGBLK.OWNER + '__' + self.NAME).upper()

    def get_address(self):
        return int(self.ADDR)

    def get_offset(self):
        return int(self.OFFSET)

    def get_base_offset(self):
        return int(self.BASE_OFFSET)

    def get_width_in_bits(self):
        return self.WIDTH_IN_BITS

    def get_fields(self):
        return self.FIELDS

    def get_staged_fields(self):
        return self.STAGED_FIELDS

    def get_reset_value(self):
        return self.RESET_VALUE

    def get_sw_access_permission(self):
        return self.SW_ACCESS_PERMISSION

    def get_type(self):
        return self.TYPE

    def get_access_type(self):
        return self.ACCESS_TYPE

    def set_address(self, addr):
        self.ADDR = addr

    def set_offset(self, addr):
        self.OFFSET = addr

    def set_fields(self, fields):
        self.FIELDS = fields

    def set_staged_fields(self, staged_fields):
        self.STAGED_FIELDS = staged_fields

    def get_value(self):
        self.FIELDS.sort(key=lambda field: field.get_start_bit())
        value = ''
        for field in self.FIELDS:
            value = field.get_bin_value() + str(value)
        if value == '':
            return None
        return hex(int(value, 2))

    def get_staged_value(self):
        self.STAGED_FIELDS.sort(key=lambda field: field.get_start_bit())
        value = ''
        for field in self.STAGED_FIELDS:
            value = field.get_bin_value() + value
        if value == '':
            return None
        return hex(int(value, 2))

    def get_field_value(self, field_name):
        for field in self.FIELDS:
            if (field.get_name() == field_name):
                return hex(field.get_value())
        return None

    def get_staged_field_value(self, field_name):
        for field in self.STAGED_FIELDS:
            if (field.get_name() == field_name):
                return hex(field.get_value())
        return None

    def is_initialized(self):
        return self.INITIALIZED

    def update_field(self, field_name, value):
        def get_bin(value):
            s = bin(value).split('b')[1]
            return s

        for field in self.STAGED_FIELDS:
            if (field.get_name() == field_name and field.get_num_bits() >= len(get_bin(value))):
                field.set_value(value)
                return hex(value) # signal successful update
        return None

    def update(self, value):
        # value should be a decimal int
        # value is actually a hex int

        def get_bin(value):
            s = bin(value).split('b')[1]
            return s

        if (len(get_bin(value)) > self.WIDTH_IN_BITS):
            return
        for field in self.STAGED_FIELDS:
            field_index = int(field.get_start_bit())
            field_value = ''
            for i in range(field.get_num_bits()):
                field_value = get_bin(value).zfill(self.WIDTH_IN_BITS)[-field_index-i-1] + field_value
            field.set_value(int(field_value, 2))
        return hex(value)

    def read(self):
        # Call driver API to get read data
        # Populate both FIELDS and STAGED_FIELDS with that data

        # If the read data is a C long, then you can convert to Python int
        # by using read_data = c_long(driver.read()).value

        def get_bin(value):
            s = bin(value).split('b')[1]
            while (len(s) < (DATA_WIDTH if DATA_WIDTH > self.WIDTH_IN_BITS else self.WIDTH_IN_BITS)):
                s = '0' + s
            return s
        read_data, read_err = driver.driver_read(self.ADDR, False)
        field_data = collections.OrderedDict()
        for field in self.FIELDS:
            print ("self.NAME = %s, field.get_num_bits = %d" % (self.NAME, field.get_num_bits()))
            field_index = int(field.get_start_bit())
            field_value = ''
            for i in range(field.get_num_bits()):
                field_value = get_bin(read_data)[-field_index-i-1] + field_value
            field.set_value(int(field_value, 2))
            field_data[field.get_name()] = hex(int(field_value, 2))
        self.STAGED_FIELDS = copy.deepcopy(self.FIELDS)
        self.INITIALIZED = True
        return hex(read_data), field_data

    def write(self):
        # Collect all of the values from the fields and pack them into one data
        write_data = ''
        for field in self.STAGED_FIELDS:
           field_value = field.get_bin_value()
           write_data = field_value + write_data
        # Call driver API to write
        driver.driver_write(self.ADDR, int(write_data, 2))

        return hex(int(write_data, 2)) # signal successful write

    def write_data(self, data):
        # Call driver API direclty since data is already provided
        # No need for packing
        # driver.write(data)
        pass

class REGBLK:
    def __init__(self, fn):
        self.OWNER = ''                                      # str
        self.BLKID = list()                                  # list
        self.BLKID_WIDTH = list()                            # list
        self.SLAVE_BLOCK_EXISTS = "YES"                      # str
        self.REGLIST = list()                                # list of REG
        self.IMPORT_LIST = list()                            # list of IMPORT
        self.REMOVE_STATION_ID_FOR_LOCAL_REQ = False         # get rid of station id when sending to local slave

        self.gen(fn)
        if type(self.BLKID_WIDTH[0]) == int:
            self.START = [x << (RING_ADDR_WIDTH - self.BLKID_WIDTH[0]) for x in self.BLKID]
        else:
            self.START = [x << (RING_ADDR_WIDTH - self.BLKID_WIDTH[0][0]) for x in self.BLKID[0]]
        self.offset_alloc()
        for reg in self.REGLIST:
            reg.ADDR = reg.OFFSET + self.START[0]
        self.RANGE = self.find_range()

    def print2python(self, indent, nindent):
        s = "%sREGBLK (\n" % (nindent * indent)
        for item in dir(self):
          if (item in ["REGLIST"]) or (callable(getattr(self, item))) or (item.startswith("__") and item.endswith("__")):
            continue
          else:
            s += "%s%s = %s,\n" % ((nindent + 1) * indent, item, printVal2Python(getattr(self, item)))
        s += "%sREGLIST = [\n" % ((nindent + 1) * indent)
        for reg in self.REGLIST:
          s += reg.print2python(indent, (nindent + 2))
        s += "%s]\n" % ((nindent + 1) *indent)
        s += "%s)\n" % (nindent * indent)
        return (s)

    def __repr__(self):
        s = 'OWNER: %s\n' % self.OWNER
        s += 'BLKID: %s\n' % self.BLKID
        s += 'BLKID_WIDTH: %d\n' % self.BLKID_WIDTH
        s += '==========\n'
        for reg in self.REGLIST:
            s += reg.__repr__()
        return s

    def get_owner(self):
        return self.OWNER

    def get_blkid(self, index):
        return self.BLKID[index]

    def get_blkid_list(self):
        return self.BLKID

    def get_blkid_length(self):
        return len(self.BLKID)

    def get_blkid_width(self):
        return self.BLKID_WIDTH

    def gen(self, fn):
        """Generates the register objects belonging to the block from a .csv file"""
        f = open(os.path.join(fn), 'r')
        reader = csv.reader(f)

        default_reg = REG()
        default_field = FIELD()
        cur_reg = None
        ls_keys = list()
        field_keys = list()

        ls_row = list()
        for row in reader:
            trow = list()
            for col in row:
                s = ""
                for c in col:
                    if c in string.printable:
                        s += c
                trow.append(s)
            ls_row.append(trow)

        is_metadata = True
        got_regkeys = False
        got_fieldkeys = False
        while (len(ls_row) > 0):
            row = ls_row.pop(0)
            if (len(row) == 0) or (row[0].startswith('#')):
                continue

            is_empty = True
            for cell in row:
                if (len(cell) > 0):
                    is_empty = False
                    break
            if (is_empty):
                continue
            if (is_metadata):
                if (row[0] == ''):
                    if (row[1] == 'FIELD_NAME'):
                        got_fieldkeys = True
                        for r in row[1:]:
                            entry = r.replace(" ", "").split('=')
                            if len(entry) == 1:
                                entry.append("")
                            if entry[0] == 'FIELD_NAME':
                                entry[0] = 'NAME'
                            field_keys.append(entry[0])
                            setattr(default_field, entry[0], entry[1])
                    else:
                        got_regkeys = True
                        for r in row[1:]:
                            ls_keys.append(r.replace(" ", "").split('=')[0])
                            if ls_keys[-1] in ["BASE_OFFSET", "DEPTH"]:
                                if r.replace(" ","").split('=')[1].find("h") >= 0:
                                    setattr(default_reg, ls_keys[-1], int(r.replace(" ","").split('=')[1][r.replace(" ","").split('=')[1].find("h") + 1:], 16))
                                else:
                                    setattr(default_reg, ls_keys[-1], int(r.replace(" ","").split('=')[1], 0))
                            else:
                                setattr(default_reg, r.replace(" ", "").split('=')[0], r.replace(" ", "").split('=')[1])

                    if got_fieldkeys == True and got_regkeys == True:
                        is_metadata = False
                else:
                    if row[0] in ["BLKID", "BLKID_WIDTH"]:
                        for i in range (1, len(row)):
                          if row[i] == '':
                            break
                          else:
                            if '|' in row[i]:
                              tmp = row[i].split('|')
                              for j in range (len(tmp)):
                                if tmp[j].find("h") >= 0:
                                  tmp[j] = int(tmp[j][tmp[j].find("h") + 1:], 16)
                                else:
                                  tmp[j] = int(tmp[j], 0)
                              if row[0] == "BLKID":
                                self.BLKID.append(tmp)
                              else:
                                self.BLKID_WIDTH.append(tmp)
                            else:
                              if row[i].find("h") >= 0:
                                if row[0] == "BLKID":
                                  self.BLKID.append(int(row[i][row[i].find("h") + 1:], 16))
                                else:
                                  self.BLKID_WIDTH.append(int(row[i][row[i].find("h") + 1:], 16))
                              else:
                                if row[0] == "BLKID":
                                  self.BLKID.append(int(row[i], 0))
                                else:
                                  self.BLKID_WIDTH.append(int(row[i], 0))
                    # elif row[0] in ["BLKID_WIDTH"]:
                    #     if row[1].find("h") >= 0:
                    #         setattr(self, row[0], int(row[1][row[1].find("h") + 1:], 16))
                    #     else:
                    #         setattr(self, row[0], int(row[1], 0))
                    elif row[0] in ["OWNER", "SLAVE_BLOCK_EXISTS"]:
                        setattr(self, row[0], row[1])
                    elif row[0] in ["REMOVE_STATION_ID_FOR_LOCAL_REQ"]:
                        if row[1] == "YES":
                            setattr(self, row[0], True)
                        else:
                            setattr(self, row[0], False)
                    elif row[0] in ["IMPORT_LIST"]:
                        for i in range (1, len(row)):
                            if row[i] == '':
                                break
                            else:
                                self.IMPORT_LIST.append(row[i])
                    else:
                        # Found the first register before getting both reg and field keys
                        is_metadata = False
                        ls_row.insert(0, row)
                continue
            else:
                if row[0] == '':
                    if cur_reg is not None and len(field_keys) > 0:
                        field = FIELD()
                        for i in range (len(field_keys)):
                            if row[i+1] == '':
                                setattr(field, field_keys[i], getattr(default_field, field_keys[i]))
                            else:
                                setattr(field, field_keys[i], row[i+1])
                        cur_reg.FIELDS.append(field)
                else:
                    reg = REG()
                    reg.NAME = row[0]
                    self.REGLIST.append(reg)
                    for i in range (len(ls_keys)):
                        if row[i+1] == '':
                            setattr(reg, ls_keys[i], getattr(default_reg, ls_keys[i]))
                        else:
                            if ls_keys[i] in ["BASE_OFFSET", "DEPTH"]:
                                if row[i+1].find("h") >= 0:
                                    setattr(reg, ls_keys[i], int(row[i+1][row[i+1].find("h") + 1:], 16))
                                else:
                                    setattr(reg, ls_keys[i], int(row[i+1], 0))
                            else:
                                setattr(reg, ls_keys[i], row[i+1])
                    if reg.WIDTH != "":
                        reg.WIDTH_IN_BITS = convert_width(reg.WIDTH)
                    # def get_rst_value(reg):
                    #     if reg.RESET_VALUE.find("h") >= 0:
                    #         reg.RESET_VALUE = int(reg.RESET_VALUE[reg.RESET_VALUE.find("h") + 1:], 16)
                    #     else:
                    #         reg.RESET_VALUE = int(reg.RESET_VALUE, 0)
                    # get_rst_value(reg)
                    # reg.set_fields([FIELD(reg.NAME, 0, reg.WIDTH_IN_BITS, "", reg.RESET_VALUE, [])])
                    cur_reg = reg

        # add regblk reference to reg
        for reg in self.REGLIST:
            reg.REGBLK = self

        # check reg width and field width
        for reg in self.REGLIST:
            if reg.WIDTH_IN_BITS == UNINITIALIZED:
                assert len(reg.FIELDS) > 0, "Register %s has no WIDTH info, neither has fields info" % (reg.NAME)
                reg.WIDTH_IN_BITS = 0
                for field in reg.FIELDS:
                    reg.WIDTH_IN_BITS += field.get_num_bits()
            elif len(reg.FIELDS) == 0:
                pass
            else:
                flen = 0
                for field in reg.FIELDS:
                    flen += field.get_num_bits()
                assert reg.WIDTH_IN_BITS == flen, "Register %s has WIDTH_IN_BITS = %d and flen = %d" % (reg.NAME, reg.WIDTH_IN_BITS, flen)

        # check for regs that have no fields
        for reg in self.REGLIST:
            if len(reg.FIELDS) == 0:
                field = FIELD()
                field.NAME = reg.NAME
                field.START_BIT = 0
                field.NUM_BITS = reg.WIDTH_IN_BITS
                reg.FIELDS.append(field)


    # def offset_alloc(self):
    #     """Allocates the addresses of each register in the block based on decreasing width"""
    #     self.REGLIST.sort(key=operator.attrgetter("WIDTH_IN_BITS"), reverse=True)
    #     self.REGLIST.sort(key=operator.attrgetter("BASE_OFFSET"), reverse=False)
    #     offset = 0
    #     for reg in self.REGLIST:
    #         if reg.BASE_OFFSET > offset:
    #             offset = reg.BASE_OFFSET
    #         reg.OFFSET = int(offset)
    #         if reg.WIDTH_IN_BITS < DATA_WIDTH:
    #             offset += int(DATA_WIDTH / 8) * reg.DEPTH
    #         else:
    #             offset += int(reg.WIDTH_IN_BITS / 8) * reg.DEPTH

    def offset_alloc(self):
        """Allocates the addresses of each register in the block based on decreasing width"""
        self.REGLIST.sort(key=operator.attrgetter("WIDTH_IN_BITS"), reverse=True)

        # Group all registers with the same base offset
        offset_groups = dict()
        offset_groups["0"] = dict()
        offset_groups["0"]["regs"] = list()
        offset_groups["0"]["size"] = 0

        for reg in self.REGLIST:
            if str(reg.BASE_OFFSET) not in offset_groups.keys():
                offset_groups[str(reg.BASE_OFFSET)] = dict()
                offset_groups[str(reg.BASE_OFFSET)]["regs"] = list()
                offset_groups[str(reg.BASE_OFFSET)]["size"] = 0

            offset_groups[str(reg.BASE_OFFSET)]["regs"].append(reg)
            if reg.WIDTH_IN_BITS < DATA_WIDTH:
                offset_groups[str(reg.BASE_OFFSET)]["size"] += int(DATA_WIDTH / 8) * reg.DEPTH
            else:
                offset_groups[str(reg.BASE_OFFSET)]["size"] += int(reg.WIDTH_IN_BITS / 8) * reg.DEPTH

        # Check that all groups fit within the base offset
        last_start_offset = 0
        last_size         = 0
        last_end_offset   = 0
        for base_offset in sorted([int(x) for x in offset_groups.keys()]):
            base_offset = str(base_offset)
            # Skip regs that have no base offset requested
            if base_offset == "-1":
                continue

            if last_size != 0 and last_end_offset > int(base_offset):
                print("ERROR: For regblk %s, offset for register group starting at offset %i with size %i encroaches on following group at offset %i" % (self.OWNER, last_start_offset, last_size, int(base_offset)))
                print("")
                print("       Offending registers at offset %i:" % (last_start_offset))
                print("         %-20s %s" % ("Name", "Size"))
                print("         -------------------------")
                for reg in offset_groups[str(last_start_offset)]["regs"]:
                    reg_size = 0
                    if reg.WIDTH_IN_BITS < DATA_WIDTH:
                        reg_size = int(DATA_WIDTH / 8) * reg.DEPTH
                    else:
                        reg_size = int(reg.WIDTH_IN_BITS / 8) * reg.DEPTH
                    print("         %-20s %i" % (reg.NAME, reg_size))
                print("")
                print("       Victim registers at offset %i:" % (int(base_offset)))
                print("         %-20s %s" % ("Name", "Size"))
                print("         -------------------------")
                for reg in offset_groups[str(base_offset)]["regs"]:
                    reg_size = 0
                    if reg.WIDTH_IN_BITS < DATA_WIDTH:
                        reg_size = int(DATA_WIDTH / 8) * reg.DEPTH
                    else:
                        reg_size = int(reg.WIDTH_IN_BITS / 8) * reg.DEPTH
                    print("         %-20s %i" % (reg.NAME, reg_size))
                print("")
                sys.exit()
            else:
                # Assign offsets for this group
                offset = int(last_start_offset)
                for reg in offset_groups[str(last_start_offset)]["regs"]:
                    reg.OFFSET = offset
                    if reg.WIDTH_IN_BITS < DATA_WIDTH:
                        offset += int(DATA_WIDTH / 8) * reg.DEPTH
                    else:
                        if (reg.WIDTH_IN_BITS % DATA_WIDTH == 0):
                            offset += int(reg.WIDTH_IN_BITS / 8) * reg.DEPTH
                        else:
                            offset += int((reg.WIDTH_IN_BITS + DATA_WIDTH - (reg.WIDTH_IN_BITS % DATA_WIDTH)) / 8) * reg.DEPTH

                # If there is room, try to fit in regs that have no base offset requested
                remaining_size = int(base_offset) - offset
                if "-1" in offset_groups.keys():
                    for reg in offset_groups["-1"]["regs"]:
                        reg_size = 0
                        if reg.WIDTH_IN_BITS < DATA_WIDTH:
                            reg_size = int(DATA_WIDTH / 8) * reg.DEPTH
                        else:
                            reg_size = int(reg.WIDTH_IN_BITS / 8) * reg.DEPTH
                    
                        if reg_size <= remaining_size:
                            reg.OFFSET = offset
                            offset += reg_size
                            offset_groups["-1"]["regs"].remove(reg)
                            remaining_size -= reg_size


            # Save data for next iteration's check
            last_start_offset = int(base_offset)
            last_size         = offset_groups[str(base_offset)]["size"]
            last_end_offset   = last_start_offset + last_size

        # Assign offsets for last group
        offset = int(last_start_offset)
        for reg in offset_groups[str(last_start_offset)]["regs"]:
            reg.OFFSET = offset
            if reg.WIDTH_IN_BITS < DATA_WIDTH:
                offset += int(DATA_WIDTH / 8) * reg.DEPTH
            else:
                if (reg.WIDTH_IN_BITS % DATA_WIDTH == 0):
                    offset += int(reg.WIDTH_IN_BITS / 8) * reg.DEPTH
                else:
                    offset += int((reg.WIDTH_IN_BITS + DATA_WIDTH - (reg.WIDTH_IN_BITS % DATA_WIDTH)) / 8) * reg.DEPTH
                
        # Add all remaining regs with no base offset requested to the end
        if "-1" in offset_groups.keys():
            for reg in offset_groups["-1"]["regs"]:
                reg.OFFSET = offset
                if reg.WIDTH_IN_BITS < DATA_WIDTH:
                    offset += int(DATA_WIDTH / 8) * reg.DEPTH
                else:
                    if (reg.WIDTH_IN_BITS % DATA_WIDTH == 0):
                        offset += int(reg.WIDTH_IN_BITS / 8) * reg.DEPTH
                    else:
                        offset += int((reg.WIDTH_IN_BITS + DATA_WIDTH - (reg.WIDTH_IN_BITS % DATA_WIDTH)) / 8) * reg.DEPTH
                
        # Check that the final offset is within the total memory size for every blkid width
        blkid_widths = list()
        for ref_id in self.BLKID_WIDTH:
            if type(ref_id) == int:
                blkid_widths.append(ref_id)
            elif type(ref_id) == list:
                for blkid in ref_id:
                    blkid_widths.append(blkid)

        #total_size = 2 ** (RING_ADDR_WIDTH - self.BLKID_WIDTH[0][0])
        for blkid_width in blkid_widths:
            total_size = 2 ** (RING_ADDR_WIDTH - blkid_width)
            if (offset > total_size):
                # Get all offending registers
                self.REGLIST.sort(key=operator.attrgetter("OFFSET"), reverse=False)
                print("ERROR: For regblk %s, registers up to offset %i surpasses total size %i\n" % (self.OWNER, offset, total_size))
                print("")
                print("       Offending registers:")
                print("         %-20s %-10s %s" % ("Name", "Offset", "Size"))
                print("         ------------------------------------")
                for reg in self.REGLIST:
                    reg_size = 0
                    if reg.WIDTH_IN_BITS < DATA_WIDTH:
                        reg_size = int(DATA_WIDTH / 8) * reg.DEPTH
                    else:
                        reg_size = int(reg.WIDTH_IN_BITS / 8) * reg.DEPTH
                    if (reg.OFFSET + reg_size) > total_size:
                        print("         %-20s %-10i %i" % (reg.NAME, reg.OFFSET, reg_size))
                print("")
                sys.exit()

    def find_range(self):
        range = 0
        for reg in self.REGLIST:
            range += reg.get_width_in_bits()
        return range

    def get_reglist(self):
        return self.REGLIST

    def get_importlist(self):
        return self.IMPORT_LIST

class MEMORY:
    def __init__(self, start, range):
        self.START = start
        self.RANGE = range
        self.targets = MEM_TARGET_LIST

    def read(self, addr, target=MEM_DEFAULT_TARGET):
        # Call driver API
        # Read size: 64 bits
        return 'Reading from ' + hex(addr) + ' (' + target + ')' # temporary

    def write(self, addr, data, target=MEM_DEFAULT_TARGET):
        # Call driver API
        # Write size: 64 bits
        return 'Writing ' + hex(data) + 'to ' + hex(addr) + ' (' + target + ')' # temporary

    def get_start(self):
        return self.START

    def get_range(self):
        return self.RANGE

    def get_targets(self):
        return self.targets

class DEVICE:
    def __init__(self, reg_file_list):
        self.REGBLK_LIST = list()
        for file in reg_file_list:
            self.REGBLK_LIST.append(REGBLK(file))

        start = 0
        range = 8*10**9
        self.MEMORY = MEMORY(start, range)

        self.REG_LIST = list()
        for regblk in self.REGBLK_LIST:
            for reg in regblk.get_reglist():
                self.REG_LIST.append(reg)

    def get_regblk_list(self):
        return self.REGBLK_LIST

    def get_reg_list(self):
        return self.REG_LIST

    def get_memory(self):
        return self.MEMORY

    def get_reg_name(self, name):
        fil = list(filter(lambda reg: reg.get_name() == name, self.REG_LIST))
        if (len(fil) > 0):
            return fil[0]
        return None

    def get_reg_addr(self, addr):
        fil = list(filter(lambda reg: reg.get_address() == addr, self.REG_LIST))
        if (len(fil) > 0):
            return fil[0]
        return None
