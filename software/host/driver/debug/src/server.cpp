#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/time.h>
#include <memory.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <stdarg.h>
#include <unistd.h>

#define PORT 2000
#define COMMAND_SIZE 6
bool setup(int& sock) {
    struct sockaddr_in address;
    struct sockaddr_in serverAddress;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return false;
    }

    memset(&serverAddress, '0', sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(PORT);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if(inet_pton(AF_INET, "127.0.0.1", &serverAddress.sin_addr)<=0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return false;
    }

    // Connect to python
    if (connect(sock, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0)
    {
        printf("\nConnection Failed \n");
        return false;
    }
    return true;
}


void driver_read(int sock, char address[]) {
	int retBytes = 0;

    if (retBytes = recv(sock, address, 12, 0)==-1) {
        printf("Error cannot recieve address\n");
    }
    printf("Address %s\n", address);
   	send(sock, address, strlen(address),0);
    sleep(1);
}

void driver_write(int sock, char address[], char  data[]) {
		int retBytes = 0;
    char const *response = "";

		printf("Executing function write. \n");

		if (retBytes = recv(sock, address, 1, 0)==-1) {
				printf("Error cannot recieve address\n");
    }
		int ad = atoi(address);
		printf("Address: %d\n", ad);

		if (retBytes = recv(sock, data, 1, 0)==-1) {
        printf("Error cannot recieve address\n");
    }

    int da = atoi(data);
		printf("Data: %d\n", da);
		response = "write";
		send(sock, response, strlen(response),0);
}

void driver_status(int sock) {
   	printf("Executing function status. \n");
    char const *response = "";

    response = "status";
   	send(sock, response, strlen(response),0);
}

int main(int argc, char const *argv[]) {
	int sock = 0;
	if (!setup(sock)){
		printf("Error, cannot setup\n");
		return -1;
	}

    char const *status = "Ready";
    char *command = new char[1];
    char *data = new char [1];
    char *address = new char[12];

    int retBytes = 0;

    while (true) {
        // Send notification to python
        printf("Sending statu\n");
        if (retBytes = send(sock , status , strlen(status) , 0 )==-1) {
            printf("Error, cannot send status\n");
            break;
        }
        printf("Waiting for command\n");
        // Get command
        if (retBytes = recv(sock, command, 1, 0)==-1) {
            printf("Error cannot recieve command\n");
            break;
        }
        printf("Got command\n");
        if (strcmp(command, "r") == 0) {
            printf("Reading from register\n");
            driver_read(sock,address);
        	command[0] = '\0';
        }

        if (strcmp(command, "w") == 0) {
        	printf("Writing to register\n");
            driver_write(sock,address,data);
        	command[0] = '\0';
        }

        if (strcmp(command, "s") == 0) {
        		driver_status(sock);
        		command[0] = '\0';
        }
    }
    // Finished doing stuff, tell server that we're done
    status = "Done";
    printf("Finished grabbing data, ending connection. \n");
    send(sock, status, strlen(status),0);
    free(command);
    free(data);
    free(address);
    close(sock);
    return 0;
}