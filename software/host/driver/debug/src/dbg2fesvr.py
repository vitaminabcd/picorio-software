#!/usr/bin/env python
# -*- coding: utf-8 -*-
import io
import socket
import struct
import time
import sys
from multiprocessing import Process, Queue
import os
PORT = 2001
cmd_queue = Queue()
response_queue = Queue()

class DBG2FESVR:

        def __init__(self):
            self.self_socket = ""
            self.client_socket = ""

        def setup(self):
            # Set up the socket on the python side
            self_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self_socket.bind(("localhost", PORT))
            self_socket.listen(5)
            c_socket, address = self_socket.accept()
            self.client_socket = c_socket
            self.self_socket = self_socket

        def driver_read(self, address, retry):
            # put into to queue 
            inputs = ["r", 8, address]
            cmd_queue.put(inputs)
            # wait for response queue to be non-empty
            while 1:
                if response_queue.empty() == False:
                    read_data = response_queue.get()
                    read_err = response_queue.get()
                    return read_data, read_err

        def driver_write(self, address, value):
            inputs = ["w", 8, address, value]
            cmd_queue.put(inputs)
            # wait for response queue to be non-empty
            while 1:
                if response_queue.empty() == False:
                    response = response_queue.get()
                    return response

        def execute_read(self, command, size, address):
            # Send data
            self.client_socket.send(command.encode())
            self.client_socket.send(str(size).encode())
            self.client_socket.send(str("%010x" % (address)).encode())


        def execute_write(self, command, size, address, value):
            # Send data
            self.client_socket.send(command.encode())
            self.client_socket.send(str(size).encode())
            self.client_socket.send(str("%010x" % (address)).encode())
            self.client_socket.send(str("%016x" % (value)).encode())

        
        def wait_for_response(self, command):
            while 1:
                response = self.client_socket.recv(1024).decode()
                if len(response)>0:
                    if command == "r":
                        response_queue.put(int(response,16))
                        response_queue.put(int(response,16))
                        break
                    if command == "w":
                        response_queue.put(response)
                        break

        def execute_command(self):
            while 1:
                status = self.client_socket.recv(1024).decode()
                if status == "Ready" :
                    while 1:
                        if cmd_queue.empty() == False:
                            next_set = cmd_queue.get()
                            command = next_set[0]
                            if (command == "r"):
                                self.execute_read(command, next_set[1], next_set[2])
                                self.wait_for_response(command)
                            if (command == "w"):
                                self.execute_write(command, next_set[1], next_set[2], next_set[3])
                                self.wait_for_response(command)
                            break
                if status == "Done":
                    break
            # Close the sockets
            self.self_socket.close()
            self.client_socket.close()
