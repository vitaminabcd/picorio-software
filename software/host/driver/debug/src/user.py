#!/usr/bin/env python3

import os
from dbg_defines import *

class User:
    def __init__(self, dev):
        self.device = dev
        self.regblk_list = dev.get_regblk_list()
        self.memory = dev.get_memory()
        self.reg_list = list()
        for regblk in self.regblk_list:
            for reg in regblk.get_reglist():
                self.reg_list.append(reg)

    def get_reg_name(self, name):
        return self.device.get_reg_name(name)

    def read_reg_name(self, name):
        reg = self.get_reg_name(name)
        if (reg != None):
            data, field_data = reg.read()
            return data, field_data
        return None

    def write_reg_name(self, name):
        reg = self.get_reg_name(name)
        if (reg != None and self.is_reg_initialized(reg)):
            data = reg.write()
            return data
        return None

    def update_reg(self, name, value):
        reg = self.get_reg_name(name)
        if (reg != None and self.is_reg_initialized(reg)):
            data = reg.update(value)
            return data
        return None

    def update_reg_field(self, reg_name, field_name, value):
        reg = self.get_reg_name(reg_name)
        if (reg != None and self.is_reg_initialized(reg)):
            data = reg.update_field(field_name, value)
            return data
        return None

    def show_reg(self, name):
        reg = self.get_reg_name(name)
        if (reg != None and self.is_reg_initialized(reg)):
            data = reg.get_value()
            staged_data = reg.get_staged_value()
            return data, staged_data
        return None, None

    def show_reg_field(self, reg_name, field_name):
        reg = self.get_reg_name(reg_name)
        if (reg != None and self.is_reg_initialized(reg)):
            data = reg.get_field_value(field_name)
            staged_data = reg.get_staged_field_value(field_name)
            return data, staged_data
        return None, None

    def get_reg_addr(self, addr):
        return self.device.get_reg_addr(addr)

    def read_reg_addr(self, addr):
        reg = self.get_reg_addr(addr)
        if (reg != None):
            data, field_data = reg.read()
            return data, field_data
        return None

    def is_valid_mem_addr(self, addr):
        start = self.memory.get_start()
        range = self.memory.get_range()
        return start <= addr and addr < start + range

    def is_valid_target(self, target):
        return target in self.memory.get_targets()

    def read_mem_addr(self, addr, target=MEM_DEFAULT_TARGET):
        if (self.is_valid_mem_addr(addr) and self.is_valid_target(target)):
            return self.memory.read(addr, target)
        return None

    def read_mem_range(self, start, rng, target=MEM_DEFAULT_TARGET):
        if start < self.memory.get_start():
            start = self.memory.get_start()
        if start + rng > self.memory.get_start() + self.memory.get_range():
            rng = self.memory.get_range()
        data_list = list()
        for i in range(start, start+rng, DATA_WIDTH):
                data_list.append(self.memory.read(i, target))
        s = ''
        for data in data_list:
            s += data
            if (data != data_list[-1]):
                s += '\n'
        if (s != ''):
            return s
        return None

    def get_register_names(self):
        name_list = list()
        for reg in self.reg_list:
            name_list.append(reg.get_name())
        return name_list

    def get_reg_field_names(self, reg):
        field_list = list()
        for field in reg.get_fields():
            field_list.append(field.get_name())
        return field_list

    def get_register_info(self):
        dc = dict()
        for reg in self.reg_list:
            dc[reg.get_name()] = reg.get_address()
        margin = str(len(max(dc.keys(), key=len)))

        s = 'REGISTERS LIST\n'
        s += '===============' + '\n'
        s += ('%-' + margin + 's %s') % ('NAME','ADDRESS') + '\n'
        for addr in sorted(dc.values()):
            s += ('%-' + margin + 's 0x%x') % (list(dc.keys())[list(dc.values()).index(addr)],addr) + '\n'
        return s

    def get_field_info(self, name):
        reg = self.get_reg_name(name)
        if (reg != None):
            s = reg.__repr__()
            return s
        return None

    def get_memory_info(self):
        s = 'MEMORY \n'
        s += '===============' + '\n'
        s += 'START: ' + hex(self.memory.get_start()) + '\n'
        s += 'RANGE: ' + hex(self.memory.get_range())
        return s

    def is_reg_initialized(self, reg):
        if (reg.is_initialized() == False):
            print('Register ' + reg.get_name() + ' must be read first!')
            return False
        else:
            return True

    def gen_station_rtl(self, proj_macro, opath="./gen_rtl"):
        if not os.path.isdir(opath):
            os.mkdir(opath)

        #fpy = open(opath + "/reginfo.py", "w")
        #fpy.write("reginfo = [\n")
        for regblk in self.regblk_list:
            # Check that all registers in this block are in the target location
            check_pass = True
            for reg in regblk.get_reglist():
                if not reg.check_permission and reg.get_location() not in [STATION, BLK]:
                    check_pass = False
            if not check_pass:
                print('Regblk %s skipped' % regblk.get_owner())
                continue

            print('Generating regblk %s' % regblk.get_owner())
            ls_station_rw = list()
            ls_station_ro = list()
            ls_station_wo = list()
            ls_station_hw = list()
            # Alloc Address
            #offset_alloc(regblk)
            #fpy.write("  station_%s =\n%s" % (regblk.OWNER.lower(), regblk.print2python("  ", 2)))
            blkname = ("station_" + regblk.OWNER).lower()
            f = open(opath + "/" + blkname + ".sv", "w")
            fc = open(opath + "/" + blkname + ".h", "w")
            fcaddrq = list()
            # Header
            f.write(company_header())
            f.write(autogen_header())
            # ifndef
            f.write("`ifdef %s\n" % (proj_macro))
            f.write("`ifndef %s\n" % (blkname + "_PKG__sv").upper())
            f.write("`define %s\n" % (blkname + "_PKG__sv").upper())
            # Gen Pkg
            f.write("package %s_pkg;\n" % blkname)
            f.write("  import pygmy_cfg::*;\n")
            f.write("  import pygmy_typedef::*;\n")
            f.write("  import pygmy_intf_typedef::*;\n")
            if len(regblk.IMPORT_LIST) > 0:
              for imp in regblk.IMPORT_LIST:
                f.write("  import %s::*;\n" % imp)
            f.write("  localparam int %s_RING_ADDR_WIDTH = 'h%x;\n" % (blkname.upper(), RING_ADDR_WIDTH))
            f.write("  localparam int %s_DATA_WIDTH = 'h%x;\n" % (blkname.upper(), DATA_WIDTH))
            f.write("  localparam [%s_RING_ADDR_WIDTH-1:0] %s_MAX_RING_ADDR = 'h%x;\n" % (blkname.upper(), blkname.upper(), MAX_RING_ADDR))
            if len(regblk.BLKID) == 1:
              if type(regblk.BLKID[0]) == int:
                f.write("  localparam int %s_BLKID = 'h%x;\n" % (blkname.upper(), regblk.BLKID[0]))
                f.write("  localparam int %s_BLKID_WIDTH = 'h%x;\n" % (blkname.upper(), regblk.BLKID_WIDTH[0]))
                f.write("  localparam bit [%d - 1:0] %s_BASE_ADDR = 'h%x;\n" % (RING_ADDR_WIDTH, blkname.upper(), regblk.BLKID[0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0])))
              elif type(regblk.BLKID[0]) == list:
                for i in range (0, len(regblk.BLKID[0])):
                  f.write("  localparam int %s_BLKID_SUB_%d = 'h%x;\n" % (blkname.upper(), i, regblk.BLKID[0][i]))
                  f.write("  localparam int %s_BLKID_WIDTH_SUB_%d = 'h%x;\n" % (blkname.upper(), i, regblk.BLKID_WIDTH[0][i]))
            else:
              for i in range (0, len(regblk.BLKID)):
                if type(regblk.BLKID[i]) == int:
                  f.write("  localparam int %s_BLKID_%0d = 'h%x;\n" % (blkname.upper(), i, regblk.BLKID[i]))
                  f.write("  localparam bit [%d - 1:0] %s_BASE_ADDR_%0d = 'h%x;\n" % (RING_ADDR_WIDTH, blkname.upper(), i, regblk.BLKID[i] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0])))
                elif type(regblk.BLKID[i]) == list:
                  for j in range (0, len(regblk.BLKID[i])):
                    f.write("  localparam int %s_BLKID_%0d_SUB_%0d = 'h%x;\n" % (blkname.upper(), i, j, regblk.BLKID[i][j]))
                    f.write("  localparam int %s_BLKID_WIDTH_%0d_SUB_%0d = 'h%x;\n" % (blkname.upper(), i, j, regblk.BLKID_WIDTH[i][j]))
                  f.write("  localparam bit [%d - 1:0] %s_BASE_ADDR_%0d = 'h%x;\n" % (RING_ADDR_WIDTH, blkname.upper(), i, regblk.BLKID[i][0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[i][0])))
              f.write("  localparam int %s_BLKID_WIDTH = 'h%x;\n" % (blkname.upper(), regblk.BLKID_WIDTH[0]))

            for reg in regblk.REGLIST:
              if reg.DEPTH == 1:
                f.write("  localparam bit [%d - 1:0] %s_%s_OFFSET = 64'h%x;\n" % (RING_ADDR_WIDTH, blkname.upper(), reg.NAME.upper(), reg.OFFSET))
              else:
                for d in range(reg.DEPTH):
                  f.write("  localparam bit [%d - 1:0] %s_%s_OFFSET__DEPTH_%d = 64'h%x;\n" % (RING_ADDR_WIDTH, blkname.upper(), reg.NAME.upper(), d, reg.OFFSET + d * int(DATA_WIDTH/8 if reg.WIDTH_IN_BITS < DATA_WIDTH else reg.WIDTH_IN_BITS/8)))
              if reg.TYPE == LOGIC:
                f.write("  localparam int %s_%s_WIDTH  = %d;\n" % (blkname.upper(), reg.NAME.upper(), reg.WIDTH_IN_BITS))
              else:
                f.write("  localparam int %s_%s_WIDTH  = $bits(%s);\n" % (blkname.upper(), reg.NAME.upper(), reg.TYPE))
              f.write("  localparam bit [%d - 1:0] %s_%s_RSTVAL = %s;\n" % (DATA_WIDTH, blkname.upper(), reg.NAME.upper(), reg.RESET_VALUE))
              if len(regblk.BLKID) == 1:
                if type(regblk.BLKID[0]) == int:
                  if reg.DEPTH == 1:
                    f.write("  localparam bit [%d - 1:0] %s_%s_ADDR = 64'h%x;\n" % (RING_ADDR_WIDTH, blkname.upper(), reg.NAME.upper(), (regblk.BLKID[0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0])) + reg.OFFSET))
                  else:
                    for d in range(reg.DEPTH):
                      f.write("  localparam bit [%d - 1:0] %s_%s_ADDR__DEPTH_%0d = 64'h%x;\n" % (RING_ADDR_WIDTH, blkname.upper(), reg.NAME.upper(), d, (regblk.BLKID[0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0])) + reg.OFFSET + d * int(DATA_WIDTH/8 if reg.WIDTH_IN_BITS < DATA_WIDTH else reg.WIDTH_IN_BITS/8)))
                elif type(regblk.BLKID[0]) == list:
                  f.write("  localparam bit [%d - 1:0] %s_%s_ADDR = 64'h%x;\n" % (RING_ADDR_WIDTH, blkname.upper(), reg.NAME.upper(), (regblk.BLKID[0][0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0][0])) + reg.OFFSET))
              else:
                for i in range (0, len(regblk.BLKID)):
                  if type(regblk.BLKID[i]) == int:
                    if reg.DEPTH == 1:
                      f.write("  localparam bit [%d - 1:0] %s_%s_ADDR_%0d = 64'h%x;\n" % (RING_ADDR_WIDTH, blkname.upper(), reg.NAME.upper(), i, (regblk.BLKID[i] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0])) + reg.OFFSET))
                    else:
                      for d in range(reg.DEPTH):
                        f.write("  localparam bit [%d - 1:0] %s_%s_ADDR_%0d__DEPTH_%0d = 64'h%x;\n" % (RING_ADDR_WIDTH, blkname.upper(), reg.NAME.upper(), i, d, (regblk.BLKID[i] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0])) + reg.OFFSET + d * int(DATA_WIDTH/8 if reg.WIDTH_IN_BITS < DATA_WIDTH else reg.WIDTH_IN_BITS/8)))
                  elif type(regblk.BLKID[i]) == list:
                    f.write("  localparam bit [%d - 1:0] %s_%s_ADDR_%0d = 64'h%x;\n" % (RING_ADDR_WIDTH, blkname.upper(), reg.NAME.upper(), i, (regblk.BLKID[i][0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[i][0])) + reg.OFFSET))

            #f.write("  `ifndef SYNTHESIS\n")
        #     f.write("  typedef enum bit [64 - 1:0] {\n")
        #     for reg in regblk.REGLIST[:-1]:
        #       f.write("    ENUM_%s_%s_OFFSET = 64'h%x,\n" % (blkname.upper(), reg.NAME.upper(), reg.OFFSET))
        #     f.write("    ENUM_%s_%s_OFFSET = 64'h%x\n  } %s_offset_e;\n" % (blkname.upper(), regblk.REGLIST[-1].NAME.upper(), regblk.REGLIST[-1].OFFSET, blkname))
        #     if len(regblk.BLKID) == 1:
        #       f.write("  typedef enum bit [64 - 1:0] {\n")
        #       for reg in regblk.REGLIST[:-1]:
        #         f.write("    ENUM_%s_%s_ADDR = 64'h%x,\n" % (blkname.upper(), reg.NAME.upper(), (regblk.BLKID[0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH)) + reg.OFFSET))
        #       f.write("    ENUM_%s_%s_ADDR = 64'h%x\n  } %s_addr_e;\n" % (blkname.upper(), regblk.REGLIST[-1].NAME.upper(), (regblk.BLKID[0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH)) + regblk.REGLIST[-1].OFFSET, blkname))
        #     else:
        #       f.write("  typedef enum bit [64 - 1:0] {\n")
        #       for reg in regblk.REGLIST[:-1]:
        #         for i in range (0, len(regblk.BLKID)):
        #           f.write("    ENUM_%s_%s_ADDR_%0d = 64'h%x,\n" % (blkname.upper(), reg.NAME.upper(), i, (regblk.BLKID[i] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH)) + reg.OFFSET))
        #       for i in range (0, len(regblk.BLKID) - 1):
        #         f.write("    ENUM_%s_%s_ADDR_%0d = 64'h%x,\n" % (blkname.upper(), regblk.REGLIST[-1].NAME.upper(), i, (regblk.BLKID[i] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH)) + regblk.REGLIST[-1].OFFSET))
        #       f.write("    ENUM_%s_%s_ADDR_%0d = 64'h%x\n  } %s_addr_e;\n" % (blkname.upper(), regblk.REGLIST[-1].NAME.upper(), len(regblk.BLKID) - 1, (regblk.BLKID[-1] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH)) + regblk.REGLIST[-1].OFFSET, blkname))

            # C Code Header file
            fc.write("#ifndef __%s_H\n" % (blkname.upper()))
            fc.write("#define __%s_H\n" % (blkname.upper()))
            if len(regblk.BLKID) == 1:
              if type(regblk.BLKID[0]) == int:
                fc.write("#define %s_BASE_ADDR 0x%08x\n" % (blkname.upper(), regblk.BLKID[0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0])))
              elif type(regblk.BLKID[0]) == list:
                fc.write("#define %s_BASE_ADDR 0x%08x\n" % (blkname.upper(), regblk.BLKID[0][0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0][0])))
            else:
              for i in range (0, len(regblk.BLKID)):
                if type(regblk.BLKID[i]) == int:
                  fc.write("#define %s_BASE_ADDR_%0d 0x%08x\n" % (blkname.upper(), i, regblk.BLKID[i] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0])))
                elif type(regblk.BLKID[i]) == list:
                  fc.write("#define %s_BASE_ADDR_%0d 0x%08x\n" % (blkname.upper(), i, regblk.BLKID[i][0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[i][0])))

            for reg in regblk.REGLIST:
              fc.write("#define %s_%s_WIDTH %d\n" % (blkname.upper(), reg.NAME.upper(), reg.WIDTH_IN_BITS))
              if (reg.WIDTH_IN_BITS <= 8):
                fc.write("#define %s_%s_WIDTH_IN_BYTE %d\n" % (blkname.upper(), reg.NAME.upper(), 1))
              else:
                fc.write("#define %s_%s_WIDTH_IN_BYTE %d\n" % (blkname.upper(), reg.NAME.upper(), reg.WIDTH_IN_BITS/8))
              #fc.write("#define %s_%s_RSTVAL %s\n" % (blkname.upper(), reg.NAME.upper(), reg.RESET_VALUE))
              if len(regblk.BLKID) == 1:
                if type(regblk.BLKID[0]) == int:
                  if reg.DEPTH == 1:
                    fc.write("#define %s_%s_OFFSET 0x%08x\n" % (blkname.upper(), reg.NAME.upper(), reg.OFFSET))
                    fc.write("#define %s_%s_ADDR 0x%08x\n" % (blkname.upper(), reg.NAME.upper(), (regblk.BLKID[0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0])) + reg.OFFSET))
                    fcaddrq.append("%s_%s_ADDR_ENUM = 0x%08x" % (blkname.upper(), reg.NAME.upper(), (regblk.BLKID[0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0])) + reg.OFFSET))
                  else:
                    for d in range(reg.DEPTH):
                      fc.write("#define %s_%s_OFFSET__DEPTH_%0d 0x%08x\n" % (blkname.upper(), reg.NAME.upper(), d, reg.OFFSET + d * int(DATA_WIDTH/8 if reg.WIDTH_IN_BITS < DATA_WIDTH else reg.WIDTH_IN_BITS/8)))
                      fc.write("#define %s_%s_ADDR__DEPTH_%0d 0x%08x\n" % (blkname.upper(), reg.NAME.upper(), d, (regblk.BLKID[0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0])) + reg.OFFSET + d * int(DATA_WIDTH/8 if reg.WIDTH_IN_BITS < DATA_WIDTH else reg.WIDTH_IN_BITS/8)))
                      fcaddrq.append("%s_%s_ADDR__DEPTH_%0d_ENUM = 0x%08x" % (blkname.upper(), reg.NAME.upper(), d, (regblk.BLKID[0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0])) + reg.OFFSET + d * int(DATA_WIDTH/8 if reg.WIDTH_IN_BITS < DATA_WIDTH else reg.WIDTH_IN_BITS/8)))
                elif type(regblk.BLKID[0]) == list:
                  fc.write("#define %s_%s_OFFSET 0x%08x\n" % (blkname.upper(), reg.NAME.upper(), reg.OFFSET))
                  fc.write("#define %s_%s_ADDR 0x%08x\n" % (blkname.upper(), reg.NAME.upper(), (regblk.BLKID[0][0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0][0])) + reg.OFFSET))
                  fcaddrq.append("%s_%s_ADDR_ENUM = 0x%08x" % (blkname.upper(), reg.NAME.upper(), (regblk.BLKID[0][0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0][0])) + reg.OFFSET))
              else:
                if type(regblk.BLKID[0]) == int:
                  if reg.DEPTH == 1:
                    fc.write("#define %s_%s_OFFSET 0x%08x\n" % (blkname.upper(), reg.NAME.upper(), reg.OFFSET))
                  else:
                    for d in range(reg.DEPTH):
                      fc.write("#define %s_%s_OFFSET__DEPTH_%0d 0x%08x\n" % (blkname.upper(), reg.NAME.upper(), d, reg.OFFSET + d * int(DATA_WIDTH/8 if reg.WIDTH_IN_BITS < DATA_WIDTH else reg.WIDTH_IN_BITS/8)))
                else:
                  #TODO: Not sure what to do yet
                  pass

                # TODO: Depth
                for i in range (0, len(regblk.BLKID)):
                  if type(regblk.BLKID[i]) == int:
                    if reg.DEPTH == 1:
                      fc.write("#define %s_%s_ADDR_%0d 0x%08x\n" % (blkname.upper(), reg.NAME.upper(), i, (regblk.BLKID[i] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0])) + reg.OFFSET))
                      fcaddrq.append("%s_%s_ADDR_%0d_ENUM = 0x%08x" % (blkname.upper(), reg.NAME.upper(), i, (regblk.BLKID[i] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0])) + reg.OFFSET))
                    else:
                      for d in range(reg.DEPTH):
                        fc.write("#define %s_%s_ADDR_%0d__DEPTH_%0d 0x%08x\n" % (blkname.upper(), reg.NAME.upper(), i, d, (regblk.BLKID[i] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0])) + reg.OFFSET + d * int(DATA_WIDTH/8 if reg.WIDTH_IN_BITS < DATA_WIDTH else reg.WIDTH_IN_BITS/8)))
                        fcaddrq.append("%s_%s_ADDR_%0d__DEPTH_%0d_ENUM = 0x%08x" % (blkname.upper(), reg.NAME.upper(), i, d, (regblk.BLKID[i] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[0])) + reg.OFFSET + d * int(DATA_WIDTH/8 if reg.WIDTH_IN_BITS < DATA_WIDTH else reg.WIDTH_IN_BITS/8)))
                  elif type(regblk.BLKID[i]) == list:
                    fc.write("#define %s_%s_ADDR_%0d 0x%08x\n" % (blkname.upper(), reg.NAME.upper(), i, (regblk.BLKID[i][0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[i][0])) + reg.OFFSET))
                    fcaddrq.append("%s_%s_ADDR_%0d_ENUM = 0x%08x" % (blkname.upper(), reg.NAME.upper(), i, (regblk.BLKID[i][0] << (RING_ADDR_WIDTH - regblk.BLKID_WIDTH[i][0])) + reg.OFFSET))

            # c enum
            fc.write("\n\n#ifdef USE_STATION_ENUM_ARRAY\n")
            fc.write("enum %s_addr_enum {\n" % (blkname.lower()))
            for i in range (0, len(fcaddrq)-1):
              fc.write("  %s,\n" % (fcaddrq[i].lower()))
            fc.write("  %s};\n" % (fcaddrq[-1].lower()))

            # c address arrays
            fc.write("static char %s_addr_name_array[%d][128] = {\n" % (blkname.lower(), len(fcaddrq)))
            for i in range (0, len(fcaddrq)-1):
              fc.write("  \"%s\",\n" % (fcaddrq[i].split(" = ")[0]))
            fc.write("  \"%s\"};\n" % (fcaddrq[-1].split(" = ")[0]))

            fc.write("static uint64_t %s_addr_array[%d] = {\n" % (blkname.lower(), len(fcaddrq)))
            for i in range (0, len(fcaddrq)-1):
              fc.write("  %s,\n" % (fcaddrq[i].split(" = ")[1]))
            fc.write("  %s};\n" % (fcaddrq[-1].split(" = ")[1]))

            fc.write("#endif\n\n")
            fc.write("#endif\n")
            fc.close()

            #f.write("  `endif\n")
            f.write("endpackage\n")
            f.write("`endif\n")
            # Group All Registers into different types
            #analyze_reg(regblk.REGLIST)
            for r in regblk.REGLIST:
                if (r.LOCATION, r.SW_ACCESS_PERMISSION) == (STATION, RW):
                    ls_station_rw.append(r)
                elif (r.LOCATION, r.SW_ACCESS_PERMISSION) == (STATION, RO):
                    ls_station_ro.append(r)
                elif (r.LOCATION, r.SW_ACCESS_PERMISSION) == (STATION, WO):
                    ls_station_wo.append(r)
                if (r.LOCATION, r.HW_WRITABLE) == (STATION, WR_YES):
                    ls_station_hw.append(r)
            has_station = (len(ls_station_rw) + len(ls_station_ro) + len(ls_station_wo) > 0)
            #for reg in regblk.REGLIST:
            #  print reg
            if not has_station:
              continue
            f.write("`ifndef %s\n" % (blkname + "__sv").upper())
            f.write("`define %s\n" % (blkname + "__sv").upper())
            # Gen Module
            f.write("module %s\n" % blkname)
            f.write("  import pygmy_cfg::*;\n")
            f.write("  import pygmy_typedef::*;\n")
            f.write("  import pygmy_intf_typedef::*;\n")
            f.write("  import %s_pkg::*;\n" % blkname)
            if len(regblk.IMPORT_LIST) > 0:
              for imp in regblk.IMPORT_LIST:
                f.write("  import %s::*;\n" % imp)
            if len(regblk.BLKID) == 1:
              f.write("""  (\n""")
            else:
              f.write("""  # ( parameter int BLOCK_INST_ID = 0 ) (\n""")
            for reg in ls_station_rw + ls_station_ro + ls_station_wo:
              if reg.TYPE == LOGIC:
                if reg.DEPTH == 1:
                  if reg.WIDTH_IN_BITS == 1:
                    f.write("  output out_%s,\n" % (reg.NAME))
                  else:
                    f.write("  output [%s_%s_WIDTH - 1 : 0] out_%s,\n" % (blkname.upper(), reg.NAME.upper(), reg.NAME))
                else:
                  if reg.WIDTH_IN_BITS == 1:
                    f.write("  output [%d - 1 : 0] out_%s,\n" % (reg.DEPTH, reg.NAME))
                  else:
                    f.write("  output [%d - 1 : 0][%s_%s_WIDTH - 1 : 0] out_%s,\n" % (reg.DEPTH, blkname.upper(), reg.NAME.upper(), reg.NAME))
              else:
                if reg.DEPTH == 1:
                  f.write("  output %s out_%s,\n" % (reg.TYPE, reg.NAME))
                else:
                  f.write("  output %s [%d - 1 : 0] out_%s,\n" % (reg.TYPE, reg.DEPTH, reg.NAME))
            for reg in ls_station_hw:
              if reg.DEPTH == 1:
                if reg.BLOCK_WRITE_ENABLE_INFERRED == "NO":
                  f.write("  input logic vld_in_%s,\n" % (reg.NAME))
                if reg.TYPE == LOGIC:
                  if reg.WIDTH_IN_BITS == 1:
                    f.write("  input in_%s,\n" % (reg.NAME))
                  else:
                    f.write("  input [%s_%s_WIDTH - 1 : 0] in_%s,\n" % (blkname.upper(), reg.NAME.upper(), reg.NAME))
                else:
                  f.write("  input %s in_%s,\n" % (reg.TYPE, reg.NAME))
              else:
                if reg.BLOCK_WRITE_ENABLE_INFERRED == "NO":
                  f.write("  input logic [%d - 1 : 0] vld_in_%s,\n" % (reg.DEPTH, reg.NAME))
                if reg.TYPE == LOGIC:
                  if reg.WIDTH_IN_BITS == 1:
                    f.write("  input [%d - 1 : 0] in_%s,\n" % (reg.DEPTH, reg.NAME))
                  else:
                    f.write("  input [%d - 1 : 0][%s_%s_WIDTH - 1 : 0] in_%s,\n" % (reg.DEPTH, blkname.upper(), reg.NAME.upper(), reg.NAME))
                else:
                  f.write("  input %s [%d - 1 : 0] in_%s,\n" % (reg.TYPE, reg.DEPTH, reg.NAME))

            f.write("  input  logic                clk,\n")
            f.write("  input  logic                clk_i_ring,\n")
            f.write("  input  logic                clk_i_local,\n")
            f.write("  input  logic                clk_o_ring,\n")
            f.write("  input  logic                clk_o_local,\n")
            f.write("  output logic                clk_en,\n")
            f.write("  input  logic                sync_i_ring_s,\n")
            f.write("  input  logic                sync_i_local_s,\n")
            f.write("  input  logic                sync_o_ring_s,\n")
            f.write("  input  logic                sync_o_local_s,\n")
            f.write("  input  logic                sync_i_ring_f,\n")
            f.write("  input  logic                sync_i_local_f,\n")
            f.write("  input  logic                sync_o_ring_f,\n")
            f.write("  input  logic                sync_o_local_f,\n")
            f.write("  input  logic                no_clk_div_i_ring,\n")
            f.write("  input  logic                no_clk_div_i_local,\n")
            f.write("  input  logic                no_clk_div_o_ring,\n")
            f.write("  input  logic                no_clk_div_o_local,\n")
            f.write("  input  logic                rstn,\n")
            f.write("  // i_req_ring_if & o_resp_ring_if\n")
            f.write("  output oursring_resp_if_b_t o_resp_ring_if_b,\n"),
            f.write("  output oursring_resp_if_r_t o_resp_ring_if_r,\n"),
            f.write("  output                      o_resp_ring_if_rvalid,\n"),
            f.write("  input                       o_resp_ring_if_rready,\n"),
            f.write("  output                      o_resp_ring_if_bvalid,\n"),
            f.write("  input                       o_resp_ring_if_bready,\n"),
            f.write("  input                       i_req_ring_if_awvalid,\n"),
            f.write("  input                       i_req_ring_if_wvalid,\n"),
            f.write("  input                       i_req_ring_if_arvalid,\n"),
            f.write("  input  oursring_req_if_ar_t i_req_ring_if_ar,\n"),
            f.write("  input  oursring_req_if_aw_t i_req_ring_if_aw,\n"),
            f.write("  input  oursring_req_if_w_t  i_req_ring_if_w,\n"),
            f.write("  output                      i_req_ring_if_arready,\n"),
            f.write("  output                      i_req_ring_if_wready,\n"),
            f.write("  output                      i_req_ring_if_awready,\n"),
            f.write("  // o_req_ring_if & i_resp_ring_if\n")
            f.write("  input  oursring_resp_if_b_t i_resp_ring_if_b,\n"),
            f.write("  input  oursring_resp_if_r_t i_resp_ring_if_r,\n"),
            f.write("  input                       i_resp_ring_if_rvalid,\n"),
            f.write("  output                      i_resp_ring_if_rready,\n"),
            f.write("  input                       i_resp_ring_if_bvalid,\n"),
            f.write("  output                      i_resp_ring_if_bready,\n"),
            f.write("  output                      o_req_ring_if_awvalid,\n"),
            f.write("  output                      o_req_ring_if_wvalid,\n"),
            f.write("  output                      o_req_ring_if_arvalid,\n"),
            f.write("  output oursring_req_if_ar_t o_req_ring_if_ar,\n"),
            f.write("  output oursring_req_if_aw_t o_req_ring_if_aw,\n"),
            f.write("  output oursring_req_if_w_t  o_req_ring_if_w,\n"),
            f.write("  input                       o_req_ring_if_arready,\n"),
            f.write("  input                       o_req_ring_if_wready,\n"),
            f.write("  input                       o_req_ring_if_awready,\n"),
            f.write("  // i_req_local_if & o_resp_local_if\n")
            f.write("  output oursring_resp_if_b_t o_resp_local_if_b,\n"),
            f.write("  output oursring_resp_if_r_t o_resp_local_if_r,\n"),
            f.write("  output                      o_resp_local_if_rvalid,\n"),
            f.write("  input                       o_resp_local_if_rready,\n"),
            f.write("  output                      o_resp_local_if_bvalid,\n"),
            f.write("  input                       o_resp_local_if_bready,\n"),
            f.write("  input                       i_req_local_if_awvalid,\n"),
            f.write("  input                       i_req_local_if_wvalid,\n"),
            f.write("  input                       i_req_local_if_arvalid,\n"),
            f.write("  input  oursring_req_if_ar_t i_req_local_if_ar,\n"),
            f.write("  input  oursring_req_if_aw_t i_req_local_if_aw,\n"),
            f.write("  input  oursring_req_if_w_t  i_req_local_if_w,\n"),
            f.write("  output                      i_req_local_if_arready,\n"),
            f.write("  output                      i_req_local_if_wready,\n"),
            f.write("  output                      i_req_local_if_awready,\n"),
            f.write("  // o_req_local_if & i_resp_local_if\n")
            f.write("  input  oursring_resp_if_b_t i_resp_local_if_b,\n"),
            f.write("  input  oursring_resp_if_r_t i_resp_local_if_r,\n"),
            f.write("  input                       i_resp_local_if_rvalid,\n"),
            f.write("  output                      i_resp_local_if_rready,\n"),
            f.write("  input                       i_resp_local_if_bvalid,\n"),
            f.write("  output                      i_resp_local_if_bready,\n"),
            f.write("  output                      o_req_local_if_awvalid,\n"),
            f.write("  output                      o_req_local_if_wvalid,\n"),
            f.write("  output                      o_req_local_if_arvalid,\n"),
            f.write("  output oursring_req_if_ar_t o_req_local_if_ar,\n"),
            f.write("  output oursring_req_if_aw_t o_req_local_if_aw,\n"),
            f.write("  output oursring_req_if_w_t  o_req_local_if_w,\n"),
            f.write("  input                       o_req_local_if_arready,\n"),
            f.write("  input                       o_req_local_if_wready,\n"),
            f.write("  input                       o_req_local_if_awready\n"),
            f.write(");\n")

            if len(regblk.BLKID) == 1:
              if type(regblk.BLKID[0]) == int:
                f.write("  localparam int                                STATION_ID_WIDTH_0 = %s_BLKID_WIDTH;\n" % (blkname.upper()))
                f.write("  localparam logic [STATION_ID_WIDTH_0 - 1 : 0] LOCAL_STATION_ID_0 = %s_BLKID;\n" % (blkname.upper()))
              elif type(regblk.BLKID[0]) == list:
                for j in range (0, len(regblk.BLKID[0])):
                  f.write("  localparam int                                  STATION_ID_WIDTH_%0d = %s_BLKID_WIDTH_SUB_%0d;\n" % (j, blkname.upper(), j))
                  f.write("  localparam logic [STATION_ID_WIDTH_%0d - 1 : 0] LOCAL_STATION_ID_%0d = %s_BLKID_SUB_%0d;\n" % (j, j, blkname.upper(), j))
            else:
              # TODO: support multiple sub blkid
              f.write("  localparam int                                STATION_ID_WIDTH_0 = %s_BLKID_WIDTH;\n" % (blkname.upper()))
              f.write("  localparam logic [STATION_ID_WIDTH_0 - 1 : 0] LOCAL_STATION_ID_0 =\n")
              for i in range (0, len(regblk.BLKID)):
                f.write("    (BLOCK_INST_ID == %d) ? %s_BLKID_%d :\n" % (i, blkname.upper(), i))
              f.write("    0;\n")
              # assertion
              f.write("""
  `ifndef SYNTHESIS
  initial begin
    assert (BLOCK_INST_ID < %d) else $fatal("%%m: BLOCK_INST_ID = %%d >= %d", BLOCK_INST_ID);
  end
  `endif
""" % (len(regblk.BLKID), len(regblk.BLKID)))

            for reg in ls_station_hw:
              if reg.BLOCK_WRITE_ENABLE_INFERRED == "YES":
                if reg.DEPTH == 1:
                  f.write("  logic vld_in_%s;\n" % (reg.NAME))
                else:
                  f.write("  logic [%d - 1 : 0] vld_in_%s;\n" % (reg.DEPTH, reg.NAME))

            f.write("""
  oursring_resp_if_b_t      station2brb_rsp_b;
  oursring_resp_if_r_t      station2brb_rsp_r;
  logic                     station2brb_rsp_rvalid;
  logic                     station2brb_rsp_rready;
  logic                     station2brb_rsp_bvalid;
  logic                     station2brb_rsp_bready;
  logic                     station2brb_req_awvalid;
  logic                     station2brb_req_wvalid;
  logic                     station2brb_req_arvalid;
  oursring_req_if_ar_t      station2brb_req_ar;
  oursring_req_if_aw_t      station2brb_req_aw;
  oursring_req_if_w_t       station2brb_req_w;
  logic                     station2brb_req_arready;
  logic                     station2brb_req_wready;
  logic                     station2brb_req_awready;

  logic                     station_clk_en;
""")
            if type(regblk.BLKID[0]) == int:
              f.write("""
  oursring_station #(.STATION_ID_WIDTH_0(STATION_ID_WIDTH_0), .LOCAL_STATION_ID_0(LOCAL_STATION_ID_0), .RING_ADDR_WIDTH(%s_RING_ADDR_WIDTH), .MAX_RING_ADDR(%s_MAX_RING_ADDR)) station_u (
""" % (blkname.upper(), blkname.upper()))
            elif type(regblk.BLKID[0]) == list:
              # support only 2
              f.write("""
  oursring_station #(.STATION_ID_WIDTH_0(STATION_ID_WIDTH_0), .NUM_STATION_ID(%0d), .LOCAL_STATION_ID_0(LOCAL_STATION_ID_0), .STATION_ID_WIDTH_1(STATION_ID_WIDTH_1), .LOCAL_STATION_ID_1(LOCAL_STATION_ID_1), .RING_ADDR_WIDTH(%s_RING_ADDR_WIDTH), .MAX_RING_ADDR(%s_MAX_RING_ADDR)) station_u (
""" % (len(regblk.BLKID[0]), blkname.upper(), blkname.upper()))
            f.write("""
    .i_req_local_if_ar      (i_req_local_if_ar), 
    .i_req_local_if_awvalid (i_req_local_if_awvalid), 
    .i_req_local_if_awready (i_req_local_if_awready), 
    .i_req_local_if_wvalid  (i_req_local_if_wvalid), 
    .i_req_local_if_wready  (i_req_local_if_wready), 
    .i_req_local_if_arvalid (i_req_local_if_arvalid), 
    .i_req_local_if_arready (i_req_local_if_arready), 
    .i_req_local_if_w       (i_req_local_if_w), 
    .i_req_local_if_aw      (i_req_local_if_aw),
    .i_req_ring_if_ar       (i_req_ring_if_ar), 
    .i_req_ring_if_awvalid  (i_req_ring_if_awvalid), 
    .i_req_ring_if_awready  (i_req_ring_if_awready), 
    .i_req_ring_if_wvalid   (i_req_ring_if_wvalid), 
    .i_req_ring_if_wready   (i_req_ring_if_wready), 
    .i_req_ring_if_arvalid  (i_req_ring_if_arvalid), 
    .i_req_ring_if_arready  (i_req_ring_if_arready), 
    .i_req_ring_if_w        (i_req_ring_if_w), 
    .i_req_ring_if_aw       (i_req_ring_if_aw),
    .o_req_local_if_ar      (station2brb_req_ar), 
    .o_req_local_if_awvalid (station2brb_req_awvalid), 
    .o_req_local_if_awready (station2brb_req_awready), 
    .o_req_local_if_wvalid  (station2brb_req_wvalid), 
    .o_req_local_if_wready  (station2brb_req_wready), 
    .o_req_local_if_arvalid (station2brb_req_arvalid), 
    .o_req_local_if_arready (station2brb_req_arready), 
    .o_req_local_if_w       (station2brb_req_w), 
    .o_req_local_if_aw      (station2brb_req_aw),
    .o_req_ring_if_ar       (o_req_ring_if_ar), 
    .o_req_ring_if_awvalid  (o_req_ring_if_awvalid), 
    .o_req_ring_if_awready  (o_req_ring_if_awready), 
    .o_req_ring_if_wvalid   (o_req_ring_if_wvalid), 
    .o_req_ring_if_wready   (o_req_ring_if_wready), 
    .o_req_ring_if_arvalid  (o_req_ring_if_arvalid), 
    .o_req_ring_if_arready  (o_req_ring_if_arready), 
    .o_req_ring_if_w        (o_req_ring_if_w), 
    .o_req_ring_if_aw       (o_req_ring_if_aw),
    .i_resp_local_if_b      (station2brb_rsp_b), 
    .i_resp_local_if_r      (station2brb_rsp_r), 
    .i_resp_local_if_rvalid (station2brb_rsp_rvalid), 
    .i_resp_local_if_rready (station2brb_rsp_rready), 
    .i_resp_local_if_bvalid (station2brb_rsp_bvalid), 
    .i_resp_local_if_bready (station2brb_rsp_bready),
    .i_resp_ring_if_b       (i_resp_ring_if_b), 
    .i_resp_ring_if_r       (i_resp_ring_if_r), 
    .i_resp_ring_if_rvalid  (i_resp_ring_if_rvalid), 
    .i_resp_ring_if_rready  (i_resp_ring_if_rready), 
    .i_resp_ring_if_bvalid  (i_resp_ring_if_bvalid), 
    .i_resp_ring_if_bready  (i_resp_ring_if_bready),
    .o_resp_local_if_b      (o_resp_local_if_b), 
    .o_resp_local_if_r      (o_resp_local_if_r), 
    .o_resp_local_if_rvalid (o_resp_local_if_rvalid), 
    .o_resp_local_if_rready (o_resp_local_if_rready), 
    .o_resp_local_if_bvalid (o_resp_local_if_bvalid), 
    .o_resp_local_if_bready (o_resp_local_if_bready),
    .o_resp_ring_if_b       (o_resp_ring_if_b), 
    .o_resp_ring_if_r       (o_resp_ring_if_r), 
    .o_resp_ring_if_rvalid  (o_resp_ring_if_rvalid), 
    .o_resp_ring_if_rready  (o_resp_ring_if_rready), 
    .o_resp_ring_if_bvalid  (o_resp_ring_if_bvalid), 
    .o_resp_ring_if_bready  (o_resp_ring_if_bready),
    .clk                    (clk),
    .clk_en                 (station_clk_en),
    .rstn                   (rstn)
    );
""")

            f.write("""
  ring_data_t wmask, wmask_inv;
  generate
    for (genvar i = 0; i < $bits(ring_strb_t); i++) begin : WMASK_GEN
      assign wmask[i * 8 +: 8]      = (station2brb_req_w.wstrb[i]) ? 8'hff : 8'h00;
      assign wmask_inv[i * 8 +: 8]  = (station2brb_req_w.wstrb[i]) ? 8'h00 : 8'hff;
    end
  endgenerate

  logic clk_reg;
  logic icg_en;
  logic icg_tst_en;
  assign icg_tst_en = 1'b0;
  assign clk_en = station_clk_en | icg_en;
  icg_rstn shared_icg_u (
    .clkg   (clk_reg),
    .en     (icg_en),
    .tst_en (icg_tst_en),
    .rstn   (rstn),
    .clk    (clk_o_local)
    );
""")
            station_icg_en = list()

            for reg in ls_station_rw + ls_station_wo:
              icg_en = list()
              if reg.DEPTH == 1:
                f.write("  logic %s_icg_en;\n" % (reg.NAME))
                f.write("  logic %s_clkg;\n" % (reg.NAME))
                f.write("  icg_rstn %s_icg_u (\n" % (reg.NAME))
                f.write("    .clkg    (%s_clkg),\n" % (reg.NAME))
                f.write("    .en      (%s_icg_en),\n" % (reg.NAME))
                f.write("    .tst_en  (icg_tst_en),\n")
                f.write("    .rstn    (rstn),\n")
                if reg.UNDER_SHARED_ICG == "YES":
                  f.write("    .clk     (clk_reg)\n")
                else:
                  f.write("    .clk     (clk_o_local)\n")
                f.write("  );\n")
              else:
                for d in range(reg.DEPTH):
                  f.write("  logic %s_icg_en_%d;\n" % (reg.NAME, d))
                  f.write("  logic %s_clkg_%d;\n" % (reg.NAME, d))
                  f.write("  icg_rstn %s_icg_%d_u (\n" % (reg.NAME, d))
                  f.write("    .clkg    (%s_clkg_%d),\n" % (reg.NAME, d))
                  f.write("    .en      (%s_icg_en_%d),\n" % (reg.NAME, d))
                  f.write("    .tst_en  (icg_tst_en),\n")
                  f.write("    .rstn    (rstn),\n")
                  if reg.UNDER_SHARED_ICG == "YES":
                    f.write("    .clk     (clk_reg)\n")
                  else:
                    f.write("    .clk     (clk_o_local)\n")
                  f.write("  );\n")
              if reg.TYPE == LOGIC:
                if reg.DEPTH == 1:
                  f.write("  logic [%s_%s_WIDTH - 1 : 0] rff_%s;\n" % (blkname.upper(), reg.NAME.upper(), reg.NAME))
                  f.write("  logic [%s_%s_WIDTH - 1 : 0] %s;\n" % (blkname.upper(), reg.NAME.upper(), reg.NAME))
                else:
                  f.write("  logic [%d - 1 : 0][%s_%s_WIDTH - 1 : 0] rff_%s;\n" % (reg.DEPTH, blkname.upper(), reg.NAME.upper(), reg.NAME))
                  f.write("  logic [%d - 1 : 0][%s_%s_WIDTH - 1 : 0] %s;\n" % (reg.DEPTH, blkname.upper(), reg.NAME.upper(), reg.NAME))
              else:
                if reg.DEPTH == 1:
                  f.write("  %s rff_%s;\n" % (reg.TYPE, reg.NAME))
                  f.write("  %s %s;\n" % (reg.TYPE, reg.NAME))
                else:
                  f.write("  %s [%d - 1 : 0] rff_%s;\n" % (reg.TYPE, reg.DEPTH, reg.NAME))
                  f.write("  %s [%d - 1 : 0] %s;\n" % (reg.TYPE, reg.DEPTH, reg.NAME))
              if reg.DEPTH == 1:
                f.write("  logic load_%s;\n" % (reg.NAME))
              else:
                f.write("  logic [%d - 1 : 0] load_%s;\n" % (reg.DEPTH, reg.NAME))
              if reg.AUTO_CLEAR == "YES":
                if reg.DEPTH == 1:
                  f.write("""
  logic dff_load_%s_d;
  always_ff @(posedge %s_clkg) begin
    dff_load_%s_d <= load_%s;
  end
""" % (reg.NAME, reg.NAME, reg.NAME, reg.NAME))
                else:
                  for d in range(reg.DEPTH):
                    f.write("""
  logic dff_load_%s_d[%d];
  always_ff @(posedge %s_clkg_%d) begin
    dff_load_%s_d[%d] <= load_%s[%d];
  end
""" % (reg.NAME, d, reg.NAME, d, reg.NAME, d, reg.NAME, d))
              if reg.DEPTH == 1:
                f.write("""
  always_ff @(posedge %s_clkg) begin
    if (rstn == 1'b0) begin
      rff_%s <= %s%s_%s_RSTVAL%s;
    end else if (load_%s == 1'b1) begin
      rff_%s <= %s(wmask & %s) | (wmask_inv & rff_%s)%s;
""" % (reg.NAME, reg.NAME, reg.typecast_pre(), blkname.upper(), reg.NAME.upper(), reg.typecast_post(), reg.NAME, reg.NAME, reg.typecast_pre(), reg.NAME, reg.NAME, reg.typecast_post()))
                icg_en.append("load_%s" % (reg.NAME))
                if reg.HW_WRITABLE == WR_YES:
                  f.write("""
    end else if (vld_in_%s == 1'b1) begin
      rff_%s <= in_%s;
""" % (reg.NAME, reg.NAME, reg.NAME))
                  icg_en.append("vld_in_%s" % (reg.NAME))
                if reg.AUTO_CLEAR == "YES":
                  f.write("""
    end else if (dff_load_%s_d == 1'b1) begin
      rff_%s <= %s%s_%s_RSTVAL%s;
""" % (reg.NAME, reg.NAME, reg.typecast_pre(), blkname.upper(), reg.NAME.upper(), reg.typecast_post()))
                  icg_en.append("dff_load_%s_d" % (reg.NAME))
                f.write("""    end
  end
  assign out_%s = rff_%s;
""" % (reg.NAME, reg.NAME))
              else:
                for d in range(reg.DEPTH):
                  f.write("""
  always_ff @(posedge %s_clkg_%d) begin
    if (rstn == 1'b0) begin
      rff_%s[%d] <= %s%s_%s_RSTVAL%s;
    end else if (load_%s[%d] == 1'b1) begin
      rff_%s[%d] <= %s(wmask & %s[%d]) | (wmask_inv & rff_%s[%d])%s;
""" % (reg.NAME, d, reg.NAME, d, reg.typecast_pre(), blkname.upper(), reg.NAME.upper(), reg.typecast_post(), reg.NAME, d, reg.NAME, d, reg.typecast_pre(), reg.NAME, d, reg.NAME, d, reg.typecast_post()))
                  icg_en.append("load_%s" % (reg.NAME))
                  if reg.HW_WRITABLE == WR_YES:
                    f.write("""
    end else if (vld_in_%s[%d] == 1'b1) begin
      rff_%s[%d] <= in_%s[%d];
""" % (reg.NAME, d, reg.NAME, d, reg.NAME, d))
                    icg_en.append("vld_in_%s" % (reg.NAME))
                  if reg.AUTO_CLEAR == "YES":
                    f.write("""
    end else if (dff_load_%s_d[%d] == 1'b1) begin
      rff_%s[%d] <= %s%s_%s_RSTVAL%s;
""" % (reg.NAME, d, reg.NAME, d, reg.typecast_pre(), blkname.upper(), reg.NAME.upper(), reg.typecast_post()))
                    icg_en.append("dff_load_%s_d" % (reg.NAME))
                  f.write("""    end
  end""")
                f.write("""
  assign out_%s = rff_%s;
""" % (reg.NAME, reg.NAME))
              if reg.DEPTH == 1:
                f.write("  assign %s_icg_en = %s;\n" % (reg.NAME, " | ".join(icg_en)))
                if reg.UNDER_SHARED_ICG == "YES":
                  station_icg_en.append("%s_icg_en" % (reg.NAME))
              else:
                icg_en = list(set(icg_en))
                for d in range(reg.DEPTH):
                  icg_en_tmp = list(icg_en)
                  for i in range(len(icg_en_tmp)):
                    icg_en_tmp[i] += "[%d]" % d
                  f.write("  assign %s_icg_en_%d = %s;\n" % (reg.NAME, d, " | ".join(sorted(icg_en_tmp))))
                  if reg.UNDER_SHARED_ICG == "YES":
                    station_icg_en.append("%s_icg_en_%d" % (reg.NAME, d))

            for reg in ls_station_ro:
              icg_en = list()
              if reg.DEPTH == 1:
                f.write("  logic %s_icg_en;\n" % (reg.NAME))
                f.write("  logic %s_clkg;\n" % (reg.NAME))
                f.write("  icg_rstn %s_icg_u (\n" % (reg.NAME))
                f.write("    .clkg    (%s_clkg),\n" % (reg.NAME))
                f.write("    .en      (%s_icg_en),\n" % (reg.NAME))
                f.write("    .tst_en  (icg_tst_en),\n")
                f.write("    .rstn    (rstn),\n")
                if reg.UNDER_SHARED_ICG == "YES":
                  f.write("    .clk     (clk_reg)\n")
                else:
                  f.write("    .clk     (clk_o_local)\n")
                f.write("  );\n")
              else:
                for d in range(reg.DEPTH):
                  f.write("  logic %s_icg_en_%d;\n" % (reg.NAME, d))
                  f.write("  logic %s_clkg_%d;\n" % (reg.NAME, d))
                  f.write("  icg_rstn %s_icg_%d_u (\n" % (reg.NAME, d))
                  f.write("    .clkg    (%s_clkg_%d),\n" % (reg.NAME, d))
                  f.write("    .en      (%s_icg_en_%d),\n" % (reg.NAME, d))
                  f.write("    .tst_en  (icg_tst_en),\n")
                  f.write("    .rstn    (rstn),\n")
                  if reg.UNDER_SHARED_ICG == "YES":
                    f.write("    .clk     (clk_reg)\n")
                  else:
                    f.write("    .clk     (clk_o_local)\n")
                  f.write("  );\n")
              if reg.TYPE == LOGIC:
                if reg.DEPTH == 1:
                  f.write("  logic [%s_%s_WIDTH - 1 : 0] rff_%s;\n" % (blkname.upper(), reg.NAME.upper(), reg.NAME))
                else:
                  f.write("  logic [%d - 1 : 0][%s_%s_WIDTH - 1 : 0] rff_%s;\n" % (reg.DEPTH, blkname.upper(), reg.NAME.upper(), reg.NAME))
              else:
                if reg.DEPTH == 1:
                  f.write("  %s rff_%s;\n" % (reg.TYPE, reg.NAME))
                else:
                  f.write("  %s [%d - 1 : 0] rff_%s;\n" % (reg.TYPE, reg.DEPTH, reg.NAME))
              if reg.DEPTH == 1:
                f.write("""
  always_ff @(posedge %s_clkg) begin
    if (rstn == 1'b0) begin
      rff_%s <= %s%s_%s_RSTVAL%s;
""" % (reg.NAME, reg.NAME, reg.typecast_pre(), blkname.upper(), reg.NAME.upper(), reg.typecast_post()))
                if reg.HW_WRITABLE == WR_YES:
                  f.write("""
    end else if (vld_in_%s == 1'b1) begin
      rff_%s <= in_%s;
""" % (reg.NAME, reg.NAME, reg.NAME))
                  icg_en.append("vld_in_%s" % (reg.NAME))
                f.write("""
    end
  end
  assign out_%s = rff_%s;
""" % (reg.NAME, reg.NAME))
              else:
                for d in range(reg.DEPTH):
                  f.write("""
  always_ff @(posedge %s_clkg_%d) begin
    if (rstn == 1'b0) begin
      rff_%s[%d] <= %s%s_%s_RSTVAL%s;
""" % (reg.NAME, d, reg.NAME, d, reg.typecast_pre(), blkname.upper(), reg.NAME.upper(), reg.typecast_post()))
                  if reg.HW_WRITABLE == WR_YES:
                    f.write("""
    end else if (vld_in_%s[%d] == 1'b1) begin
      rff_%s[%d] <= in_%s[%d];
""" % (reg.NAME, d, reg.NAME, d, reg.NAME, d))
                    icg_en.append("vld_in_%s" % (reg.NAME))
                  f.write("""
    end
  end""")
                f.write("""
  assign out_%s = rff_%s;
""" % (reg.NAME, reg.NAME))
              if reg.DEPTH == 1:
                f.write("  assign %s_icg_en = %s;\n" % (reg.NAME, " | ".join(icg_en)))
                if reg.UNDER_SHARED_ICG == "YES":
                  station_icg_en.append("%s_icg_en" % (reg.NAME))
              else:
                icg_en = list(set(icg_en))
                for d in range(reg.DEPTH):
                  icg_en_tmp = list(icg_en)
                  for i in range(len(icg_en_tmp)):
                    icg_en_tmp[i] += "[%d]" % d
                  f.write("  assign %s_icg_en_%d = %s;\n" % (reg.NAME, d, " | ".join(sorted(icg_en_tmp))))
                  if reg.UNDER_SHARED_ICG == "YES":
                    station_icg_en.append("%s_icg_en_%d" % (reg.NAME, d))
            f.write("  assign icg_en = %s;\n" % (" | ".join(station_icg_en)))

            for reg in ls_station_hw:
              if reg.BLOCK_WRITE_ENABLE_INFERRED == "YES":
                if reg.DEPTH == 1:
                  f.write("  assign vld_in_%s = (in_%s != rff_%s);\n" % (reg.NAME, reg.NAME, reg.NAME))
                else:
                  for d in range(reg.DEPTH):
                    f.write("  assign vld_in_%s[%d] = (in_%s[%d] != rff_%s[%d]);\n" % (reg.NAME, d, reg.NAME, d, reg.NAME, d))


            f.write("""
  logic                         rdec;
  logic                         bdec;
  axi4_resp_t                   rresp;
  axi4_resp_t                   bresp;
  logic [%s_DATA_WIDTH - 1 : 0] data;

  always_comb begin
    rdec  = 1'b0;
    bdec  = 1'b0;
    rresp = AXI_RESP_DECERR;
    bresp = AXI_RESP_DECERR;
    data  = {%s_DATA_WIDTH{1'b0}};
""" % (blkname.upper(), blkname.upper()))

            for reg in ls_station_rw + ls_station_wo:
              if reg.DEPTH == 1:
                f.write("    %s = rff_%s;\n" % (reg.NAME, reg.NAME))
                f.write("    load_%s = 1'b0;\n" % (reg.NAME))
              else:
                for d in range(reg.DEPTH):
                  f.write("    %s[%d] = rff_%s[%d];\n" % (reg.NAME, d, reg.NAME, d))
                  f.write("    load_%s[%d] = 1'b0;\n" % (reg.NAME, d))

            f.write("""
    if (station2brb_req_arvalid) begin
      case (1'b1)""")
            for reg in ls_station_rw + ls_station_ro + ls_station_wo: # for write only, returns 0 always
              if reg.DEPTH == 1:
                f.write("""
        ((station2brb_req_ar.araddr[%s_RING_ADDR_WIDTH-1 -: STATION_ID_WIDTH_0] == LOCAL_STATION_ID_0) && ({{(STATION_ID_WIDTH_0+$clog2(%s_DATA_WIDTH/8)){1'b0}}, station2brb_req_ar.araddr[%s_RING_ADDR_WIDTH-STATION_ID_WIDTH_0-1:$clog2(%s_DATA_WIDTH/8)]} == (%s_%s_OFFSET >> $clog2(%s_DATA_WIDTH/8)))): begin""" % (blkname.upper(), blkname.upper(), blkname.upper(), blkname.upper(), blkname.upper(), reg.NAME.upper(), blkname.upper()))
                if reg.SW_ACCESS_PERMISSION == WO:
                  f.write("""
          data = '0;""")
                else:
                  if reg.ACCESS_IN_32_BITS == "NO":
                    f.write("""
          data = rff_%s;""" % (reg.NAME))
                  else:
                    f.write("""
          if (station2brb_req_ar.araddr[$clog2(%s_DATA_WIDTH/8)-1 : 0] == {$clog2(%s_DATA_WIDTH/8){1'b0}}) begin
            data = {32'b0, rff_%s[31:0]};
          end else begin
            data = {rff_%s[63:32], 32'b0};
          end""" % (blkname.upper(), blkname.upper(), reg.NAME, reg.NAME))
                f.write("""
          rdec  = 1'b1;
          rresp = AXI_RESP_OKAY;
        end""")
              else:
                for d in range(reg.DEPTH):
                  f.write("""
        ((station2brb_req_ar.araddr[%s_RING_ADDR_WIDTH-1 -: STATION_ID_WIDTH_0] == LOCAL_STATION_ID_0) && ({{(STATION_ID_WIDTH_0+$clog2(%s_DATA_WIDTH/8)){1'b0}}, station2brb_req_ar.araddr[%s_RING_ADDR_WIDTH-STATION_ID_WIDTH_0-1:$clog2(%s_DATA_WIDTH/8)]} == (%s_%s_OFFSET__DEPTH_%d >> $clog2(%s_DATA_WIDTH/8)))): begin""" % (blkname.upper(), blkname.upper(), blkname.upper(), blkname.upper(), blkname.upper(), reg.NAME.upper(), d, blkname.upper()))
                  if reg.SW_ACCESS_PERMISSION == WO:
                    f.write("""
          data = '0;""")
                  else:
                    if reg.ACCESS_IN_32_BITS == "NO":
                      f.write("""
          data = rff_%s[%d];""" % (reg.NAME, d))
                    else:
                      f.write("""
          if (station2brb_req_ar.araddr[$clog2(%s_DATA_WIDTH/8)-1 : 0] == {$clog2(%s_DATA_WIDTH/8){1'b0}}) begin
            data = {32'b0, rff_%s[%d][31:0]};
          end else begin
            data = {rff_%s[%d][63:32], 32'b0};
          end""" % (blkname.upper(), blkname.upper(), reg.NAME, d, reg.NAME, d))
                  f.write("""
          rdec  = 1'b1;
          rresp = AXI_RESP_OKAY;
        end""")
            f.write("""
        default: begin""")
            if (regblk.SLAVE_BLOCK_EXISTS == "YES"):
              f.write("""
          rdec  = 1'b0;""")
            else:
              f.write("""
          rdec  = 1'b1;""")
            f.write("""
          data  = {%s_DATA_WIDTH{1'b0}};
          rresp = AXI_RESP_DECERR;
        end
      endcase
    end""" % blkname.upper())

            f.write("""
    if (station2brb_req_awvalid & station2brb_req_wvalid) begin
      case (1'b1)""")
            for reg in ls_station_rw + ls_station_wo + ls_station_ro: # for read only, write takes no effect
              if reg.DEPTH == 1:
                f.write("""
        ((station2brb_req_aw.awaddr[%s_RING_ADDR_WIDTH-1 -: STATION_ID_WIDTH_0] == LOCAL_STATION_ID_0) && ({{(STATION_ID_WIDTH_0+$clog2(%s_DATA_WIDTH/8)){1'b0}}, station2brb_req_aw.awaddr[%s_RING_ADDR_WIDTH-STATION_ID_WIDTH_0-1:$clog2(%s_DATA_WIDTH/8)]} == (%s_%s_OFFSET >> $clog2(%s_DATA_WIDTH/8)))): begin""" % (blkname.upper(), blkname.upper(), blkname.upper(), blkname.upper(), blkname.upper(), reg.NAME.upper(), blkname.upper()))
                if reg.SW_ACCESS_PERMISSION == RO:
                  f.write("""
          bdec  = 1'b1;
          bresp = AXI_RESP_OKAY;
        end""")
                else:
                  if reg.ACCESS_IN_32_BITS == "NO":
                    f.write("""
          %s = %sstation2brb_req_w.wdata[%s_%s_WIDTH - 1 : 0]%s;
          load_%s = station2brb_req_awready & station2brb_req_wready;
          bdec  = 1'b1;
          bresp = AXI_RESP_OKAY;
        end""" % (reg.NAME, reg.typecast_pre(), blkname.upper(), reg.NAME.upper(), reg.typecast_post(), reg.NAME))
                  else:
                    f.write("""
          if (station2brb_req_aw.awaddr[$clog2(%s_DATA_WIDTH/8)-1 : 0] == {$clog2(%s_DATA_WIDTH/8){1'b0}}) begin
            %s[31:0] = station2brb_req_w.wdata[31:0];
            load_%s = station2brb_req_awready & station2brb_req_wready;
          end else begin
            %s[63:32] = station2brb_req_w.wdata[63:32];
            load_%s = station2brb_req_awready & station2brb_req_wready;
          end
          bdec  = 1'b1;
          bresp = AXI_RESP_OKAY;
        end""" % (blkname.upper(), blkname.upper(), reg.NAME, reg.NAME, reg.NAME, reg.NAME))
              else:
                for d in range(reg.DEPTH):
                  f.write("""
        ((station2brb_req_aw.awaddr[%s_RING_ADDR_WIDTH-1 -: STATION_ID_WIDTH_0] == LOCAL_STATION_ID_0) && ({{(STATION_ID_WIDTH_0+$clog2(%s_DATA_WIDTH/8)){1'b0}}, station2brb_req_aw.awaddr[%s_RING_ADDR_WIDTH-STATION_ID_WIDTH_0-1:$clog2(%s_DATA_WIDTH/8)]} == (%s_%s_OFFSET__DEPTH_%d >> $clog2(%s_DATA_WIDTH/8)))): begin""" % (blkname.upper(), blkname.upper(), blkname.upper(), blkname.upper(), blkname.upper(), reg.NAME.upper(), d, blkname.upper()))
                  if reg.SW_ACCESS_PERMISSION == RO:
                    f.write("""
          bdec  = 1'b1;
          bresp = AXI_RESP_OKAY;
        end""")
                  else:
                    if reg.ACCESS_IN_32_BITS == "NO":
                      f.write("""
          %s[%d] = %sstation2brb_req_w.wdata[%s_%s_WIDTH - 1 : 0]%s;
          load_%s[%d] = station2brb_req_awready & station2brb_req_wready;
          bdec  = 1'b1;
          bresp = AXI_RESP_OKAY;
        end""" % (reg.NAME, d, reg.typecast_pre(), blkname.upper(), reg.NAME.upper(), reg.typecast_post(), reg.NAME, d))
                    else:
                      f.write("""
          if (station2brb_req_aw.awaddr[$clog2(%s_DATA_WIDTH/8)-1 : 0] == {$clog2(%s_DATA_WIDTH/8){1'b0}}) begin
            %s[%d][31:0] = station2brb_req_w.wdata[31:0];
            load_%s[%d] = station2brb_req_awready & station2brb_req_wready;
          end else begin
            %s[%d][63:32] = station2brb_req_w.wdata[63:32];
            load_%s[%d] = station2brb_req_awready & station2brb_req_wready;
          end
          bdec  = 1'b1;
          bresp = AXI_RESP_OKAY;
        end""" % (blkname.upper(), blkname.upper(), reg.NAME, d, reg.NAME, d, reg.NAME, d, reg.NAME, d))
            f.write("""
        default: begin""")
            if (regblk.SLAVE_BLOCK_EXISTS == "YES"):
              f.write("""
          bdec  = 1'b0;""")
            else:
              f.write("""
          bdec  = 1'b1;""")
            f.write("""
          bresp = AXI_RESP_DECERR;
        end
      endcase
    end
  end

  // Response Muxing
  oursring_resp_if_b_t  i_resp_if_b[2];
  oursring_resp_if_r_t  i_resp_if_r[2];
  logic                 i_resp_if_rvalid[2];
  logic                 i_resp_if_rready[2];
  logic                 i_resp_if_bvalid[2];
  logic                 i_resp_if_bready[2];
  oursring_resp_if_b_t  o_resp_ppln_if_b[1];
  oursring_resp_if_r_t  o_resp_ppln_if_r[1];
  logic                 o_resp_ppln_if_rvalid[1];
  logic                 o_resp_ppln_if_rready[1];
  logic                 o_resp_ppln_if_bvalid[1];
  logic                 o_resp_ppln_if_bready[1];

  logic                 rff_awrdy, rff_wrdy, next_awrdy, next_wrdy;
  // Request Bypassing
  assign o_req_local_if_awvalid  = (station2brb_req_awvalid & ~bdec) & ~rff_awrdy;
  assign o_req_local_if_wvalid   = (station2brb_req_wvalid & ~bdec) & ~rff_wrdy;
  assign o_req_local_if_arvalid  = (station2brb_req_arvalid & ~rdec);""")
            if (regblk.REMOVE_STATION_ID_FOR_LOCAL_REQ):
              if len(regblk.BLKID) == 1:
                if type(regblk.BLKID[0]) == int:
                  f.write("""
  always_comb begin
    o_req_local_if_ar         = station2brb_req_ar;
    o_req_local_if_ar.araddr  = station2brb_req_ar.araddr[%s_RING_ADDR_WIDTH - %s_BLKID_WIDTH - 1 : 0];
    o_req_local_if_aw         = station2brb_req_aw;
    o_req_local_if_aw.awaddr  = station2brb_req_aw.awaddr[%s_RING_ADDR_WIDTH - %s_BLKID_WIDTH - 1 : 0];
  end""" % (blkname.upper(), blkname.upper(), blkname.upper(), blkname.upper()))
                elif type(regblk.BLKID[0]) == list:
                  f.write("""
  always_comb begin
    o_req_local_if_ar         = station2brb_req_ar;
    o_req_local_if_ar.araddr  = station2brb_req_ar.araddr[%s_RING_ADDR_WIDTH - %s_BLKID_WIDTH_SUB_0 - 1 : 0];
    o_req_local_if_aw         = station2brb_req_aw;
    o_req_local_if_aw.awaddr  = station2brb_req_aw.awaddr[%s_RING_ADDR_WIDTH - %s_BLKID_WIDTH_SUB_0 - 1 : 0];
  end""" % (blkname.upper(), blkname.upper(), blkname.upper(), blkname.upper()))
              else:
                f.write("""
  always_comb begin
    o_req_local_if_ar         = station2brb_req_ar;
    o_req_local_if_ar.araddr  = station2brb_req_ar.araddr[%s_RING_ADDR_WIDTH - %s_BLKID_WIDTH - 1 : 0];
    o_req_local_if_aw         = station2brb_req_aw;
    o_req_local_if_aw.awaddr  = station2brb_req_aw.awaddr[%s_RING_ADDR_WIDTH - %s_BLKID_WIDTH - 1 : 0];
  end""" % (blkname.upper(), blkname.upper(), blkname.upper(), blkname.upper()))
            else:
              f.write("""
  assign o_req_local_if_ar       = station2brb_req_ar;
  assign o_req_local_if_aw       = station2brb_req_aw;""")
            f.write("""
  assign o_req_local_if_w        = station2brb_req_w;

  // Request Readys
  always_ff @(posedge clk_o_local) begin
    if (rstn == 1'b0) begin
      rff_awrdy <= 1'b0;
      rff_wrdy  <= 1'b0;
    end else begin
      rff_awrdy <= next_awrdy;
      rff_wrdy  <= next_wrdy;
    end
  end
  always_comb begin
    next_awrdy = rff_awrdy;
    next_wrdy  = rff_wrdy;
    if ((~bdec & o_req_local_if_awready & o_req_local_if_awvalid) | (bdec & i_resp_if_bready[0])) begin
      next_awrdy = 1'b1;
    end else if (rff_awrdy & rff_wrdy) begin
      next_awrdy = 1'b0;
    end
    if ((~bdec & o_req_local_if_wready & o_req_local_if_wvalid)  | (bdec & i_resp_if_bready[0])) begin
      next_wrdy = 1'b1;
    end else if (rff_awrdy & rff_wrdy) begin
      next_wrdy = 1'b0;
    end
  end
  assign station2brb_req_awready  = rff_awrdy & rff_wrdy;
  assign station2brb_req_wready   = rff_awrdy & rff_wrdy;

  assign station2brb_req_arready  = (~rdec & o_req_local_if_arready) | (rdec & i_resp_if_rready[0]);

  // if 1, means input port i's destination station id matches output j's station id
  logic [1:0][0:0]      is_r_dst_match;
  logic [1:0][0:0]      is_b_dst_match;

  oursring_resp_if_b_t  brb_b;
  oursring_resp_if_r_t  brb_r;

  assign brb_b.bid   = station2brb_req_aw.awid;
  assign brb_b.bresp = bresp;

  assign brb_r.rid   = station2brb_req_ar.arid;
  assign brb_r.rresp = rresp;
  assign brb_r.rdata = data;
  assign brb_r.rlast = 1'b1;

  assign i_resp_if_b[0] = brb_b;
  assign i_resp_if_b[1] = i_resp_local_if_b;

  assign i_resp_if_r[0] = brb_r;
  assign i_resp_if_r[1] = i_resp_local_if_r;

  assign i_resp_if_rvalid[0] = station2brb_req_arvalid & rdec;
  assign i_resp_if_rvalid[1] = i_resp_local_if_rvalid;

  assign i_resp_local_if_rready = i_resp_if_rready[1];

  assign i_resp_if_bvalid[0] = station2brb_req_awvalid & station2brb_req_wvalid & bdec & ~rff_awrdy & ~rff_wrdy;
  assign i_resp_if_bvalid[1] = i_resp_local_if_bvalid;

  assign i_resp_local_if_bready = i_resp_if_bready[1];

  assign station2brb_rsp_b        = o_resp_ppln_if_b[0];
  assign station2brb_rsp_r        = o_resp_ppln_if_r[0];
  assign station2brb_rsp_rvalid   = o_resp_ppln_if_rvalid[0];
  assign o_resp_ppln_if_rready[0] = station2brb_rsp_rready;
  assign station2brb_rsp_bvalid   = o_resp_ppln_if_bvalid[0];
  assign o_resp_ppln_if_bready[0] = station2brb_rsp_bready;
  
  assign is_r_dst_match = 2'b11;
  assign is_b_dst_match = 2'b11;

  oursring_resp #(.N_IN_PORT(2), .N_OUT_PORT(1)) resp_u (
    .i_resp_if_b            (i_resp_if_b),
    .i_resp_if_r            (i_resp_if_r),
    .i_resp_if_rvalid       (i_resp_if_rvalid),
    .i_resp_if_rready       (i_resp_if_rready),
    .i_resp_if_bvalid       (i_resp_if_bvalid),
    .i_resp_if_bready       (i_resp_if_bready),
    .o_resp_ppln_if_b       (o_resp_ppln_if_b),
    .o_resp_ppln_if_r       (o_resp_ppln_if_r),
    .o_resp_ppln_if_rvalid  (o_resp_ppln_if_rvalid),
    .o_resp_ppln_if_rready  (o_resp_ppln_if_rready),
    .o_resp_ppln_if_bvalid  (o_resp_ppln_if_bvalid),
    .o_resp_ppln_if_bready  (o_resp_ppln_if_bready),
    .is_r_dst_match         (is_r_dst_match),
    .is_b_dst_match         (is_b_dst_match),
    .rstn                   (rstn),
    .clk                    (clk_o_local)
    );
  
""")
            f.write("endmodule\n")
            # end ifndef
            f.write("`endif\n")
            f.write("`endif\n")
            f.close()
        #fpy.write("]")
        #fpy.close()


    def gen_addr_map(self):
      dc_reg = dict()
      for regblk in self.regblk_list:
        #offset_alloc(regblk)
        base_addr = list()
        for blkid in regblk.get_blkid_list():
          if type(blkid) == int:
            base_addr.append(blkid << (RING_ADDR_WIDTH - regblk.get_blkid_width()[0]))
          else:
            base_addr.append(blkid[0] << (RING_ADDR_WIDTH - regblk.get_blkid_width()[0][0]))
        if regblk.get_blkid_length() == 1:
          for reg in regblk.get_reglist():
            dc_reg[base_addr[0] + reg.get_offset()] = reg.get_name()
        else:
          for reg in regblk.get_reglist():
            for i in range(regblk.get_blkid_length()):
              dc_reg[base_addr[i] + reg.get_offset()] = reg.get_name() + "_%d" % i
      for addr in sorted(dc_reg.keys()):
        print("%s: 0x%x" % (dc_reg[addr], addr))
