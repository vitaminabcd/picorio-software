#!/usr/bin/env python3
from multiprocessing import Process
from dbg_defines import *
parse_args()

from lib import *
import sys
import os
proj_root = os.environ['PROJ_ROOT']

# Thread to FESVR
def notification():
    driver.setup()
    driver.execute_command()

def main():
    folder_path = '%s/doc/design/oursring/' % (proj_root) # change to the appropriate file path
    file_list = list()
    for file in os.listdir(folder_path):
        if file.endswith('.csv'):
            file_list.append(folder_path + file)
    device = DEVICE(file_list)
    user = User(device)
    lib = LIB(user) # gui is the base to lib

    if (DRV == "DBG2FESVR"):
        notify = Process(target=notification)
        notify.start()

    lib.cmdloop()

if __name__ == '__main__':
    main()
