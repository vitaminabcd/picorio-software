"""
    FTDI FT4222H SPI interface code
    Author: Wilson Ko
    Date created: 12/19/2017
    Python version: 3.6
    Copyright 2017, OURS Technology Inc.
"""
from ctypes import *
import os.path
import platform
from enum import Enum
from spi import SPI
import time
if platform.system() == 'Windows':
    from ctypes.wintypes import *

class FTDI_FT4222H(SPI):

    def __init__(self, spi_mode, spi_clock, spi_c_pol, spi_c_pha, debug_msg=False):
        super().__init__()

        if platform.system() == 'Windows':
            self._libft4222 = cdll.LoadLibrary(os.path.join(os.path.dirname(__file__), "LibFT4222.dll"))
            self._ftd2xx = cdll.LoadLibrary("ftd2xx.dll")
        else:
            self._libft4222 = cdll.LoadLibrary(os.path.join(os.path.dirname(__file__), "libft4222.so.1.4.1.231"))
            self._ftd2xx = cdll.LoadLibrary(os.path.join(os.path.dirname(__file__), "libft4222.so.1.4.1.231"))
        self._spi_mode = spi_mode
        self._spi_clock = spi_clock
        self._spi_c_pol = spi_c_pol
        self._spi_c_pha = spi_c_pha
        self._debug = debug_msg

        g_ft4222_dev_list = self._list_ft_usb_devices()

        if not g_ft4222_dev_list:
            raise IOError("No FT4222 device is found!")

        ft_open_by_location = 4
        ft_handle = c_void_p()
        ft_status = self._ftd2xx.FT_OpenEx(g_ft4222_dev_list[0].LocId, ft_open_by_location, pointer(ft_handle))

        if ft_status != FTDI_FT4222H.FT_Status.FT_OK.value:
            raise IOError("Open a FT4222 device failed!")

        mode = c_uint(spi_mode.value)
        div = c_uint(spi_clock.value)
        cpol = c_uint(spi_c_pol.value)
        cpha = c_uint(spi_c_pha.value)
        ft_status = self._libft4222.FT4222_SPIMaster_Init(ft_handle, mode, div, cpol, cpha, 0x01)

        if ft_status != FTDI_FT4222H.FT_Status.FT_OK.value:
            raise IOError("Init FT4222 as SPI master device failed!")

        self._spi_handle = ft_handle

    def _dev_flag_to_str(self, flag):
        if flag & 0x1:
            msg = "DEVICE_OPEN, "
        else:
            msg = "DEVICE_CLOSED, "
        if flag & 0x2:
            msg = msg + "High-speed USB"
        else:
            msg = msg + "Full-speed USB"
        return msg

    def _list_ft_usb_devices(self):

        g_ft4222_dev_list = []
        num_of_devices = c_uint64(0)
        status = self._ftd2xx.FT_CreateDeviceInfoList(pointer(num_of_devices))

        for i_dev in range(0, num_of_devices.value):
            FT_DEV_LIST_INFO_NODE = FTDI_FT4222H.FT_DEVICE_LIST_INFO_NODE
            dev_info = FTDI_FT4222H.FT_DEVICE_LIST_INFO_NODE()
            status = self._ftd2xx.FT_GetDeviceInfoDetail(c_uint64(i_dev),
                                                         byref(dev_info,
                                                               FT_DEV_LIST_INFO_NODE.Flags.offset),
                                                         byref(dev_info,
                                                               FT_DEV_LIST_INFO_NODE.Type.offset),
                                                         byref(dev_info,
                                                               FT_DEV_LIST_INFO_NODE.ID.offset),
                                                         byref(dev_info,
                                                               FT_DEV_LIST_INFO_NODE.LocId.offset),
                                                         byref(dev_info,
                                                               FT_DEV_LIST_INFO_NODE.SerialNumber.offset),
                                                         byref(dev_info,
                                                               FT_DEV_LIST_INFO_NODE.Description.offset),
                                                         byref(dev_info,
                                                               FT_DEV_LIST_INFO_NODE.ftHandle.offset))
            if status == FTDI_FT4222H.FT_Status.FT_OK.value:
                if self._debug:
                    print("Dev %d:" % i_dev)
                    print("  Flags        = 0x%x, (%s)" % (dev_info.Flags, self._dev_flag_to_str(dev_info.Flags)))
                    print("  Type         = 0x%x" % dev_info.Type)
                    print("  ID           = 0x%x" % dev_info.ID)
                    print("  LocId        = 0x%x" % dev_info.LocId)
                    print("  SerialNumber = %s" % dev_info.SerialNumber)
                    print("  Description  = %s" % dev_info.Description)
                    # print("  ftHandle     = 0x%x" % dev_info.ftHandle)

                desc = dev_info.Description
                if desc == b'FT4222' or desc == b'FT4222 A':
                    g_ft4222_dev_list.append(dev_info)
        return g_ft4222_dev_list

    def driver_write(self, addr, data):
        rb = c_uint8 * 100
        wb = c_uint8 * 100
        read_buffer = rb()
        write_buffer = wb()

        if self._spi_mode != FTDI_FT4222H.SPIMode.SPI_IO_SINGLE:
            single_write_bytes = c_uint8(1)
            multi_write_bytes = c_uint16(13)
            multi_read_bytes = c_uint16(0)
            size_of_read = pointer(c_uint32())

            write_buffer[0] = 0x04
            write_buffer[1] = (addr >> 32) & 0xff
            write_buffer[2] = (addr >> 24) & 0xff
            write_buffer[3] = (addr >> 16) & 0xff
            write_buffer[4] = (addr >> 8) & 0xff
            write_buffer[5] = (addr >> 0) & 0xff
            write_buffer[6] = (data >> 56) & 0xff
            write_buffer[7] = (data >> 48) & 0xff
            write_buffer[8] = (data >> 40) & 0xff
            write_buffer[9] = (data >> 32) & 0xff
            write_buffer[10] = (data >> 24) & 0xff
            write_buffer[11] = (data >> 16) & 0xff
            write_buffer[12] = (data >> 8) & 0xff
            write_buffer[13] = (data >> 0) & 0xff

            if self._debug:
                for i in range(0, 14):
                    print("write_buffer[%d] = 0x%x" % (i, write_buffer[i]))

            ft_status = self._libft4222.FT4222_SPIMaster_MultiReadWrite(self._spi_handle,
                                                                        pointer(read_buffer),
                                                                        pointer(write_buffer),
                                                                        single_write_bytes,
                                                                        multi_write_bytes,
                                                                        multi_read_bytes,
                                                                        size_of_read)

            if ft_status != FTDI_FT4222H.FT_Status.FT_OK.value:
                raise IOError("driver_write failed!")

            if self._debug:
                print("Done writing data 0x%011x to address 0x%011x" % (data, addr))

        else:
            size_transferred = pointer(c_uint16(0))
            is_end_transaction = c_bool(True)
            size_to_transfer = c_uint16(14)

            write_buffer[0] = 0x04       # Write
            write_buffer[1] = (addr >> 32) & 0xff
            write_buffer[2] = (addr >> 24) & 0xff
            write_buffer[3] = (addr >> 16) & 0xff
            write_buffer[4] = (addr >> 8) & 0xff
            write_buffer[5] = (addr >> 0) & 0xff
            write_buffer[6] = (data >> 56) & 0xff
            write_buffer[7] = (data >> 48) & 0xff
            write_buffer[8] = (data >> 40) & 0xff
            write_buffer[9] = (data >> 32) & 0xff
            write_buffer[10] = (data >> 24) & 0xff
            write_buffer[11] = (data >> 16) & 0xff
            write_buffer[12] = (data >> 8) & 0xff
            write_buffer[13] = (data >> 0) & 0xff

            ft_status = self._libft4222.FT4222_SPIMaster_SingleReadWrite(self._spi_handle,
                                                                         pointer(read_buffer),
                                                                         pointer(write_buffer),
                                                                         size_to_transfer,
                                                                         size_transferred,
                                                                         is_end_transaction)
            if ft_status != FTDI_FT4222H.FT_Status.FT_OK.value:
                raise IOError("driver_write failed!")

            if self._debug:
                print("Done writing data 0x%011x to address 0x%011x" % (data, addr))

    def driver_read(self, addr, retry=True):
        rb = c_uint8 * 40
        wb = c_uint8 * 40
        read_buffer = rb(0)
        write_buffer = wb()
        status = 1
        resp_err = 1
        ret_data = 0
        retry_count = 1000

        if self._spi_mode != FTDI_FT4222H.SPIMode.SPI_IO_SINGLE:
            single_write_bytes = c_uint8(1)
            multi_write_bytes = c_uint16(6)
            multi_read_bytes = c_uint16(9)
            size_of_read = pointer(c_uint32(0))

            write_buffer[0] = 0x05      # Read
            write_buffer[1] = (addr >> 32) & 0xff
            write_buffer[2] = (addr >> 24) & 0xff
            write_buffer[3] = (addr >> 16) & 0xff
            write_buffer[4] = (addr >> 8) & 0xff
            write_buffer[5] = (addr >> 0) & 0xff
            write_buffer[6] = 0xa5      # Turn around byte: 1010 0101

            opc_ctr = 0
            while opc_ctr < retry_count:
                size_of_read.value = 0
                for i in range(0, 33):
                    read_buffer[i] = 0
                ft_status = self._libft4222.FT4222_SPIMaster_MultiReadWrite(self._spi_handle,
                                                                            pointer(read_buffer),
                                                                            pointer(write_buffer),
                                                                            single_write_bytes,
                                                                            multi_write_bytes,
                                                                            multi_read_bytes,
                                                                            size_of_read)

                if (ft_status != FTDI_FT4222H.FT_Status.FT_OK.value) or (size_of_read.value != multi_read_bytes):
                    raise IOError("driver_read failed!")
                else:
                    status = read_buffer[32]
                    resp_err = status & 0x01
                    if (status & 0x0f) == 0:
                        break
                    else:
                        #print("spi 0x158160a")
                        if retry:
                            opc_ctr += 1
                            time.sleep(0.001 * 1.005 ** opc_ctr)
                        else:
                            break

            if (status & 0x0e) != 0:
                if retry:
                    raise IOError("driver_read failed: timeout")
                else:
                    if self._debug:
                        print("driver_read failed: timeout")
            elif (status & 0x01) == 1:
                if retry:
                    raise IOError("driver_read failed: invalid data")
                else:
                    if self._debug:
                        print("driver_read failed: invalid data")

            for i in range(0, 32):
                ret_data = ret_data << 8
                ret_data = ret_data | read_buffer[i]

            if self._debug:
                print("Done reading address 0x%011x, got data 0x%011x" % (addr, ret_data))
                print("Done reading address 0x%011x, got raw data 0x%x" % (addr, read_buffer))

            return ret_data, resp_err

        else:
            # spi single read
            size_transferred = pointer(c_uint16(0))
            is_end_transaction = c_bool(True)
            size_to_transfer = c_uint16(16)

            write_buffer[0] = 0x05       # Read
            write_buffer[1] = (addr >> 32) & 0xff
            write_buffer[2] = (addr >> 24) & 0xff
            write_buffer[3] = (addr >> 16) & 0xff
            write_buffer[4] = (addr >> 8) & 0xff
            write_buffer[5] = (addr >> 0) & 0xff
            write_buffer[6] = 0xa5       # Turn around byte: 1010 0101
            for i in range(7, 16):
                write_buffer[i] = 0x0

            opc_ctr = 0
            while opc_ctr < retry_count:
                size_transferred.value = 0
                for i in range(0, 16):
                    read_buffer[i] = 0x0

                ft_status = self._libft4222.FT4222_SPIMaster_SingleReadWrite(self._spi_handle,
                                                                             pointer(read_buffer),
                                                                             pointer(write_buffer),
                                                                             size_to_transfer,
                                                                             size_transferred,
                                                                             is_end_transaction)

                if ft_status != FTDI_FT4222H.FT_Status.FT_OK.value:
                    raise IOError("driver_read failed!")
                else:
                    status = read_buffer[16 - 1]
                    resp_err = status & 0x01
                    if (status & 0x0f) == 0:
                        break
                    else:
                        # here for error message
                        #print("spi read error")
                        if retry:
                            opc_ctr += 1
                            time.sleep(0.001 * 1.005 ** opc_ctr)
                        else:
                            break

            if (status & 0x0e) != 0:
                if retry:
                    raise IOError("driver_read failed: timeout")
                else:
                    if self._debug:
                        print("driver_read failed: timeout")
            elif (status & 0x01) == 1:
                if retry:
                    raise IOError("driver_read failed: invalid data")
                else:
                    if self._debug:
                        print("driver_read failed: invalid data")

            for i in range(0, 8):
                ret_data = ret_data << 8
                ret_data = ret_data | read_buffer[i + 7]

            if self._debug:
                print("Done reading address 0x%011x, got data 0x%011x" % (addr, ret_data))

                read_buffer_content = 0
                for i in range(len(read_buffer)):
                    read_buffer_content = read_buffer_content << 8
                    read_buffer_content = read_buffer_content | read_buffer[i]
                    print("Done reading address 0x%011x, got raw data 0x%x at %d" % (addr, read_buffer[i], i))
                    print("Done reading address 0x%011x, got raw data 0x%x" % (addr, read_buffer_content))

            return ret_data, resp_err

    class SPIMode(Enum):
        SPI_IO_NONE = 0
        SPI_IO_SINGLE = 1
        SPI_IO_DUAL = 2
        SPI_IO_QUAD = 4

    class SPIClock(Enum):
        CLK_NONE = 0
        CLK_DIV_2 = 1      # 1/2   System Clock
        CLK_DIV_4 = 2      # 1/4   System Clock
        CLK_DIV_8 = 3      # 1/8   System Clock
        CLK_DIV_16 = 4     # 1/16  System Clock
        CLK_DIV_32 = 5     # 1/32  System Clock
        CLK_DIV_64 = 6     # 1/64  System Clock
        CLK_DIV_128 = 7    # 1/128 System Clock
        CLK_DIV_256 = 8    # 1/256 System Clock
        CLK_DIV_512 = 9     # 1/512 System Clock

    class SPICPol(Enum):
        CLK_IDLE_LOW = 0
        CLK_IDLE_HIGH = 1

    class SPICPha(Enum):
        CLK_LEADING = 0
        CLK_TRAILING = 1

    class FT_DEVICE_LIST_INFO_NODE(Structure):
        _fields_ = [
            ("Flags", c_ulong),
            ("Type", c_ulong),
            ("ID", c_ulong),
            ("LocId", c_uint64),
            ("SerialNumber", c_char * 16),
            ("Description", c_char * 64),
            ("ftHandle", c_void_p)
        ]

    class FT_Status(Enum):
        FT_OK = 0
        FT_INVALID_HANDLE = 1
        FT_DEVICE_NOT_FOUND = 2
        FT_DEVICE_NOT_OPENED = 3
        FT_IO_ERROR = 4
        FT_INSUFFICIENT_RESOURCES = 5
        FT_INVALID_PARAMETER = 6
        FT_INVALID_BAUD_RATE = 7

        FT_DEVICE_NOT_OPENED_FOR_ERASE = 8
        FT_DEVICE_NOT_OPENED_FOR_WRITE = 9
        FT_FAILED_TO_WRITE_DEVICE = 10
        FT_EEPROM_READ_FAILED = 11
        FT_EEPROM_WRITE_FAILED = 12
        FT_EEPROM_ERASE_FAILED = 13
        FT_EEPROM_NOT_PRESENT = 14
        FT_EEPROM_NOT_PROGRAMMED = 15
        FT_INVALID_ARGS = 16
        FT_NOT_SUPPORTED = 17
        FT_OTHER_ERROR = 18
        FT_DEVICE_LIST_NOT_READY = 19

