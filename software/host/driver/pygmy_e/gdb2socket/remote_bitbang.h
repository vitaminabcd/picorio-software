#ifndef REMOTE_BITBANG_H
#define REMOTE_BITBANG_H

#include <stdint.h>

#include "gdb2socket/jtag_dtm.h"
#include "gdb2socket/fake_dmstatus.h"

class remote_bitbang_t
{
public:
  // Create a new server, listening for connections from localhost on the given
  // port.
  remote_bitbang_t(jtag_dtm_t *tap);

  // Do a bit of work.
  void tick();

  jtag_dtm_t *tap;
private:

  int socket_fd;
  int client_fd;

  static const ssize_t buf_size = 64 * 1024;
  char recv_buf[buf_size];
  ssize_t recv_start, recv_end;

  // Check for a client connecting, and accept if there is one.
  void accept();
  // Execute any commands the client has for us.
  void execute_commands();
};

#ifdef __cplusplus
extern "C" {
#endif
remote_bitbang_t* rbb_new();
#ifdef __cplusplus
}
#endif

#endif
