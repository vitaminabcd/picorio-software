#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <inttypes.h>

#include "common-spi.h"

//#define DEBUG_FT4222

enum {
	SPI_CMD_NONE 		= 0,
	SPI_CMD_READ 		= 1,
	SPI_CMD_WRITE 		= 2,
	SPI_CMD_READ_RESP  	= 3,
	SPI_CMD_WRITE_RESP = 4,		
	SPI_CMD_BYPASS 		= 5,		
};

#define BUFFER_SIZE			32
#define CMD_RESP_INDEX		4
#define XFER1_SIZE			16
#define XFER2_SIZE			32

/*
 * commented by Rong Chen
 * We select Mode 0. 
Mode Number of Interfaces 
0    2
Device Function:
(FT4222 A)a. The first interface: it can be one of SPI master, SPI slave, I2C master, or I2C slave device.
(FT4222 B)b. The second interface: GPIO device.
*
*/

/*
 * select all gpios for reset, high level by default.
*/
FT_HANDLE gpio_setup(void)
{
  FT_HANDLE ftHandle = NULL;
  FT_STATUS ftStatus;
  char *desc = (char *)"FT4222 B";
  ftStatus = FT_OpenEx(desc, FT_OPEN_BY_DESCRIPTION, &ftHandle);
  if (FT_OK != ftStatus) {
    printf("Open a FT4222 B device failed!\n");
    return NULL;
  }

//  printf("Open FT4222 B device !\n");
  
  GPIO_Dir gpioDir[4];
  gpioDir[0] = GPIO_OUTPUT;
  gpioDir[1] = GPIO_OUTPUT;
  gpioDir[2] = GPIO_OUTPUT;
  gpioDir[3] = GPIO_OUTPUT;
  FT4222_GPIO_Init(ftHandle, gpioDir);
  //disable suspend out , enable gpio 2
  FT4222_SetSuspendOut(ftHandle, false);
  //disable interrupt , enable gpio 3
  FT4222_SetWakeUpInterrupt(ftHandle, false);
  // set gpio0/gpio1/gpio2/gpio3 output level high
  FT4222_GPIO_Write(ftHandle, GPIO_PORT0, 1);
  FT4222_GPIO_Write(ftHandle, GPIO_PORT1, 1);
  FT4222_GPIO_Write(ftHandle, GPIO_PORT2, 1);
  FT4222_GPIO_Write(ftHandle, GPIO_PORT3, 1);

  return ftHandle;
}

void gpio_reset(FT_HANDLE ftHandle)
{
  FT4222_GPIO_Write(ftHandle, GPIO_PORT0, 0);
  FT4222_GPIO_Write(ftHandle, GPIO_PORT1, 0);
  FT4222_GPIO_Write(ftHandle, GPIO_PORT2, 0);
  FT4222_GPIO_Write(ftHandle, GPIO_PORT3, 0);
  printf("Set RESET\n");
  FT4222_GPIO_Write(ftHandle, GPIO_PORT0, 1);
  FT4222_GPIO_Write(ftHandle, GPIO_PORT1, 1);
  FT4222_GPIO_Write(ftHandle, GPIO_PORT2, 1);
  FT4222_GPIO_Write(ftHandle, GPIO_PORT3, 1);
  printf("Release RESET\n");
}

inline std::string DeviceFlagToString(DWORD flags)
{
  std::string msg;
  msg += (flags & 0x1) ? "DEVICE_OPEN" : "DEVICE_CLOSED";
  msg += ", ";
  msg += (flags & 0x2) ? "High-speed USB" : "Full-speed USB";
  return msg;
}

std::vector< FT_DEVICE_LIST_INFO_NODE > ListFtUsbDevices()
{
  std::vector< FT_DEVICE_LIST_INFO_NODE > g_FT4222DevList;
  
  DWORD numOfDevices = 0;
  FT_STATUS status = FT_CreateDeviceInfoList(&numOfDevices);
  for (DWORD iDev = 0; iDev < numOfDevices; ++iDev)
  {
    FT_DEVICE_LIST_INFO_NODE devInfo;
    memset(&devInfo, 0, sizeof(devInfo));

    status = FT_GetDeviceInfoDetail(iDev,
             &devInfo.Flags, &devInfo.Type, &devInfo.ID, &devInfo.LocId,
             devInfo.SerialNumber, devInfo.Description, &devInfo.ftHandle);

    if (FT_OK == status)
    {
#ifdef DEBUG_FT4222
      printf("Dev %d:\n", iDev);
      printf("  Flags        = 0x%x, (%s)\n", devInfo.Flags,
                  DeviceFlagToString(devInfo.Flags).c_str());
      printf("  Type         = 0x%x\n",       devInfo.Type);
      printf("  ID           = 0x%x\n",       devInfo.ID);
      printf("  LocId        = 0x%x\n",       devInfo.LocId);
      printf("  SerialNumber = %s\n",         devInfo.SerialNumber);
      printf("  Description  = %s\n",         devInfo.Description);
      printf("  ftHandle     = 0x%x\n",       devInfo.ftHandle);
#endif
      const std::string desc = devInfo.Description;
      if (desc == "FT4222" || desc == "FT4222 A")
      {
        g_FT4222DevList.push_back(devInfo);
      }
    }
  }
  return g_FT4222DevList;
}

FT_HANDLE setup(FT4222_SPIMode mode, FT4222_SPIClock div, FT4222_SPICPOL cpol, FT4222_SPICPHA cpha)
{
#if 0 /* it is too slow during finding FT4222 */
  std::vector< FT_DEVICE_LIST_INFO_NODE > g_FT4222DevList;

  g_FT4222DevList = ListFtUsbDevices();

  if (g_FT4222DevList.empty())
  {
    printf("No FT4222 device is found!\n");
    return NULL;
  }

  FT_HANDLE ftHandle = NULL;
  FT_STATUS ftStatus;
  for (int i = 0; i < g_FT4222DevList.size(); i++) {
    ftStatus = FT_OpenEx((PVOID)g_FT4222DevList[i].LocId,
                         FT_OPEN_BY_LOCATION, &ftHandle);
    if (FT_OK != ftStatus) {
      printf("Open a FT4222 device (index = %0d) failed!\n", i);
      continue;
    }
#ifdef DEBUG_FT4222
    // Check Product ID
    printf("Open FT4222 device (index = %0d)!\n", i);
#endif
    break;
  }
#else
  FT_HANDLE ftHandle = NULL;
  FT_STATUS ftStatus;
  char *desc = (char *)"FT4222 A";
  ftStatus = FT_OpenEx(desc, FT_OPEN_BY_DESCRIPTION, &ftHandle);
  if (FT_OK != ftStatus) {
    printf("Open a FT4222 A device failed!\n");
    return NULL;
  }
#ifdef DEBUG_FT4222
    // Check Product ID
    printf("Open FT4222 A device !\n");
#endif
#endif

  ftStatus = FT4222_SPIMaster_Init(ftHandle, mode, div,
                                   cpol, cpha, 0x01);
  if (FT_OK != ftStatus)
  {
    printf("Init FT4222 as SPI master device failed!\n");
    return NULL;
  }
  return ftHandle;
}

void do_status_check(FT4222_SPIMode mode, FT_HANDLE ftHandle)
{
	// TODO:
}

int do_signle_rw(FT_HANDLE ftHandle, uint64 addr, uint64 data, uint64 *rdata, int spi_cmd)
{
	uint8 readBuffer[BUFFER_SIZE];
	uint8 writeBuffer[BUFFER_SIZE];
	int i, ret = 0;
	uint64 ret_data = 0UL;

    uint16 sizeTransferred = 0;
    BOOL     isEndTransaction = 0;
    uint16   sizeToTransfer = XFER1_SIZE;
    FT_STATUS ftStatus;
	uint8 cmd_resp;

	memset(readBuffer, 0, sizeof(readBuffer));
	memset(writeBuffer, 0, sizeof(writeBuffer));

	if (SPI_CMD_WRITE == spi_cmd) {
		writeBuffer[ 2] = (data >> 56) & 0xff; // data7
		writeBuffer[ 3] = (data >> 48) & 0xff; // data6
		writeBuffer[ 4] = (data >> 40) & 0xff; // data5
		writeBuffer[ 5] = (data >> 32) & 0xff; // data4
		writeBuffer[ 6] = (data >> 24) & 0xff; // data3
		writeBuffer[ 7] = (data >> 16) & 0xff; // data2
		writeBuffer[ 8] = (data >> 8) & 0xff; // data1
		writeBuffer[ 9] = (data) & 0xff; // data0
	}
    writeBuffer[10] = (addr >> 32) & 0xffUL;
    writeBuffer[11] = (addr >> 24) & 0xff;
    writeBuffer[12] = (addr >> 16) & 0xff;
    writeBuffer[13] = (addr >> 8) & 0xff;
    writeBuffer[14] = (addr) & 0xff;
	writeBuffer[15] = spi_cmd;

    ftStatus = FT4222_SPIMaster_SingleWrite(ftHandle, writeBuffer, sizeToTransfer,
                                                &sizeTransferred, isEndTransaction);

    if (FT_OK != ftStatus || sizeTransferred != sizeToTransfer) {
      printf("%s xfer1 failed: ftSatus: %d, sizeTransferred: %u !\n", __func__, ftStatus, sizeTransferred);
      return -1;
    } 

	isEndTransaction = 1;
	sizeToTransfer = XFER2_SIZE;
    if (sizeToTransfer > sizeof(writeBuffer)) {
      printf("%s: buffer overflow...\n", __func__);
      return -1;
    }
    memset(writeBuffer, 0, sizeToTransfer);

    ftStatus = FT4222_SPIMaster_SingleReadWrite(ftHandle, readBuffer,
                                                writeBuffer, sizeToTransfer,
                                                &sizeTransferred, isEndTransaction);

	cmd_resp = readBuffer[CMD_RESP_INDEX];

    if (FT_OK != ftStatus || 
		sizeTransferred != sizeToTransfer || 
		cmd_resp != ((SPI_CMD_WRITE == spi_cmd) ? SPI_CMD_WRITE_RESP : SPI_CMD_READ_RESP)) {
      printf("%s xfer2 failed: ftSatus: %d, sizeTransferred: %u, cmd_resp: 0x%2x !\n", __func__, ftStatus, sizeTransferred, cmd_resp);
      return -1;
    }

	if (SPI_CMD_WRITE == spi_cmd)
		return 0;

    for (i = CMD_RESP_INDEX+1; i < CMD_RESP_INDEX+1+8; i++) {
      ret_data = ret_data << 8;
      ret_data = ret_data | readBuffer[i];
    }

	*rdata = ret_data;
	return 0;
}

uint64 do_read(FT4222_SPIMode mode, FT_HANDLE ftHandle, uint64 addr)
{
  if (mode == SPI_IO_SINGLE) {
	int ret;
	uint64 ret_data = 0UL;

	ret = do_signle_rw(ftHandle, addr, 0UL, &ret_data, SPI_CMD_READ);
	if (ret) 
		return 0UL;

	return ret_data;
  }
  
  printf("non-SPI_IO_SINGLE mode is not implemented yet !...\n");
  return 0UL;
}

void do_write(FT4222_SPIMode mode, FT_HANDLE ftHandle, uint64 addr, uint64 data)
{
  if (mode == SPI_IO_SINGLE)  {
	do_signle_rw(ftHandle, addr, data, NULL, SPI_CMD_WRITE);
  } else {
	printf("%s: non-SPI_IO_SINGLE mode is not implemented yet !...\n", __func__);
  }
}

